//
//  AppDelegate.m
//  Baisi
//
//  Created by albert on 16/3/13.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "AppDelegate.h"
#import "JKTabBarController.h"
#import "JKPushGuideView.h"
#import "JKTopWindowViewController.h"
#import "WelcomeView.h"
#import "JKRuntimeTool.h"
#import "WXApi.h"
#import "SVProgressHUD.h"
#import "WeiboSDK.h"
#import "JKFileExplorerManager.h"
#import <Bugly/Bugly.h>

@interface AppDelegate () <WXApiDelegate, WeiboSDKDelegate>

/** consolTextView */
@property (nonatomic, weak) UITextView *consolTextView;
@end

NSString * const BuglyAppID = @"578b18e76f";

@implementation AppDelegate

- (UITextView *)consolTextView{
    if (!_consolTextView) {
        UITextView *consolTextView = [[UITextView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        consolTextView.userInteractionEnabled = NO;
        consolTextView.text = @"";
        consolTextView.backgroundColor = nil;
        consolTextView.font = [UIFont systemFontOfSize:12];
        consolTextView.editable = NO;
        [self.window addSubview:consolTextView];
        _consolTextView = consolTextView;
    }
    return _consolTextView;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [application setStatusBarOrientation:(UIInterfaceOrientationPortrait) animated:NO];
    
    // Bugly
    [Bugly startWithAppId:BuglyAppID];
    
    // 注册微信
    [WXApi registerApp:JKWXAppkey];
    [WeiboSDK registerApp:JKWeiboAppKey];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    //创建窗口
    self.window = [[UIWindow alloc] init];
    
    //设置window的frame
    self.window.frame = [UIScreen mainScreen].bounds;
    
    self.window.backgroundColor = [UIColor whiteColor];
    
    JKTabBarController *tabBarController = [[JKTabBarController alloc] init];
    
    //tabBarController.delegate = self;
    
    //设置窗口的根控制器为UITabBarController
    self.window.rootViewController = tabBarController;
    
    //设置窗口为主窗口并可见
    [self.window makeKeyAndVisible];
    
    // 显示欢迎界面
    [WelcomeView show];
    
    //推送提示
    [JKPushGuideView show];
    
    [JKRuntimeTool getMemberVariablesWithClassName:NSStringFromClass([UITextField class])];
    
    checkScreenX();
    
    [JKFileExplorerManager initializeWithAirDropFolderName:nil];
    
    return YES;
}

/** 横屏控制 */
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    
    if (self.isCanAutoRotate) {
        
        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeRight;
        
    }else {
        
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    return [WXApi handleOpenURL:url delegate:self] || [WeiboSDK handleOpenURL:url delegate:self];//|| [TencentOAuth HandleOpenURL:url]
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    if ([url isFileURL]) { // 处理文件
        
        NSLog(@"接收到文件: %@", [url path]);
        
        [JKFileExplorerManager handleOpenURL:url];
    }
    
    return [WXApi handleOpenURL:url delegate:self] || [WeiboSDK handleOpenURL:url delegate:self];//|| [TencentOAuth HandleOpenURL:url]
}

// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options{
    
    if ([url isFileURL]) { // 处理文件
        
        NSLog(@"接收到文件: %@", [url path]);
        
        [JKFileExplorerManager handleOpenURL:url];
    }
    
    return [WXApi handleOpenURL:url delegate:self] || [WeiboSDK handleOpenURL:url delegate:self];//|| [TencentOAuth HandleOpenURL:url]
}

#pragma mark - <WXApiDelegate>
- (void)onResp:(BaseResp*)resp;{
    switch (resp.errCode) {
        case WXSuccess:
            [JKProgressHUD showSuccessWithStatus:@"分享成功！"];
            break;
        case WXErrCodeUserCancel:
            [JKProgressHUD showErrorWithStatus:@"取消操作！"];
            break;
        case WXErrCodeAuthDeny:
            [JKProgressHUD showErrorWithStatus:@"授权失败！"];
            break;
        case WXErrCodeUnsupport:
            [JKProgressHUD showErrorWithStatus:@"微信不支持！"];
            break;
        default:
            [JKProgressHUD showErrorWithStatus:@"分享失败！"];
            break;
    }
}

#pragma mark - <WeiboSDKDelegate>
/**
 收到一个来自微博客户端程序的请求
 
 收到微博的请求后，第三方应用应该按照请求类型进行处理，处理完后必须通过 [WeiboSDK sendResponse:] 将结果回传给微博
 @param request 具体的请求对象
 */
- (void)didReceiveWeiboRequest:(WBBaseRequest *)request{
    
}

/**
 收到一个来自微博客户端程序的响应
 
 收到微博的响应后，第三方应用可以通过响应类型、响应的数据和 WBBaseResponse.userInfo 中的数据完成自己的功能
 @param response 具体的响应对象
 */
- (void)didReceiveWeiboResponse:(WBBaseResponse *)response{
    
    switch (response.statusCode) {
        case WeiboSDKResponseStatusCodeSuccess:
            [JKProgressHUD showSuccessWithStatus:@"分享成功！"];
            break;
        case WeiboSDKResponseStatusCodeUserCancel:
            [JKProgressHUD showErrorWithStatus:@"取消操作！"];
            break;
        case WeiboSDKResponseStatusCodeAuthDeny:
            [JKProgressHUD showErrorWithStatus:@"授权失败！"];
            break;
        case WeiboSDKResponseStatusCodeUnsupport:
            [JKProgressHUD showErrorWithStatus:@"不支持的请求！"];
            break;
        case WeiboSDKResponseStatusCodeUnknown:
            [JKProgressHUD showErrorWithStatus:@"未知错误！"];
            break;
        case WeiboSDKResponseStatusCodePayFail:
            [JKProgressHUD showErrorWithStatus:@"支付失败！"];
            break;
        default:
            [JKProgressHUD showErrorWithStatus:@"分享失败！"];
            break;
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //添加一个window，点击这个window，可以让屏幕的scrollView滚动到最顶部
    [JKTopWindowViewController show];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

