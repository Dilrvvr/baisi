//
//  JKRuntimeTool.m
//  Baisi
//
//  Created by 安永博 on 2016/11/24.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "JKRuntimeTool.h"
#import <objc/message.h>

@implementation JKRuntimeTool
/** 获取某个类的属性列表 .h中的属性 */
+ (void)getPropertiesWithClassName:(NSString *)className{
#ifdef DEBUG
    unsigned int count = 0;
    
    // 拷贝出所有的属性列表（.h文件中的@property属性）
    objc_property_t *properties =  class_copyPropertyList([UITextField class], &count);
    
    for (int i = 0; i < count; i++) {
        // 取出属性
        objc_property_t property = properties[i];
        
        // property_getName获取属性名称
        // property_getAttributes获取属性的类型
        NSLog(@"%s<--->%s", property_getName(property), property_getAttributes(property));
    }
    
    // 释放
    free(properties);
#endif
}

/** 获取某个类所有的成员变量列表 包括私有的 */
+ (void)getMemberVariablesWithClassName:(NSString *)className{
#ifdef DEBUG
    unsigned int count = 0;
    
    // 拷贝出所有的成员变量列表
    Ivar *ivars = class_copyIvarList([UITextField class], &count);
    
    for (int i = 0; i < count; i++) {
        // Ivar ivar = *(ivars + i);
        // 取出成员变量，和上面是等价的
        Ivar ivar = ivars[i];
        
        // ivar_getName获取成员变量名称
        // ivar_getTypeEncoding获取成员变量的类型
        NSLog(@"%s<--->%s", ivar_getName(ivar), ivar_getTypeEncoding(ivar));
    }
    
    // 释放
    free(ivars);
#endif
}
@end
