//
//  JKRecommendCategoryCell.h
//  Baisi
//
//  Created by albert on 16/3/15.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKRecommendCategoryModel;

@interface JKRecommendCategoryCell : UITableViewCell

/** 类别模型 */
@property (nonatomic,strong) JKRecommendCategoryModel *category;

@end
