//
//  JKRecommendUserCell.m
//  Baisi
//
//  Created by albert on 16/3/15.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKRecommendUserCell.h"
#import "JKRecommendUserModel.h"
#import "UIImageView+WebCache.h"

@interface JKRecommendUserCell ()
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel     *screen_nameLabel;
@property (weak, nonatomic) IBOutlet UILabel     *fans_countLabell;

@end

@implementation JKRecommendUserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUser:(JKRecommendUserModel *)user{
    _user = user;
    
    [self.headerImageView setHeaderIcon:user.header];
    
    self.screen_nameLabel.text = user.screen_name;
    
    //vip名字红色显示
    if (user.is_vip) {
        self.screen_nameLabel.textColor = JKColor(219, 21, 26);
    }
    
    self.fans_countLabell.text = [NSString stringWithFormat:@"%@人关注", user.fans_count];
}
@end
