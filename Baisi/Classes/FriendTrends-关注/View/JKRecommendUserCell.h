//
//  JKRecommendUserCell.h
//  Baisi
//
//  Created by albert on 16/3/15.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKRecommendUserModel;

@interface JKRecommendUserCell : UITableViewCell

/** 用户数据模型 */
@property (nonatomic,strong) JKRecommendUserModel *user;

@end
