//
//  JKRecommendCategoryCell.m
//  Baisi
//
//  Created by albert on 16/3/15.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKRecommendCategoryCell.h"
#import "JKRecommendCategoryModel.h"

@interface JKRecommendCategoryCell ()
@property (weak, nonatomic) IBOutlet UIView *selectedIndicator;

@end

@implementation JKRecommendCategoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectedIndicator.backgroundColor = JKColor(219, 21, 26);
    
    //当cell的selection为None时，即使cell被选中，cell内部的子控件也不会进入高亮状态

}

/**
 *  监听cell是否被选中，可以设置cell选中状态的样式
 */
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    //在非选中状态下，隐藏左侧的红边
    self.selectedIndicator.hidden = !selected;
    
    //设置textLabel文字颜色选中时为红色，非选中为默认灰色
    self.textLabel.textColor = selected ? self.selectedIndicator.backgroundColor : JKColor(78, 78, 78);
    
    //设置cell背景选中状态为白色，非选中状态为默认浅灰色
    self.backgroundColor = selected ? [UIColor whiteColor] : [UIColor clearColor];
}

- (void)setCategory:(JKRecommendCategoryModel *)category{
    _category = category;
    
    self.textLabel.text = category.name;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    //修改textLabel的高度
    self.textLabel.y = 2;
    self.textLabel.height = self.contentView.height - 2 * self.textLabel.y;
}
@end
