//
//  JKRecommendCategoryModel.m
//  Baisi
//
//  Created by albert on 16/3/15.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKRecommendCategoryModel.h"
//#import <MJExtension.h>

@implementation JKRecommendCategoryModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"ID" : @"id"};
}

- (NSMutableArray *)users{
    if (_users == nil) {
        _users = [NSMutableArray array];
    }
    return _users;
}
@end
