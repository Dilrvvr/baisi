//
//  JKRecommendUserModel.h
//  Baisi
//
//  Created by albert on 16/3/15.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKRecommendUserModel : NSObject

/** 头像 */
@property (nonatomic,copy) NSString *header;

/** 昵称 */
@property (nonatomic,copy) NSString *screen_name;

/** 粉丝数量 */
@property (nonatomic,strong) NSNumber *fans_count;

/** 是否vip */
@property (nonatomic,assign) BOOL is_vip;

/** 是否关注 */
@property (nonatomic,assign) BOOL is_follow;

@end
