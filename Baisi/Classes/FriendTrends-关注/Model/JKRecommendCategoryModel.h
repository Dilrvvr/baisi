//
//  JKRecommendCategoryModel.h
//  Baisi
//
//  Created by albert on 16/3/15.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKRecommendCategoryModel : NSObject
//使用NSNumber，防止服务器端返回null，我们kvc赋值的时候崩溃
/** id */
@property (nonatomic,strong) NSNumber *ID;
/** 名称 */
@property (nonatomic,copy) NSString *name;
/** 数量 */
@property (nonatomic,strong) NSNumber *count;
/** 用户总数 */
@property (nonatomic,strong) NSNumber *total;

/** 这个类对应的用户数据 */
@property (nonatomic,strong) NSMutableArray *users;
/** 当前页码 */
@property (nonatomic,assign) NSInteger currentPage;
@end
