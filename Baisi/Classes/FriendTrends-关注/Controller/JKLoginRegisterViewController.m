//
//  JKLoginRegisterViewController.m
//  Baisi
//
//  Created by albert on 16/3/17.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKLoginRegisterViewController.h"
#import "JKTextField.h"
#import "JKTopWindowViewController.h"

@interface JKLoginRegisterViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet JKTextField *phoneField;
@property (weak, nonatomic) IBOutlet UIButton *registerLoginButton;

/**
 *  登录框距离左边的间距
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginViewLeftMargin;

@end

@implementation JKLoginRegisterViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.isRegisterIn) {
        self.loginViewLeftMargin.constant = - JKScreenW;
        self.registerLoginButton.selected = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    //将背景放在最底层
    [self.view insertSubview:self.bgImageView atIndex:0];
    
    [[JKTopWindowViewController sharedTopWindow] setStatusBarStyle:(UIStatusBarStyleLightContent)];
}

//退出登录注册页面
- (IBAction)dismissClick {
    [self dismissViewControllerAnimated:YES completion:^{
        [[JKTopWindowViewController sharedTopWindow] setStatusBarStyle:(UIStatusBarStyleDefault)];
    }];

}

//点击右上角注册帐号按钮
- (IBAction)registerClick:(UIButton *)button {
    if (self.loginViewLeftMargin.constant == 0) {//间距为0，切换到注册页面
        
        self.loginViewLeftMargin.constant = - JKScreenW;
        
        button.selected = YES;  //右上角显示‘已有帐号？’
        
    }else {//切换到登录页面
        
        self.loginViewLeftMargin.constant = 0;
        
        button.selected = NO;  //右上角显示‘注册帐号’
    }
    
    //设置动画
    [UIView animateWithDuration:0.25 animations:^{
        //立即更新子控件
        [self.view layoutIfNeeded];
    }];
}

//状态栏高亮
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

//退出键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

//退出键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    
    return YES;
}

- (BOOL)shouldAutorotate{
    return NO;
}
@end
