//
//  JKFriendTrendsViewController.m
//  Baisi
//
//  Created by albert on 16/3/13.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKFriendTrendsViewController.h"
#import "JKRecommendViewController.h"
#import "JKLoginRegisterViewController.h"

@interface JKFriendTrendsViewController ()

@end

@implementation JKFriendTrendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //设置导航栏内容
    //通过initWithImage设置的UIImageView的尺寸就和图片的尺寸一样了
    self.navigationItem.title = @"我的关注";
    
//    self.title = @"我的关注";
//    //self.title就相当于下面两句
//    self.navigationItem.title = @"我的关注";
//    self.tabBarItem.title = @"我的关注";

    //设置导航栏leftBarButtonItem为自定义按钮
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:@"friendsRecommentIcon" highImage:@"friendsRecommentIcon-click" target:self action:@selector(friendButtonClick)];
    
    //设置背景颜色
    self.view.backgroundColor = JKGlobalBgColor;
}

- (void)friendButtonClick{
    
    JKRecommendViewController *recVc = [[JKRecommendViewController alloc] init];
    
    [self.navigationController pushViewController:recVc animated:YES];
}

- (IBAction)loginRegister {
    //alloc init默认就是加载同名的xib
    JKLoginRegisterViewController *loginRegisterVc = [[JKLoginRegisterViewController alloc] init];
    
    [self.navigationController presentViewController:loginRegisterVc animated:YES completion:nil];
}

- (IBAction)registerIn {
    JKLoginRegisterViewController *loginRegisterVc = [[JKLoginRegisterViewController alloc] init];
    loginRegisterVc.isRegisterIn = YES;
    [self.navigationController presentViewController:loginRegisterVc animated:YES completion:nil];
}
@end
