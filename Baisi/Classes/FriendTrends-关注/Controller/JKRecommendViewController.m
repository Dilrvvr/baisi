//
//  JKRecommendViewController.m
//  Baisi
//
//  Created by albert on 16/3/15.
//  Copyright (c) 2016年 albert. All rights reserved.
//  推荐关注

#import "JKRecommendViewController.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "MJExtension.h"
#import "JKRecommendCategoryCell.h"
#import "JKRecommendCategoryModel.h"
#import "JKRecommendUserCell.h"
#import "JKRecommendUserModel.h"
#import "MJRefresh.h"

//左侧选中cell对应的模型数据
#define JKSelectedCategory self.categories[self.categoryTableView.indexPathForSelectedRow.row]

@interface JKRecommendViewController () <UITableViewDataSource, UITableViewDelegate>
/** 左边的类型数据 */
@property (nonatomic,strong) NSArray *categories;
/** 请求参数 */
@property (nonatomic,strong) NSMutableDictionary *params;
/** AFN请求管理者 */
@property (nonatomic,strong) AFHTTPSessionManager *manager;

@property (weak, nonatomic) IBOutlet UITableView *categoryTableView;
@property (weak, nonatomic) IBOutlet UITableView *userTableView;

@end

@implementation JKRecommendViewController

static NSString * const categoryId = @"category";
static NSString * const userId = @"user";

- (AFHTTPSessionManager *)manager{
    if (_manager == nil) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}

#pragma mark - 控制器的销毁
- (void)dealloc{
    //停止所有操作
    [self.manager.operationQueue cancelAllOperations];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //初始化控件
    [self setupTableView];
    
    //添加刷新控件
    [self setupRefresh];
    
    //加载左侧的类别数据
    [self loadCategories];
}

/**
 *  加载左侧的类别数据
 */
- (void)loadCategories{
    //网络请求
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"a"] = @"category";
    params[@"c"] = @"subscribe";
    [self.manager GET:@"http://api.budejie.com/api/api_open.php" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //隐藏指示器
        [SVProgressHUD dismiss];
        
        self.categories = [JKRecommendCategoryModel mj_objectArrayWithKeyValuesArray:responseObject[@"list"]];
        
        //刷新表格
        [self.categoryTableView reloadData];
        
        //默认选中首行
        [self.categoryTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:(UITableViewScrollPositionTop)];
        //再配合下面这个，就可以完美选中首行了
        //[self tableView:self.categoryTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        //这样也可以，让它开始下拉刷新
        [self.userTableView.mj_header beginRefreshing];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //显示失败信息
        [JKProgressHUD showErrorWithStatus:@"加载推荐信息失败"];
    }];
}

/**
 *  初始化控件
 */
- (void)setupTableView{
    self.userTableView.rowHeight = 66;
    
    //注册，用NSStringFromClass就不用这样写死了@"JKRecommendCategoryCell"
    [self.categoryTableView registerNib:[UINib nibWithNibName:NSStringFromClass([JKRecommendCategoryCell class]) bundle:nil] forCellReuseIdentifier:categoryId];
    [self.userTableView registerNib:[UINib nibWithNibName:NSStringFromClass([JKRecommendUserCell class]) bundle:nil] forCellReuseIdentifier:userId];
    
    self.navigationItem.title = @"推荐关注";
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //设置背景颜色
    self.view.backgroundColor = JKGlobalBgColor;
    
    
    self.categoryTableView.backgroundColor = [UIColor clearColor];
    
    //显示指示器
    [JKProgressHUD show];
}

/**
 *  添加下拉刷新控件
 */
- (void)setupRefresh{
    
    //添加头部下拉刷新控件
    self.userTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewUsers)];
    
    //self.userTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        //此处加载更多数据
    //}];
    
    //一般还是用这个target
    self.userTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreUsers)];
    
    //一开始就先隐藏这个刷新空间，否则在网速慢的时候，一开始会显示在顶部
    //在numberOfRowsInSection方法中控制刷新控件的显示和隐藏
    
    //在返回cell数量的时候设置就ok了
    //self.userTableView.mj_footer.hidden = YES;
}

#pragma mark - 下拉刷新
- (void)loadNewUsers{
    
    JKRecommendCategoryModel *category = JKSelectedCategory;
    
    category.currentPage = 1;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"a"] = @"list";
    params[@"c"] = @"subscribe";
    params[@"category_id" ] = category.ID;//和“推荐关注”页面中左侧关注标签API返回值中的id参数对应
    params[@"page"] = @(category.currentPage);
    
    self.params = params;
    
    [self.manager GET:@"http://api.budejie.com/api/api_open.php" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //字典数组 -> 模型数组
        NSArray *users = [JKRecommendUserModel mj_objectArrayWithKeyValuesArray:responseObject[@"list"]];
        
        //保存用户总数
        category.total= responseObject[@"total"];
        
        //移除所有旧数据
        [category.users removeAllObjects];
        
        //添加到当前类别对应的用户数组中
        [category.users addObjectsFromArray:users];
        
        //不是最后一次请求，params还是上一个params，但是self.params已经重新赋值？
        //连续点击，之前的数据回来的话，还是要先给它存起来，再次点击回去的时候，直接刷新表格
        //不能浪费流量
        if (self.params != params) return;
        
        //刷新右边的表格
        [self.userTableView reloadData];
        
        //结束刷新状态
        [self.userTableView.mj_header endRefreshing];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (self.params != params) return;
        //提醒
        [JKProgressHUD showErrorWithStatus:@"加载用户数据失败"];
        
        //结束刷新状态
        [self.userTableView.mj_header endRefreshing];
    }];
}

#pragma mark - 加载更多用户数据
- (void)loadMoreUsers{
    JKRecommendCategoryModel *category = JKSelectedCategory;
    
    //发送请求给服务器，加载右侧的数据
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"a"] = @"list";
    params[@"c"] = @"subscribe";
    params[@"category_id" ] = category.ID;//和“推荐关注”页面中左侧关注标签API返回值中的id参数对应
    params[@"page"] = @(++category.currentPage);
    
    self.params = params;
    
    [self.manager GET:@"http://api.budejie.com/api/api_open.php" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //字典数组 -> 模型数组
        NSArray *users = [JKRecommendUserModel mj_objectArrayWithKeyValuesArray:responseObject[@"list"]];
        
        //添加到当前类别对应的用户数组中
        [category.users addObjectsFromArray:users];
        
        //不是最后一次请求，params还是上一个params，但是self.params已经重新赋值？
        if (self.params != params) return;
        
        //刷新右边的表格
        [self.userTableView reloadData];
        
        //结束刷新状态
        [self.userTableView.mj_footer endRefreshing];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (self.params != params) return;
        
        //提醒
        [JKProgressHUD showErrorWithStatus:@"加载用户数据失败"];
        
        //结束刷新状态
        [self.userTableView.mj_footer endRefreshing];
    }];
}

#pragma mark - <UITableViewDataSource>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.categoryTableView) {//左边的类别表格
        return self.categories.count;
    }else{//右边的用户表格
        
        //数组中的数量
        NSInteger count = [JKSelectedCategory users].count;
        
        //总的用户数量
        NSInteger total = [[JKSelectedCategory total] integerValue];
        
        //别忘了最开始两个数都为0
        self.userTableView.mj_footer.hidden = (count == total);
        
        return count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.categoryTableView) {//左边的类别表格
        JKRecommendCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:categoryId];
        
        cell.category = self.categories[indexPath.row];
        
        return cell;
        
    }else{//右边的用户表格
        JKRecommendUserCell *cell = [tableView dequeueReusableCellWithIdentifier:userId];
        
        cell.user = [JKSelectedCategory users][indexPath.row];
        
        return cell;
    }
}

#pragma mark - <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //结束刷新状态
    [self.categoryTableView.mj_header endRefreshing];
    [self.userTableView.mj_footer endRefreshing];
    
    JKRecommendCategoryModel *category = JKSelectedCategory;
    
    //如果已经加载过数据
    if (category.users.count) {
        //显示曾经的数据
        [self.userTableView reloadData];
    }else{//发送请求给服务器，加载右侧的数据
        //马上刷新数据。解决网络慢时，点击另一行，右边数据还停留在上一个数据
        [self.userTableView reloadData];
        
        //进入下拉刷新状态,刷新数据
        [self.userTableView.mj_header beginRefreshing];
    }
}
/**
 1.目前只能显示1页数据
 2.重复发送请求，浪费用户流量
 3.网速慢带来的细节问题
 */
@end
