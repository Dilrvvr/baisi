//
//  JKLoginRegisterViewController.h
//  Baisi
//
//  Created by albert on 16/3/17.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKLoginRegisterViewController : UIViewController
/** 是否点击的注册按钮 */
@property (nonatomic, assign) BOOL isRegisterIn;
@end
