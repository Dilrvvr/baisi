//
//  JKShowPictureViewController.h
//  Baisi
//
//  Created by albert on 16/3/22.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKPostModel;

@interface JKShowPictureViewController : UIViewController
/** 数据模型 */
@property (nonatomic, strong) JKPostModel *post;

+ (void)showWithPost:(JKPostModel *)post touchPoint:(CGPoint)touchPoint sourceVc:(UIViewController *)sourceVc;
@end


@interface JKShowPicturePresentationManager : NSObject <UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning>

/** touchPoint */
@property (nonatomic, assign) CGPoint touchPoint;

+ (JKShowPicturePresentationManager *)shared;

+ (void)presentAnimationWithView:(UIView *)animationView duration:(CGFloat)duration transitionContext:(id<UIViewControllerContextTransitioning>)transitionContext;

+ (void)dismissAnimationWithView:(UIView *)animationView duration:(CGFloat)duration transitionContext:(id<UIViewControllerContextTransitioning>)transitionContext;

+ (void)animationWithView:(UIView *)animationView duration:(CGFloat)duration transitionContext:(id<UIViewControllerContextTransitioning>)transitionContext isPresent:(BOOL)isPresent touchPoint:(CGPoint)touchPoint;
@end
