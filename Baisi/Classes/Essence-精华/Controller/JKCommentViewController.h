//
//  JKCommentViewController.h
//  Baisi
//
//  Created by albert on 16/3/26.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKPostCell, JKPostModel;

@interface JKCommentViewController : UIViewController
/** 数据模型 */
@property (nonatomic, strong) JKPostModel *post;

/** 列表页点进来的cell */
@property (nonatomic, weak) JKPostCell *homeSelectCell;
@end
