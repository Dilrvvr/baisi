//
//  JKRecommendTagTableViewController.m
//  Baisi
//
//  Created by albert on 16/3/16.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKRecommendTagTableViewController.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "MJExtension.h"
#import "JKRecommendTagModel.h"
#import "JKRecommendTagCell.h"

@interface JKRecommendTagTableViewController ()
/** 所有请求的管理者 */
@property (nonatomic,strong) AFHTTPSessionManager *manager;

/** 模型数组 */
@property (nonatomic,strong) NSArray *tags;
@end

@implementation JKRecommendTagTableViewController

static NSString * const tagID = @"tag";

//懒加载
- (AFHTTPSessionManager *)manager{
    if (_manager == nil) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //初始化控件
    [self setupTableView];
    
    //发送请求,加载数据
    [self loadData];
    
}

/**
 *  发送请求,加载数据
 */
- (void)loadData{
    //发送请求
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"a"] = @"tag_recommend";
    params[@"action"] = @"sub";
    params[@"c"] = @"topic";
    [self.manager GET:@"http://api.budejie.com/api/api_open.php" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //隐藏加载的圈圈
        [SVProgressHUD dismiss];
        
        //字典转模型
        self.tags = [JKRecommendTagModel mj_objectArrayWithKeyValuesArray:responseObject];
        
        //记得刷新表格！！
        [self.tableView reloadData];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [JKProgressHUD showErrorWithStatus:@"数据加载失败"];
    }];
}

/**
 *  初始化控件
 */
- (void)setupTableView{
    //设置标题
    self.navigationItem.title = @"推荐标签";
    
    //设置背景颜色
    self.tableView.backgroundColor = JKGlobalBgColor;
    
    //去除分割线
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //注册
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([JKRecommendTagCell class]) bundle:nil] forCellReuseIdentifier:tagID];
    
    //设置cell行高
    self.tableView.rowHeight = 66;
    
    //加载的圈圈
    [JKProgressHUD showWithLoadingIsAllowUserAction:YES];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tags.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    JKRecommendTagCell *cell = [tableView dequeueReusableCellWithIdentifier:tagID];
    
    //取出模型数据
    cell.tagModel = self.tags[indexPath.row];
    
    return cell;
}
@end
