//
//  JKBasePostTableViewController.h
//  Baisi
//
//  Created by albert on 16/3/21.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKBasePostTableViewController : UIViewController
/** 模型数组 */
@property (nonatomic,strong) NSMutableArray *posts;

/** 帖子类型 */
@property (nonatomic, assign) JKPostType postType;

/** tableView */
@property (nonatomic, weak) UITableView *tableView;

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
@end
