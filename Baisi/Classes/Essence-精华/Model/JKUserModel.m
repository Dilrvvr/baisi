//
//  JKUserModel.m
//  Baisi
//
//  Created by albert on 16/3/26.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKUserModel.h"

@implementation JKUserModel

- (void)setTotal_cmt_like_count:(NSString *)total_cmt_like_count{
    _total_cmt_like_count = [total_cmt_like_count copy];
    
    _total_cmt_like_bgColor = JKColor(169, 188, 215);
    
    NSInteger count = [_total_cmt_like_count integerValue];
    
    if (count < 1000) {
        
        _total_cmt_like_des = _total_cmt_like_count;
        
    }else{
        
        NSInteger num = count / 100;
        _total_cmt_like_des = [NSString stringWithFormat:@"%.1fk", (num * 1.0 / 10.0)];
        
        if ([_total_cmt_like_des containsString:@".0k"]) {
            _total_cmt_like_des = [_total_cmt_like_des stringByReplacingCharactersInRange:[_total_cmt_like_des rangeOfString:@".0k"] withString:@"k"];
        }
        
        _total_cmt_like_bgColor = (count >= 10000) ?  JKColor(255, 175, 181) : JKColor(198, 186, 240);
    }
}
@end
