//
//  JKPostModel.h
//  Baisi
//
//  Created by albert on 16/3/20.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKCommentModel;

@interface JKPostModel : NSObject
/** id */
@property (nonatomic, copy) NSString *ID;
/** 头像URL */
@property (nonatomic, copy) NSString *profile_image;
/** 昵称 */
@property (nonatomic, copy) NSString *name;
/** 段子内容 */
@property (nonatomic, copy) NSString *text;
/** 段子内容 */
@property (nonatomic, copy) NSAttributedString *attrText;
/** 发帖时间 */
@property (nonatomic, copy) NSString *create_time;
/** 顶的数量 */
@property (nonatomic, strong) NSNumber *ding;
/** 踩的数量 */
@property (nonatomic, strong) NSNumber *cai;
/** 评论数量 */
@property (nonatomic, strong) NSNumber *comment;
/** 转发数量 */
@property (nonatomic, strong) NSNumber *repost;

/** 微信分享url */
@property (nonatomic, copy) NSString *weixin_url;

/** 是否新浪加V用户 */
@property (nonatomic, assign, getter=isSina_v) BOOL sina_v;

/** 小图片URL */
@property (nonatomic, copy) NSString *small_image;
/** 大图片URL */
@property (nonatomic, copy) NSString *large_image;
/** 中图片URL */
@property (nonatomic, copy) NSString *middle_image;
/** 图片宽度 */
@property (nonatomic, strong) NSNumber *width;
/** 图片高度 */
@property (nonatomic, strong) NSNumber *height;
/** 是否gif */
@property (nonatomic, assign, getter=isGif) BOOL is_gif;

/** 帖子类型 */
@property (nonatomic, assign) JKPostType type;

/** 播放次数 */
@property (nonatomic, copy) NSString *playcount;
/** 声音时长 */
@property (nonatomic, strong) NSNumber *voicetime;
/** 声音的URL */
@property (nonatomic, copy) NSString *voiceuri;

/** 视频时长 */
@property (nonatomic, strong) NSNumber *videotime;
/** 视频的URL */
@property (nonatomic, copy) NSString *videouri;

/** 最热评论（期望这个数组中存放的是JKCommentModel模型） */
//@property (nonatomic, strong) NSArray *top_cmt;
/** 改为最热评论模型 */
@property (nonatomic, strong) JKCommentModel *top_cmt;
@property (nonatomic, copy) NSAttributedString *top_cmtAttr;


/****** 额外的属性 ******/
/** cell行高 */
@property (nonatomic, assign, readonly) CGFloat cellHeight;
/** 图片的frame */
@property (nonatomic, assign, readonly) CGRect pictureFrame;
/** 图片是否太大 */
@property (nonatomic, assign, getter=isPictureTooBig) BOOL pictureTooBig;
/** 图片的下载进度 */
@property (nonatomic, assign) CGFloat pictureProgress;
/** 是否正在下载 */
@property (nonatomic, assign) BOOL isDownloadingVideo;
/** 视频的下载进度 */
@property (nonatomic, assign) CGFloat videoDownloadProgress;


/** 声音的frame */
@property (nonatomic, assign, readonly) CGRect voiceFrame;
/** 视频的frame */
@property (nonatomic, assign, readonly) CGRect videoFrame;


/** 缓存的文件名 */
@property (nonatomic, copy) NSString *fileName;

/** 缓存的文件大小 */
@property (nonatomic, copy) NSString *fileSize;

+ (NSCache *)topImageCache;
@end
