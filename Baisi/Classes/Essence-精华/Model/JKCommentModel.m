//
//  JKCommentModel.m
//  Baisi
//
//  Created by albert on 16/3/26.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKCommentModel.h"
#import <MJExtension/MJExtension.h>

@implementation JKCommentModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"ID" : @"id"
             };
}

- (void)setContent:(NSString *)content{
    _content = [content copy];
    
    
    
    NSMutableParagraphStyle *para = [[NSMutableParagraphStyle alloc] init];
    para.lineSpacing = 5;
    
    _contentAttr = [[NSAttributedString alloc] initWithString:_content attributes:@{NSParagraphStyleAttributeName : para}];
}
@end
