//
//  JKPostModel.m
//  Baisi
//
//  Created by albert on 16/3/20.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKPostModel.h"
#import "NSDate+JKExtension.h"
#import "MJExtension.h"
#import "JKCommentModel.h"
#import "JKUserModel.h"

@interface JKPostModel ()

@end

static NSCache *topImageCache_;

@implementation JKPostModel
{
    CGFloat _cellHeight;
}

+ (NSCache *)topImageCache{
    
    if (topImageCache_ == nil) {
        
        topImageCache_ = [[NSCache alloc] init];
    }
    
    return topImageCache_;
}

//数组属性中装什么对象类型
//+ (NSDictionary *)mj_objectClassInArray{
//    
//    //return @{@"top_cmt" : [JKCommentModel class]};
//    
//    //不想依赖导入头文件的话，也可以直接字符串
//    return @{@"top_cmt" : @"JKCommentModel"};
//}

//替换属性名称
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"small_image" : @"image0",
             @"large_image" : @"image1",
             @"midlle_image" : @"image2",
             @"ID" : @"id",
             @"top_cmt" : @"top_cmt[0]"
             //@"qzone_id" : @"top_cmt[0].user.qzone_id"
             };
}

//+ (NSString *)mj_replacedKeyFromPropertyName121:(NSString *)propertyName{
//    
//    //要求属性 propertyName == myName == myHeight
//    //但是服务器返回的是  my_name   my_height
//    //用这个很方便？
//    if ([propertyName isEqualToString:@"ID"]) return @"id";
//    
//    return propertyName;
//}

- (void)setFileSize:(NSString *)fileSize{
    
    _fileSize = ([fileSize integerValue] / 1024.0 / 1024.0 <= 0.01) ? @"0.01" : [NSString stringWithFormat:@"%.2f", [fileSize integerValue] / 1024.0 / 1024.0];
}

//判断如果包含中文
- (BOOL)containChinese:(NSString *)str {
    
    for (int i = 0; i < [str length]; i++) {
        
        int a = [str characterAtIndex:i];
        
        if (a > 0x4e00 && a < 0x9fff) {
            
            return YES;
        }
    }
    return NO;
}

- (CGFloat)cellHeight{
    //只算一遍
    if (!_cellHeight) {
        
        NSMutableParagraphStyle *para = [[NSMutableParagraphStyle alloc] init];
        para.lineSpacing = 8;
        
        UIFont *font = [UIFont systemFontOfSize:17];
        
        //计算文字的高度
        CGSize maxSize = CGSizeMake((MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)) - 20, MAXFLOAT);
        CGFloat textH = [self.text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : font, NSParagraphStyleAttributeName : para} context:nil].size.height;
        
        _attrText = [[NSAttributedString alloc] initWithString:_text attributes:@{NSParagraphStyleAttributeName : para}];
        
        if (textH - font.lineHeight <= 8) {
            
            if ([self containChinese:self.text]) {
                textH-=8;
                
                _attrText = [[NSAttributedString alloc] initWithString:_text attributes:nil];
            }
        }
        
        //计算cell的高度，这些可以写成常量
        //文字部分高度
        _cellHeight = JKPostCellTextY + textH + JKPostCellMargin;
        if (self.type == kPostTypePicture) {
            if (self.width != 0 && self.height != 0) {
                //图片需要显示的宽度
                CGFloat pictureW = maxSize.width;
                //图片需要显示的高度
                CGFloat pictureH = pictureW * [self.height floatValue] / [self.width floatValue];
                
                // 忽略大写lowercaseString
                BOOL isGif = [_large_image.pathExtension.lowercaseString isEqualToString:@"gif"];
                
                if (pictureH >= JKPostCellPictureMaxH && !isGif) {
                    pictureH = pictureW;
                    self.pictureTooBig = YES;
                }
                
                //计算图片的frame
                CGFloat pictureX = JKPostCellMargin;
                CGFloat pictureY = JKPostCellTextY + textH + JKPostCellMargin;
                _pictureFrame = CGRectMake(pictureX, pictureY, pictureW, pictureH);
                
                //再加上下面的一个间距
                _cellHeight += pictureH + JKPostCellMargin;
            }
            
        }else if (self.type == kPostTypeVoice){//声音帖子
            CGFloat voiceW = maxSize.width;
            CGFloat voiceH = voiceW * [self.height floatValue] / [self.width floatValue];
            CGFloat voiceX = JKPostCellMargin;
            CGFloat voiceY = JKPostCellTextY + textH +JKPostCellMargin;
            
            _voiceFrame = CGRectMake(voiceX, voiceY, voiceW, voiceH);
            
            //再加上下面的一个间距
            _cellHeight += voiceH + JKPostCellMargin;
            
        }else if (self.type == kPostTypeVideo){//视频帖子
            CGFloat videoW = maxSize.width;
            CGFloat videoH = videoW * [self.height floatValue] / [self.width floatValue];
            CGFloat videoX = JKPostCellMargin;
            CGFloat videoY = JKPostCellTextY + textH +JKPostCellMargin;
            
            _videoFrame = CGRectMake(videoX, videoY, videoW, videoH);
            
            //再加上下面的一个间距
            _cellHeight += videoH + JKPostCellMargin;
        }
        
        //获取评论模型
        JKCommentModel *comment = self.top_cmt;
        //评论内容
        NSString *content = [NSString stringWithFormat:@"%@: %@", comment.user.username, comment.content];
        
        _top_cmtAttr = [[NSAttributedString alloc] initWithString:content attributes:@{NSParagraphStyleAttributeName : para}];
        
        //评论内容的高度
        CGFloat contentH = [content boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15], NSParagraphStyleAttributeName : para} context:nil].size.height;
        if (comment) {//有最热评论的时候，cell高度加上评论的高度
            _cellHeight += JKPostCellMargin * 2 + JKPostCellTopCmtTitleH + contentH;
        }
        
        //底部工具条高度
        _cellHeight += JKPostCellMargin + JKPostCellBottomBar;
    }
    
    return _cellHeight;
}

- (NSString *)create_time{
    //时间格式
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    //帖子的创建时间
    //_create_time = @"2015-12-31 21:40:29";
    NSDate *create = [fmt dateFromString: _create_time];
    if (create.isThisYear) {//今年
        
        if (create.isToday) {//今天
            //日历是currentCalendar！！！
            //NSCalendar *calendar = [NSCalendar currentCalendar];
            //NSDateComponents *cmps = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:create toDate:[NSDate date] options:0];

            //也可以直接用分类的类方法或对象方法直接计算时间差
            //NSDateComponents *cmps = [NSDate deltaFromeDate:create toDate:[NSDate date]];
            NSDateComponents *cmps = [[NSDate date] deltaFromDate:create];
            
            if (cmps.hour >= 1) {//1小时以上
                fmt.dateFormat = [NSString stringWithFormat:@"%ld小时前", (long)cmps.hour];
                return [fmt stringFromDate:create];
                
            }else if(cmps.minute >= 1){//1小时以内，1分钟以上
                fmt.dateFormat = [NSString stringWithFormat:@"%ld分钟前", (long)cmps.minute];
                return [fmt stringFromDate:create];
                
            }else{//1分钟以内
                fmt.dateFormat = @"刚刚";
                return [fmt stringFromDate:create];
            }
            
        }else if(create.isYesterday){//昨天
            fmt.dateFormat = @"昨天 HH:mm:ss";
            return [fmt stringFromDate:create];
            
        }else{//其它
            fmt.dateFormat = @"MM-dd HH:mm:ss";
            return [fmt stringFromDate:create];
        }
        
    }else{//非今年
        return _create_time;
    }
}
@end
