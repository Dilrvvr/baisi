//
//  JKCommentModel.h
//  Baisi
//
//  Created by albert on 16/3/26.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JKUserModel;

@interface JKCommentModel : NSObject
/** 评论内容 */
@property (nonatomic, copy) NSString *content;
/** 评论内容 */
@property (nonatomic, copy) NSAttributedString *contentAttr;
/** 评论点赞人数 */
@property (nonatomic, strong) NSNumber *like_count;
/** 声音评论的时长 */
@property (nonatomic, strong) NSNumber *voicetime;
/** 声音评论的URL */
@property (nonatomic, copy) NSString *voiceuri;
/** 用户 */
@property (nonatomic, strong) JKUserModel *user;
/** 评论ID */
@property (nonatomic, copy) NSString *ID;
@end
