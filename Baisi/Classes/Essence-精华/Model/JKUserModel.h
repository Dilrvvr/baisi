//
//  JKUserModel.h
//  Baisi
//
//  Created by albert on 16/3/26.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKUserModel : NSObject
/** 用户名 */
@property (nonatomic, copy) NSString *username;
/** 用户头像 */
@property (nonatomic, copy) NSString *profile_image;
/** 性别 */
@property (nonatomic, copy) NSString *sex;
/** 是否vip */
@property (nonatomic, assign, getter=isVip) BOOL is_vip;

/** 评论总赞数 */
@property (nonatomic, copy) NSString *total_cmt_like_count;

/** 评论总赞数 应显示字符 */
@property (nonatomic, copy) NSString *total_cmt_like_des;

/** 评论总赞数 背景色 */
@property (nonatomic, strong) UIColor *total_cmt_like_bgColor;
@end
