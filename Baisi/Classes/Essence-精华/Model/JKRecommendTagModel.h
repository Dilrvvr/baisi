//
//  JKRecommendTagModel.h
//  Baisi
//
//  Created by albert on 16/3/16.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKRecommendTagModel : NSObject

/** 标签图片 */
@property (nonatomic,copy) NSString *image_list;
/** 订阅人数 */
@property (nonatomic,strong) NSNumber *sub_number;
/** 标签名称*/
@property (nonatomic,copy) NSString *theme_name;

@end
