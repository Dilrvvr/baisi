//
//  JKPostCell.h
//  Baisi
//
//  Created by albert on 16/3/20.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JKPostVideoView.h"

@class JKPostModel;

@interface JKPostCell : UITableViewCell

/** 数据模型 */
@property (nonatomic,strong) JKPostModel *post;

/** 行高 */
@property (nonatomic, assign) CGFloat rowHeight;

/** 段子内容 */
@property (weak, nonatomic) IBOutlet UILabel *contentTextLabel;

/** moreClickBlock */
@property (nonatomic, copy) void (^moreClickBlock)(JKPostCell *cell);

/** 视频帖子中间的内容 */
@property (nonatomic, weak) JKPostVideoView *videoView;

/** 避免设置图片 */
@property (nonatomic, assign) BOOL avoidSetImage;
@end
