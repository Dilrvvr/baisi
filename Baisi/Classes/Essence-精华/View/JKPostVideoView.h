//
//  JKPostVideoView.h
//  Baisi
//
//  Created by albert on 16/3/26.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKPostModel;

@interface JKPostVideoView : UIView
/** 数据模型 */
@property (nonatomic, strong) JKPostModel *post;

/** 避免设置图片 */
@property (nonatomic, assign) BOOL avoidSetImage;

/** 下载视频的block */
@property (nonatomic, copy) void (^downloadVideoButtonClickBlock)();

- (void)downloadVideo;
@end
