//
//  JKCommentCell.h
//  Baisi
//
//  Created by albert on 16/3/27.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKCommentModel;

@interface JKCommentCell : UITableViewCell

/** 评论数据模型 */
@property (nonatomic, strong) JKCommentModel *comment;

@end
