//
//  JKRecommendTagCell.h
//  Baisi
//
//  Created by albert on 16/3/16.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKRecommendTagModel;

@interface JKRecommendTagCell : UITableViewCell
/** 模型数据 */
@property (nonatomic,strong) JKRecommendTagModel *tagModel;
@end
