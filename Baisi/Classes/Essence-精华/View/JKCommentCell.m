//
//  JKCommentCell.m
//  Baisi
//
//  Created by albert on 16/3/27.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKCommentCell.h"
#import "JKCommentModel.h"
#import "JKUserModel.h"
#import "UIImageView+WebCache.h"

@interface JKCommentCell ()
/** 头像 */
@property (weak, nonatomic) IBOutlet UIImageView *profile_imageView;
/** 用户名 */
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
/** 评论内容 */
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
/** 点赞数量 */
@property (weak, nonatomic) IBOutlet UILabel *like_countLabel;
/** 性别图标 */
@property (weak, nonatomic) IBOutlet UIImageView *sexImageView;
/** 声音按钮 */
@property (weak, nonatomic) IBOutlet UIButton *voiceButton;


/** 评论总赞数label */
@property (weak, nonatomic) IBOutlet UILabel *total_cmt_like_countLabel;

@end

@implementation JKCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //设置背景图片
//    UIImageView *bgView = [[UIImageView alloc] init];
//    bgView.image = [UIImage imageNamed:@"mainCellBackground"];
//    self.backgroundView = bgView;
    
    self.total_cmt_like_countLabel.layer.cornerRadius = 3;
    self.total_cmt_like_countLabel.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setComment:(JKCommentModel *)comment{
    _comment = comment;
    
    //头像
    [self.profile_imageView setHeaderIcon:comment.user.profile_image];
    
    self.total_cmt_like_countLabel.text = _comment.user.total_cmt_like_des;
    self.total_cmt_like_countLabel.backgroundColor = _comment.user.total_cmt_like_bgColor;
    
    //用户名
    self.usernameLabel.text = comment.user.username;
    
    //评论内容
    self.contentLabel.attributedText = comment.contentAttr;
    
    //性别图标
    if ([self.comment.user.sex isEqualToString:JKUserSexMale]) {
        self.sexImageView.transform = CGAffineTransformIdentity;
        self.sexImageView.image = [UIImage imageNamed:@"Profile_manIcon"];
    }else{//女的顺时针旋转45°
        self.sexImageView.transform = CGAffineTransformMakeRotation(M_PI_4);
        self.sexImageView.image = [UIImage imageNamed:@"Profile_womanIcon"];
    }
    
    //点赞数量
    if ([comment.like_count integerValue]) {
        self.like_countLabel.text = [NSString stringWithFormat:@"%@", comment.like_count];
    }
    
    //使用字符串的length判断，因为空字符串也是有值
    if (comment.voiceuri.length) {
        self.voiceButton.hidden = NO;
        [self.voiceButton setTitle:[NSString stringWithFormat:@"%@''", comment.voicetime] forState:(UIControlStateNormal)];
    }else{
        self.voiceButton.hidden = YES;
    }
}

//- (void)setFrame:(CGRect)frame{
//    
//    frame.size.width = JKScreenW - JKPostCellMargin * 2;
//    frame.origin.x = JKPostCellMargin;
//    
//    [super setFrame:frame];
//}

#pragma mark - UIMenuController
/** 能成为第一响应者 */
- (BOOL)canBecomeFirstResponder{
    return YES;
}

/** 能执行哪些操作（copy、paste...） */
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    //让控制器称为第一响应者必须这样？？！！
    //if (action == @selector(ding:) || action == @selector(respond:) || action == @selector(report:)) return YES;
    return NO;
}
@end
