//
//  JKResponderView.m
//  Baisi
//
//  Created by albert on 2018/3/20.
//  Copyright © 2018年 安永博. All rights reserved.
//

#import "JKResponderView.h"

@implementation JKResponderView


#pragma mark - UIMenuController
/** 能成为第一响应者 */
- (BOOL)canBecomeFirstResponder{
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    return NO;
}

@end
