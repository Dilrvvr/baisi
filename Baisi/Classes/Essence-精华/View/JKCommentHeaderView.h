//
//  JKCommentHeaderView.h
//  Baisi
//
//  Created by albert on 16/3/27.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKCommentHeaderView : UITableViewHeaderFooterView

/** 标题文字 */
@property (nonatomic, copy) NSString *headerTitle;

+ (instancetype)headerViewWithTableView:(UITableView *)tableView;
@end
