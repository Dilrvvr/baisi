//
//  JKProgressView.m
//  Baisi
//
//  Created by albert on 16/3/22.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKProgressView.h"

@implementation JKProgressView

//设置一些属性
- (void)awakeFromNib{
    [super awakeFromNib];
    
    //设置圆角
    self.roundedCorners = 2;
    
    //设置进度文字颜色
    self.progressLabel.textColor = [UIColor whiteColor];
}

//重写父类方法
- (void)setProgress:(CGFloat)progress animated:(BOOL)animated{

    //设置进度文字内容。%% 转义为一个 %
    //NSString *progressStr = [NSString stringWithFormat:@"%.0f", progress * 100];
    //使用 %.0f 会出现 -0 情况，即进度条会显示一个 -0%
    // 可以将progress转为字符串，然后将负号@"-"转为空字符@""
    //progressStr = [progressStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    //也可以直接使用 ld%,注意强制转换只转换后面的第一个数，要把(progress * 100)括起来
    self.progressLabel.text = [NSString stringWithFormat:@"%ld%%", (long)(progress * 100)];
    
    [super setProgress:progress animated:animated];
}
@end
