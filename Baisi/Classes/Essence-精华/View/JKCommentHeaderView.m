//
//  JKCommentHeaderView.m
//  Baisi
//
//  Created by albert on 16/3/27.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKCommentHeaderView.h"

@interface JKCommentHeaderView ()
/** label */
@property (nonatomic, weak) UILabel *label;
@end

@implementation JKCommentHeaderView

+ (instancetype)headerViewWithTableView:(UITableView *)tableView{
    static NSString *ID = @"header";
    JKCommentHeaderView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if (header == nil) {//缓存池中没有，自己创建
        header = [[JKCommentHeaderView alloc] initWithReuseIdentifier:ID];
    }
    return header;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        
        //直接设置header的背景方法已经过期了，header也有个contentView
        //header.backgroundColor = JKGlobalBgColor;
        self.contentView.backgroundColor = JKColor(233, 233, 233);
        
        UILabel *label = [[UILabel alloc] init];
        //label属性
        label.textColor = JKColor(97, 97, 97);
        label.width = 200;
        label.x = JKPostCellMargin * 0.5;
        //自动与父控件拉伸
        label.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        
        [self.contentView addSubview:label];
        
        self.label = label;
    }
    return self;
}

- (void)setHeaderTitle:(NSString *)headerTitle{
    //copy的属性要这样写
    _headerTitle = [headerTitle copy];
    
    self.label.text = self.headerTitle;
}
@end
