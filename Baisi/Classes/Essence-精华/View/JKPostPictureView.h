//
//  JKPostPictureView.h
//  Baisi
//
//  Created by albert on 16/3/21.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKPostModel;

@interface JKPostPictureView : UIView

/** 数据模型 */
@property (nonatomic,strong) JKPostModel *post;

@end
