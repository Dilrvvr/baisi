//
//  JKPostVoiceView.m
//  Baisi
//
//  Created by albert on 16/3/26.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKPostVoiceView.h"
#import "JKPostModel.h"
#import "UIImageView+WebCache.h"
#import <AVFoundation/AVFoundation.h>
#import "JKShowPictureViewController.h"
#import "VideoPlayView.h"
#import "FullViewController.h"
#import "JKDraggingVideoView.h"

@interface JKPostVoiceView () <VideoPlayViewDelegate>
@property (weak, nonatomic) IBOutlet JKAnimatedImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *playCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *voiceTimeLabel;
@property (weak, nonatomic) UIButton *playButton;
/** 音频播放器 */
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic, weak) VideoPlayView *playView;
@property (nonatomic, strong) FullViewController *fullVc;

@end

@implementation JKPostVoiceView

- (FullViewController *)fullVc{
    if (_fullVc == nil) {
        _fullVc  = [[FullViewController alloc] init];
    }
    
    return _fullVc;
}

- (AVAudioPlayer *)audioPlayer{
    if (_audioPlayer) {
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:self.post.voiceuri] error:nil];
    }
    return _audioPlayer;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
    //给图片添加点击事件
    self.imageView.userInteractionEnabled = YES;
    [self.imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPicture:)]];
    
    //发现控件明明设置好了，但是显示效果却有问题，就有可能是Autoresizing问题
    self.autoresizingMask = UIViewAutoresizingNone;
}

//模态窗口显示图片
- (void)showPicture:(UITapGestureRecognizer *)tap{
    
    CGPoint location = [tap locationInView:tap.view];
    
    location = [tap.view convertPoint:location toView:[UIApplication sharedApplication].delegate.window];
    
    [JKShowPictureViewController showWithPost:self.post touchPoint:location sourceVc:nil];
}

- (void)setPost:(JKPostModel *)post{
    _post = post;
    
    //图片
    [self.imageView fl_setImageWithURL:[NSURL URLWithString:post.large_image]];
    
    //播放次数
    self.playCountLabel.text = [NSString stringWithFormat:@"%@播放",post.playcount];
    
    //声音时长
    NSInteger minute = [post.voicetime integerValue] / 60;
    NSInteger second = [post.voicetime integerValue] % 60;
    self.voiceTimeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", (long)minute, (long)second];
    
//    AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL URLWithString:post.voiceuri] error:nil];
//    [self.audioPlayer setValue:[NSURL URLWithString:post.voiceuri] forKeyPath:@"url"];
//    JKLog(@"%@",post. voiceuri);
}

//点击播放按钮
- (IBAction)playButtonClick:(UIButton *)button {
    
    [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
    
    //    [self.audioPlayer play];[self.playView resetPlayView];
    
    [JKDraggingVideoView pause];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:JKResetPlayerNotification object:nil];
    
    VideoPlayView *playView = [VideoPlayView videoPlayView];
//    playView.backgroundColor = nil;
    playView.frame = CGRectMake(0, 0, self.width, self.height);
    playView.imageView.image = self.imageView.image;
    playView.delegate = self;
    
    [self addSubview:playView];
    
    self.playView = playView;
    
    AVPlayerItem *item = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:self.post.voiceuri]];
    self.playView.playerItem = item;
    playView.toolView.alpha = 1;
}

#pragma mark VideoPlayViewDelegate 视频播放时窗口模式与全屏模式切换
- (void)videoplayViewSwitchOrientation:(BOOL)isFull{
    if (isFull) {
        [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:self.fullVc animated:NO completion:^{
            self.playView.frame = self.fullVc.view.bounds;
            [self.fullVc.view addSubview:self.playView];
        }];
    } else {
        [self.fullVc dismissViewControllerAnimated:NO completion:^{
            self.fullVc = nil;
            self.playView.frame = CGRectMake(0, 0, self.width, self.height);
            [self addSubview:self.playView];
        }];
    }
}
@end
