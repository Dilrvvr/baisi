//
//  JKRecommendTagCell.m
//  Baisi
//
//  Created by albert on 16/3/16.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKRecommendTagCell.h"
#import "JKRecommendTagModel.h"
#import "UIImageView+WebCache.h"

@interface JKRecommendTagCell ()
@property (weak, nonatomic) IBOutlet UIImageView *tagImageView;
@property (weak, nonatomic) IBOutlet UILabel     *tagNameLabel;
@property (weak, nonatomic) IBOutlet UILabel     *subNumberLabel;

@end

@implementation JKRecommendTagCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.tagImageView.layer.cornerRadius = 5;
    self.tagImageView.layer.masksToBounds = YES;
    
//    //cell背景透明
//    self.backgroundColor = [UIColor clearColor];
//    
//    //contentView背景设置为白色
//    self.contentView.backgroundColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTagModel:(JKRecommendTagModel *)tagModel{
    _tagModel = tagModel;
    
    [self.tagImageView sd_setImageWithURL:[NSURL URLWithString:tagModel.image_list] placeholderImage:[UIImage imageNamed:@"defaultUserIcon"]];
    
    self.tagNameLabel.text = tagModel.theme_name;
    
    NSInteger subNumber = [tagModel.sub_number integerValue];
    if (subNumber < 10000) {//小于1万，直接显示具体数字
        self.subNumberLabel.text = [NSString stringWithFormat:@"%ld人订阅", (long)subNumber];
    }else{//大于1万，显示多少万
        self.subNumberLabel.text = [NSString stringWithFormat:@"%.1f万人订阅", subNumber / 10000.0];
    }
    
}

//- (void)layoutSubviews{
//    [super layoutSubviews];
//    
//    //设置contentView的frame
//    self.contentView.frame = CGRectMake(5, 0, self.width - 10, self.height - 1);
//}

- (void)setFrame:(CGRect)frame{
    //frame.origin.x = 5;
    //frame.size.width -= 2 * frame.origin.x;
    frame.size.height-= 1;
    
    //写在下面，是在修改了之后再传给父类，防止设置好的frame再次被改回去
    [super setFrame:frame];
}

//如果只写frame，那么通过设置bound，还是能修改大小
- (void)setBounds:(CGRect)bounds{
    bounds.size = self.frame.size;
    
    [super setBounds:bounds];
}
@end
