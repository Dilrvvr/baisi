//
//  JKPostCell.m
//  Baisi
//
//  Created by albert on 16/3/20.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKPostCell.h"
#import "UIImageView+WebCache.h"
#import "JKPostModel.h"
#import "UILabel+JKTopLeft.h"
#import "JKPostPictureView.h"
#import "JKPostVoiceView.h"
#import "JKPostVideoView.h"
#import "JKCommentModel.h"
#import "JKUserModel.h"
#import "JKShareView.h"
#import "JKVideoManager.h"

@interface JKPostCell ()
/** 头像 */
@property (weak, nonatomic) IBOutlet UIImageView *profile_imageView;
/** 昵称 */
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
/** 发帖时间 */
@property (weak, nonatomic) IBOutlet UILabel *create_timeLabel;


/** 顶 */
@property (weak, nonatomic) IBOutlet UIButton *dingButton;
/** 踩 */
@property (weak, nonatomic) IBOutlet UIButton *caiButton;


/** 转发数量 */
@property (weak, nonatomic) IBOutlet UIButton *repostButton;
/** 评论数量 */
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
/** 新浪加V */
@property (weak, nonatomic) IBOutlet UIImageView *sina_vImageView;

/** 图片帖子中间的内容 */
@property (nonatomic, weak) JKPostPictureView *pictureView;

/** 声音帖子中间的内容 */
@property (nonatomic, weak) JKPostVoiceView *voiceView;

/** 最热评论父控件 */
@property (weak, nonatomic) IBOutlet UIView *topCmtView;
/** 最热评论内容 */
@property (weak, nonatomic) IBOutlet UILabel *topCmtContentLabel;
@end

@implementation JKPostCell

//为了防止反复创建，懒加载！！
- (JKPostVideoView *)videoView{
    if (_videoView == nil) {
        
        //因为是弱指针，不能让_pictureView直接就alloc init，这样一出生就挂了！！
        JKPostVideoView *videoView = [JKPostVideoView viewFromXib];
        
        //先添加，用强指针引用它
        [self.contentView addSubview:videoView];
        
        //再赋值
        _videoView = videoView;
    }
    return _videoView;
}

//为了防止反复创建，懒加载！！
- (JKPostVoiceView *)voiceView{
    if (_voiceView == nil) {
        
        //因为是弱指针，不能让_pictureView直接就alloc init，这样一出生就挂了！！
        JKPostVoiceView *voiceView = [JKPostVoiceView viewFromXib];
        
        //先添加，用强指针引用它
        [self.contentView addSubview:voiceView];
        
        //再赋值
        _voiceView = voiceView;
    }
    return _voiceView;
}

//为了防止反复创建，懒加载！！
- (JKPostPictureView *)pictureView{
    if (_pictureView == nil) {
        
        //因为是弱指针，不能让_pictureView直接就alloc init，这样一出生就挂了！！
        JKPostPictureView *pictureView = [JKPostPictureView viewFromXib];
        
        //先添加，用强指针引用它
        [self.contentView addSubview:pictureView];
        
        //再赋值
        _pictureView = pictureView;
    }
    return _pictureView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //设置背景图片
//    UIImageView *bgView = [[UIImageView alloc] init];
//    bgView.image = [UIImage imageNamed:@"mainCellBackground"];
//    self.backgroundView = bgView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPost:(JKPostModel *)post{
    _post = post;
    
    [post cellHeight];
    
    if (!_avoidSetImage) {
        
        //设置图片
        [self.profile_imageView setHeaderIcon:post.profile_image];
    }
    
    //设置新浪加V图片
    self.sina_vImageView.hidden = !post.isSina_v;

    //设置名字
    self.nameLabel.text = post.name;
    
    //设置时间显示,将时间判断写到模型的getter方法中
    self.create_timeLabel.text = post.create_time;
    [self.create_timeLabel setTextTopLeft];
    
    //设置段子内容
    self.contentTextLabel.attributedText = post.attrText;
    
    //设置按钮文字,也可以写在getter方法中
    [self setButtonTitle:self.dingButton count:[post.ding integerValue] placeHolder:@"顶"];
    [self setButtonTitle:self.caiButton count:[post.cai integerValue] placeHolder:@"踩"];
    [self setButtonTitle:self.repostButton count:[post.repost integerValue] placeHolder:@"分享"];
    [self setButtonTitle:self.commentButton count:[post.comment integerValue] placeHolder:@"评论"];
    
    if (post.type == kPostTypePicture) {//图片帖子
        //图片可见
        self.pictureView.hidden = NO;
        self.pictureView.frame = post.pictureFrame;
        self.pictureView.post = post;
        
        //其它隐藏
        self.voiceView.hidden = YES;
        self.videoView.hidden = YES;
        
    }else if (post.type == kPostTypeVoice){//声音帖子
        //声音可见
        self.voiceView.hidden = NO;
        self.voiceView.frame = post.voiceFrame;
        self.voiceView.post = post;
        
        //其它隐藏
        self.pictureView.hidden = YES;
        self.videoView.hidden = YES;
        
    }else if (post.type == kPostTypeVideo){//视频帖子
        //视频可见
        self.videoView.hidden = NO;
        self.videoView.frame = post.videoFrame;
        self.videoView.avoidSetImage = self.avoidSetImage;
        self.videoView.post = post;
        
        //其它隐藏
        self.pictureView.hidden = YES;
        self.voiceView.hidden = YES;
        
    }else if (post.type == kPostTypeWord){//段子帖子
        //其它均不可见
        self.pictureView.hidden = YES;
        self.voiceView.hidden = YES;
        self.videoView.hidden = YES;
    }
    
    //获取评论模型
    JKCommentModel *comment = post.top_cmt;
    //最热评论内容
//    NSString *content = [NSString stringWithFormat:@"%@: %@", comment.user.username, comment.content];
    if (comment) {//有最热评论才显示
        self.topCmtView.hidden = NO;
        self.topCmtContentLabel.attributedText = post.top_cmtAttr;
        self.topCmtContentLabel.height +=5;
    }else {
        self.topCmtView.hidden = YES;
    }
}

//设置按钮文字
- (void)setButtonTitle:(UIButton *)button count:(NSInteger)count placeHolder:(NSString *)placeholder{
    if (count > 10000) {
        placeholder = [NSString stringWithFormat:@"%.1fw", count / 10000.0];
    }else if (count > 0) {
        placeholder = [NSString stringWithFormat:@"%ld", (long)count];
    }
    [button setTitle:placeholder forState:(UIControlStateNormal)];
}

- (void)setFrame:(CGRect)frame{
    //headerView会调用这个方法，如何高度就一直减减减，就没了！！！
    //frame.size.height -= JKPostCellMargin;
    //解决办法
    frame.size.height = self.post.cellHeight - JKPostCellMargin;
    
    //frame.origin.x = JKPostCellMargin;
    //frame.size.width -= 2 * frame.origin.x;
    frame.origin.y += JKPostCellMargin;
    
    [super setFrame:frame];
}

- (IBAction)repostButtonClick:(id)sender {
    
    [JKShareView showWithModel:self.post];
}

- (IBAction)moreClick {
    
    if (self.moreClickBlock) {
        
        self.moreClickBlock(self);
        
        return;
    }
    
    [JKAlertView alertViewWithTitle:nil message:nil style:(JKAlertStyleActionSheet)].addAction([JKAlertAction actionWithTitle:@"收藏" style:(JKAlertActionStyleDefault) handler:^(JKAlertAction *action) {
        
    }]).addAction([JKAlertAction actionWithTitle:@"举报" style:(JKAlertActionStyleDefault) handler:^(JKAlertAction *action) {
        
    }]).show();
}
@end
