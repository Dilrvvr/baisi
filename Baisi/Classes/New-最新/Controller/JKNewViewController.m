//
//  JKNewViewController.m
//  Baisi
//
//  Created by albert on 16/3/13.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKNewViewController.h"
#import "JKRecommendTagTableViewController.h"

@interface JKNewViewController ()

@end

@implementation JKNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //设置导航栏内容
    //通过initWithImage设置的UIImageView的尺寸就和图片的尺寸一样了
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MainTitle"]];
    
    //设置导航栏leftBarButtonItem为自定义按钮
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:@"MainTagSubIcon" highImage:@"MainTagSubIconClick" target:self action:@selector(tagBtnClick)];
    
    //设置背景颜色
    self.view.backgroundColor = JKGlobalBgColor;
}

/**
 *  点击导航栏左按钮
 */
- (void)tagBtnClick{
    JKRecommendTagTableViewController *tagVc = [[JKRecommendTagTableViewController alloc] init];
    [self.navigationController pushViewController:tagVc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
