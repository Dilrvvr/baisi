//
//  JKSquareModel.h
//  Baisi
//
//  Created by albert on 16/3/29.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKSquareModel : NSObject
/** 名称 */
@property (nonatomic, copy) NSString *name;
/** 图标 */
@property (nonatomic, copy) NSString *icon;
/** 链接 */
@property (nonatomic, copy) NSString *url;
@end
