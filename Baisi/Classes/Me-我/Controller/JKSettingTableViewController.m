//
//  JKSettingTableViewController.m
//  Baisi
//
//  Created by albert on 16/4/1.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKSettingTableViewController.h"
#import "SDImageCache.h"
#import "SVProgressHUD.h"

@interface JKSettingTableViewController ()
/** 文件大小 */
@property (nonatomic, assign) CGFloat size;
@end

@implementation JKSettingTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.title = @"设置";
    self.tableView.backgroundColor = JKGlobalBgColor;
    [self.tableView jk_adapt_iOS_11_scrollView];
    self.tableView.contentInset = UIEdgeInsetsMake(JKNavBarHeight, 0, JKCurrentHomeIndicatorHeight, 0);
    
    //获取软件配置文件Info.plist
    NSDictionary *info = [NSBundle mainBundle].infoDictionary;
    
    //版本号的key
    NSString *versionKey = @"CFBundleShortVersionString";
    
    //获取当前软件的版本号
    NSString *currentVersion = info[versionKey];
    
    UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, JKScreenW, 44)];
    tipLabel.layer.backgroundColor = NULL;
    tipLabel.text = [NSString stringWithFormat:@"v%@", currentVersion];
    tipLabel.textAlignment = NSTextAlignmentCenter;
    tipLabel.font = [UIFont systemFontOfSize:12];
    tipLabel.textColor = JKColor(186, 186, 186);
    self.tableView.tableFooterView = tipLabel;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
       self.size = [SDImageCache sharedImageCache].getSize / 1024.0 / 1024;
        [self.tableView reloadData];
    });
}
/**
- (void)getSize2{
    //图片缓存，SDWebImage默认并没有减去子文件夹本身的大小
    //我觉得也没必要减去，因为子文件也是属于整个缓存的一部分
    NSUInteger size = [SDImageCache sharedImageCache].getSize;
    JKLog(@"SDImageSize===%lu", (unsigned long)size);
    
    NSFileManager *manager = [NSFileManager defaultManager];
    NSString *caches= [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *cachePath = [caches stringByAppendingPathComponent:@"default/com.hackemist.SDWebImageCache.default"];
    
    //fileManeger获取文件夹内部的所有内容
    //只能获取当前文件的内容，子文件的内容无法获取
    NSArray *contents = [manager contentsOfDirectoryAtPath:cachePath error:nil];
    JKLog(@"%@", contents);
    //可以获取文件夹所有内容，包括子文件夹中的的内容
    NSArray *subPaths = [manager subpathsAtPath:cachePath];
    JKLog(@"%@", subPaths);
}*/
/**
- (void)getSize{
    //图片缓存，SDWebImage默认并没有减去子文件夹本身的大小
    //我觉得也没必要减去，因为子文件也是属于整个缓存的一部分
    NSUInteger size = [SDImageCache sharedImageCache].getSize;
    JKLog(@"SDImageSize===%lu", (unsigned long)size);
    
    NSFileManager *manager = [NSFileManager defaultManager];
    NSString *caches= [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *cachePath = [caches stringByAppendingPathComponent:@"default/com.hackemist.SDWebImageCache.default"];
    
    //文件遍历器
    NSDirectoryEnumerator *fileEnumerator = [manager enumeratorAtPath:cachePath];
    //文件夹中所有文件的总大小
    NSInteger totalSize = 0;
    for (NSString *fileName in fileEnumerator) {
        
        //每个文件的全路径
        NSString *filePath = [cachePath stringByAppendingPathComponent:fileName];
        
        //每个文件的详情
        NSDictionary *fileInfo = [manager attributesOfItemAtPath:filePath error:nil];
        
        / **判断文件类型：文件\文件夹，还可判断文件是否存在
         BOOL isDirectory = NO;
         [manager fileExistsAtPath:filePath isDirectory:&isDirectory];
         //如果是文件夹，跳过
         if (isDirectory) continue;**** /
        
        //fileManager本身也有判断文件夹类型的NSFileType
        //NSFileTypeRegular   文件
        //NSFileTypeDirectory 文件夹
        if ([fileInfo[NSFileType] isEqualToString:NSFileTypeDirectory]) continue;
        
        //每个文件的大小
        NSInteger fileSize = [fileInfo[NSFileSize] integerValue];
        
        //将每个文件的大小加起来
        totalSize += fileSize;
    }
    JKLog(@"totalSize--%ld", totalSize);
} */

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"其它";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *settingId = @"setting";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingId];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:settingId];
    }
    
    switch (indexPath.row) {
        case 0:
        {
            if (!self.size) {
                
                cell.textLabel.text = @"清理图片缓存";
                
            }else{
                
                cell.textLabel.text = [NSString stringWithFormat:@"清理图片缓存(已使用%.2fMB)", self.size];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
        }
            break;
        case 1:
        {
            
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row > 0) {
        
        
        return;
    }
    
    [JKProgressHUD showWithStatus:@"正在清理..."];
    //清除缓存
    //使用NSFileManager
    //[[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    
    //使用SDImageCache
    [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
        
        //重新获取缓存
        self.size = 0;
        [JKProgressHUD showSuccessWithStatus:@"清理缓存成功！"];
        [self.tableView reloadData];
    }];
}
@end
