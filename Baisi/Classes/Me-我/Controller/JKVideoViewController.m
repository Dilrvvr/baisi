//
//  JKVideoViewController.m
//  Baisi
//
//  Created by albert on 2018/3/20.
//  Copyright © 2018年 安永博. All rights reserved.
//

#import "JKVideoViewController.h"
#import "JKVideoManager.h"
#import "JKPostModel.h"
#import "JKPostCell.h"

@interface JKVideoViewController ()

@end

@implementation JKVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.view.backgroundColor = JKGlobalBgColor;
    
    self.postType = kPostTypeVideo;
    
    self.tableView.contentInset = UIEdgeInsetsMake(JKNavBarHeight, 0, JKCurrentHomeIndicatorHeight, 0);
    self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
    
    [self loadNewPosts];
}

- (void)setupRefresh{}

- (void)loadNewPosts{
    
    NSMutableArray *arr = [NSMutableArray arrayWithContentsOfFile:[JKVideoManager videoInfoPath]];
    self.posts = [JKPostModel mj_objectArrayWithKeyValuesArray:arr];
    
    [self.tableView reloadData];
    
    [self calculateCacheSize];
}

- (void)calculateCacheSize{
    
    CGFloat size = 0;
    
    for (JKPostModel *model in self.posts) {
        
        size += [model.fileSize floatValue];
    }
    
    NSString *navTitle = [NSString stringWithFormat:@"视频缓存(%.2fMB)", size];
    
    if (size > 1024.0) {
        
        navTitle = [NSString stringWithFormat:@"视频缓存(%.2fGB)", size / 1024.0];
    }
    
    self.navigationItem.title = self.posts.count > 0 ? navTitle : @"视频缓存";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JKPostCell *cell = (JKPostCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    __weak typeof(self) weakSelf = self;
    
    [cell setMoreClickBlock:^(JKPostCell *cell) {
        
        if ([JKVideoManager videoExistWithUrl:cell.post.videouri]) {
            
            [JKAlertView alertViewWithTitle:nil message:nil style:(JKAlertStyleActionSheet)].addAction([JKAlertAction actionWithTitle:@"删除缓存视频" style:(JKAlertActionStyleDestructive) handler:^(JKAlertAction *action) {
                
                if ([JKVideoManager deleteVideWithUrl:cell.post.videouri]) {
                    
                    [weakSelf.posts removeObject:cell.post];
                    
                    NSIndexPath *indexPath = [weakSelf.tableView indexPathForCell:cell];
                    [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationAutomatic)];
                    
                    [weakSelf calculateCacheSize];
                    
                }else{
                    
                    [JKProgressHUD showErrorWithStatus:@"删除失败!"];
                }
            }]).show();
            
            return;
        }
    }];
    
    return cell;
}

- (void)dealloc{
    NSLog(@"%d, %s",__LINE__, __func__);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
