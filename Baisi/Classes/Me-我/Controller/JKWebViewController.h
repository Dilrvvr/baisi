//
//  JKWebViewController.h
//  Baisi
//
//  Created by albert on 16/3/29.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKWebViewController : UIViewController
/** 链接 */
@property (nonatomic, copy) NSString *urlStr;
@end
