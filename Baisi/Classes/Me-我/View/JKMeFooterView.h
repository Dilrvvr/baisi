//
//  JKMeFooterView.h
//  Baisi
//
//  Created by albert on 16/3/29.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKMeFooterView : UIView

- (void)createSquares:(NSArray *)squares;
@end
