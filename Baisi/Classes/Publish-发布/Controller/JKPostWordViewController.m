//
//  JKPostWordViewController.m
//  Baisi
//
//  Created by albert on 16/3/30.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKPostWordViewController.h"
#import "JKPlaceholderTextView.h"
#import "SVProgressHUD.h"
#import "JKAddTagToolbarView.h"

@interface JKPostWordViewController () <UITextViewDelegate>
/** textView */
@property (nonatomic, weak) JKPlaceholderTextView *textView;
/** toolbar */
@property (nonatomic, weak) JKAddTagToolbarView *toolbar;
@end

@implementation JKPostWordViewController

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //进来就弹出键盘
    [self.textView becomeFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //初始化导航栏
    [self setupNav];
    
    //设置textView
    [self setupTextView];
    
    //设置toolbar
    [self setupToolbar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

/**
 *  监听键盘的弹出和隐藏
 */
- (void)keyboardWillChangeFrame:(NSNotification *)noti{
    
    //键盘最终的frame
    CGRect endFrame = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [noti.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        //self.toolbar.y = endFrame.origin.y - self.toolbar.height;
        //推荐使用transform
        self.toolbar.transform = CGAffineTransformMakeTranslation(0, endFrame.origin.y - JKScreenH + (endFrame.origin.y == JKScreenH ? 0 : JKCurrentHomeIndicatorHeight));
    }];
}

//设置toolbar
- (void)setupToolbar{
    JKAddTagToolbarView *toobar = [JKAddTagToolbarView viewFromXib];
    toobar.width = self.view.width;
    toobar.y = JKScreenH - toobar.height - JKCurrentHomeIndicatorHeight;
    [self.view addSubview:toobar];
    
    self.toolbar = toobar;
}

//设置textView
- (void)setupTextView{
    JKPlaceholderTextView *textView = [[JKPlaceholderTextView alloc] init];
    textView.frame = self.view.bounds;
    textView.placeholder = @"把好玩的图片，好笑的段子或糗事发到这里，接受千万网友膜拜吧！发布违反国家法律内容的，我们将依法提交给有关部门处理。";
    [self.view addSubview:textView];
    
    //设置代理
    textView.delegate = self;
    
    self.textView = textView;
}

//初始化导航栏
- (void)setupNav{
    //标题
    self.title = @"发表文字";
    //背景
    self.view.backgroundColor = JKGlobalBgColor;
    
    //添加左右item。这里的style从iOS7开始，写什么都一样
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:(UIBarButtonItemStyleDone) target:self action:@selector(cancel)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发表" style:(UIBarButtonItemStyleDone) target:self action:@selector(postWord)];
    
    //设置右item为红色，并且一开始不能点击
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} forState:(UIControlStateNormal)];
    
    //默认不能点击,因为刚进来肯定没有文字
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    //强制刷新
    [self.navigationController.navigationBar layoutIfNeeded];
}

//点击发表按钮
- (void)postWord{
    
    JKLog(@"%s", __func__);
    [self.view endEditing:YES];
    
    if (self.textView.text.length < 5 || self.textView.attributedText.length < 5) {
        
        [JKAlertView alertViewWithTitle:@"提示" message:@"帖子内容不能少于5个字" style:(JKAlertStylePlain)].addAction([JKAlertAction actionWithTitle:@"退出发表" style:(JKAlertActionStyleCancel) handler:^(JKAlertAction *action) {
            
            [self dismissViewControllerAnimated:NO completion:nil];
            
        }]).addAction([JKAlertAction actionWithTitle:@"继续编辑" style:(JKAlertActionStyleDefault) handler:^(JKAlertAction *action) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self.textView becomeFirstResponder];
            });
            
        }]).show();
        
    }else{
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
        [JKProgressHUD showWithStatus:@"正在发布..."];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [JKProgressHUD showSuccessWithStatus:@"发布成功！" ];
        });
    }
}

#pragma mark - <UITextViewDelegate>
- (void)textViewDidChange:(UITextView *)textView{
    self.navigationItem.rightBarButtonItem.enabled = self.textView.hasText;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

//点击取消按钮
- (void)cancel{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
