//
//  JKAddTagViewController.h
//  Baisi
//
//  Created by albert on 16/3/30.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKAddTagViewController : UIViewController
/** 标签文字的block */
@property (nonatomic, copy) void(^tagsBlock)(NSArray *tags);

/** 存放标签文字的数组 */
@property (nonatomic, strong) NSArray *tags;
@end


@interface JKItemButton : UIButton

@end
