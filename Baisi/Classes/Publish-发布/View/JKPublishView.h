//
//  JKPublishView.h
//  Baisi
//
//  Created by albert on 16/3/23.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JKVerticalButton.h"

@interface JKPublishView : UIView
+ (void)show;
@end

@interface JKNoHighlightedVerticleButton : JKVerticalButton

@end
