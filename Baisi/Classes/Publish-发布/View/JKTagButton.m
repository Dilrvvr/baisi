//
//  JKTagButton.m
//  Baisi
//
//  Created by albert on 16/3/31.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKTagButton.h"

@implementation JKTagButton

- (void)setup{
    self.titleLabel.font = JKTagFont;
    [self setBackgroundColor:JKTagBg];
    [self setImage:[UIImage imageNamed:@"chose_tag_close_icon"] forState:(UIControlStateNormal)];
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state{
    [super setTitle:title forState:state];
    
    [self sizeToFit];
    
    self.width += 3 * JKTagMargin;
    self.height = JKTagH;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.titleLabel.x = JKTagMargin;
    self.imageView.x = CGRectGetMaxX(self.titleLabel.frame) + JKTagMargin;
}
@end
