//
//  JKTagTextField.m
//  Baisi
//
//  Created by albert on 16/3/31.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKTagTextField.h"

@implementation JKTagTextField

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.placeholder = @"多个标签用逗号或换行分隔";
        
        //占位文字的label是懒加载的，设置了占位文字内容以后，才能设置颜色
        [self setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        self.height = JKTagH;
    }
    return self;
}

- (void)insertText:(NSString *)text{
    [super insertText:text];
    
    //如果是\n 就是点击了return key
    if ([text isEqualToString:@"\n"]) {
        //通知那个控制器。。。
    }
}

- (void)deleteBackward{
    
    //如果block有值就执行，三目运算符的冒号后面必须有东西
    !self.deleteBlock ? : self.deleteBlock();
    
    //先调用block，再调用父类方法
    //这样删除完毕后再点删除，文本框的hasText才是0（NO）
    [super deleteBackward];
}
@end
