//
//  JKPublishViewController.m
//  Baisi
//
//  Created by albert on 16/3/23.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKPublishView.h"
#import "POP.h"
#import "JKPostWordViewController.h"
#import "JKNavigationController.h"
#import "JKLoginTool.h"

@interface JKPublishView ()
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (nonatomic, weak) UIImageView *sloganView;

@end

@implementation JKPublishView

//window要定义为全局的，否则创建出来就死了
static UIWindow *window_;

+ (void)show{
    window_ = [[UIWindow alloc] init];
    window_.frame = [UIScreen mainScreen].bounds;
    
    //window要显示出来，只需要设置hidden为NO就行
    window_.hidden = NO;
    
    window_.backgroundColor = nil;
    
    //添加publishView
    JKPublishView *publishView = [JKPublishView viewFromXib];
    publishView.backgroundColor = nil;
    publishView.frame = window_.bounds;
    [window_ addSubview:publishView];
    
    UIToolbar *tbar = [[UIToolbar alloc] initWithFrame:publishView.bounds];
    [publishView insertSubview:tbar atIndex:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.autoresizingMask = UIViewAutoresizingNone;
    //一开始让控制器的view不能点击
    self.userInteractionEnabled = NO;
    
    //初始化6个发布按钮
    [self setupPublishButtons];
}

//初始化6个发布按钮
- (void)setupPublishButtons{
    //图片和标题
    NSArray *images = @[@"publish-video", @"publish-picture", @"publish-text", @"publish-audio", @"publish-review", @"publish-offline"];
    NSArray *titles = @[@"发视频", @"发图片", @"发段子", @"发声音", @"审帖", @"发链接"];
    
    CGFloat buttonW = 72;
    CGFloat buttonH = buttonW + 30;
    CGFloat margin = (JKScreenW - buttonW * 3) / 4;
    CGFloat buttonStartX = margin;
    CGFloat buttonStartY = (JKScreenH - buttonW * 2 - 65) * 0.5;
    NSInteger maxCols = 3;
    
    //6个发布按钮
    for (NSInteger i = 0; i < images.count; i++) {
        JKNoHighlightedVerticleButton *button = [JKNoHighlightedVerticleButton buttonWithType:(UIButtonTypeCustom)];
        //按钮添加事件
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:(UIControlEventTouchUpInside)];
        
        //绑定tag
        button.tag = i;
        
        [self addSubview:button];
        
        NSInteger row = i / maxCols;
        NSInteger col = i % maxCols;
        
        CGFloat buttonX = buttonStartX + (margin + buttonW) * col;
        CGFloat buttonEndY = buttonStartY + (buttonH + 20) * row;
        CGFloat buttonStartY = buttonEndY - JKScreenH;
        button.y = buttonStartY;
        
        [button setImage:[UIImage imageNamed:images[i]] forState:(UIControlStateNormal)];
        button.titleLabel.font = [UIFont systemFontOfSize:15];
        [button setTitleColor:[UIColor darkGrayColor] forState:(UIControlStateNormal)];
        [button setTitle:titles[i] forState:(UIControlStateNormal)];
        
        //创建动画
        POPSpringAnimation *ani = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
        
        if (i == 4) {
            ani.beginTime = CACurrentMediaTime() + 0.01;
        }else if (i == 3 || i == 5){
            ani.beginTime = CACurrentMediaTime() + 0.1;
        }else if (i == 1){
            ani.beginTime = CACurrentMediaTime() + 0.2;
        }else if (i == 0 || i == 2){
            ani.beginTime = CACurrentMediaTime() + 0.3;
        }
        
        //动画效果
        ani.springSpeed = 15;
        ani.springBounciness = 6;
        
        //动画的起始位置
        ani.fromValue = [NSValue valueWithCGRect:CGRectMake(buttonX, buttonStartY, buttonW, buttonH)];
        ani.toValue = [NSValue valueWithCGRect:CGRectMake(buttonX, buttonEndY, buttonW, buttonH)];
        
        //标语动画执行完毕，恢复点击事件
        [ani setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            self.userInteractionEnabled = YES;
        }];
        
        //添加动画到按钮
        [button pop_addAnimation:ani forKey:nil];
    }
    
    //标语view
    UIImageView *sloganView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"app_slogan"]];
    CGFloat centerX = JKScreenW *0.5;
    CGFloat endCenterY = JKScreenH * 0.18;
    CGFloat startCenterY = endCenterY - JKScreenH;
    sloganView.centerY = startCenterY;
    
    //标语动画
    POPSpringAnimation *ani = [POPSpringAnimation animationWithPropertyNamed:kPOPViewCenter];
    ani.fromValue = [NSValue valueWithCGPoint:CGPointMake(centerX, startCenterY)];
    ani.toValue = [NSValue valueWithCGPoint:CGPointMake(centerX, endCenterY)];
    ani.beginTime = CACurrentMediaTime() + 0.35;
    [sloganView pop_addAnimation:ani forKey:nil];
    
    [self addSubview:sloganView];
    self.sloganView = sloganView;
}

- (void)cancelWithBlock:(void(^)())block{
    //让控制器的view不能点击
    self.userInteractionEnabled = NO;
    
    //取消按钮也动画下落
    [UIView animateWithDuration:0.1 animations:^{
        self.cancelButton.centerY += JKScreenH;
    }];
    
    //6个按钮动画下落
    for (NSInteger i = 1; i < self.subviews.count - 1; i++) {
        UIView *subView = self.subviews[i];
        
        //使用基本动画就行,都看不见了，就不需要弹簧效果了
        POPBasicAnimation *ani = [POPBasicAnimation animationWithPropertyNamed:kPOPViewCenter];
        
        //让动画一开始很快，后面加快
        ani.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        if (i == 5) {
            ani.beginTime = CACurrentMediaTime() + 0.01;
        }else if (i == 4 || i == 6){
            ani.beginTime = CACurrentMediaTime() + 0.1;
        }else if (i == 2){
            ani.beginTime = CACurrentMediaTime() + 0.2;
        }else if (i == 1 || i == 3){
            ani.beginTime = CACurrentMediaTime() + 0.3;
        }
        
        //只需要一个toValue
        ani.toValue = [NSValue valueWithCGPoint:CGPointMake(subView.centerX, subView.centerY + JKScreenH)];
        
        [subView pop_addAnimation:ani forKey:nil];
    }
    
    //标语下落基本动画
    POPBasicAnimation *ani = [POPBasicAnimation animationWithPropertyNamed:kPOPViewCenter];
    ani.toValue = [NSValue valueWithCGPoint:CGPointMake(self.sloganView.centerX, self.sloganView.centerY + JKScreenH)];
    ani.beginTime = CACurrentMediaTime() + 0.35;
    
    //动画执行完毕后dismiss窗口
    [ani setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        window_.hidden = YES;
        window_ = nil;
        
        //执行传进来的block
        //if (block) {
            //block();
        //}
        !block ? : block();
    }];
    
    [self.sloganView pop_addAnimation:ani forKey:nil];
}

//点击取消要执行完动画后再弹出对应的控制器
- (void)buttonClick:(UIButton *)button{
    [self cancelWithBlock:^{
        if (button.tag == 0) {
            JKLog(@"发视频");
        }else if(button.tag == 2){
            //没有登录就直接返回，方法内部会弹出登录框
            //if (![JKLoginTool getUid:YES]) return;
            
            //创建发表文字的控制器
            JKPostWordViewController *postWordVc = [[JKPostWordViewController alloc] init];
            //创建导航控制器，并指定其根控制器为发表文字控制器，这样弹出来上面就又导航栏了
            JKNavigationController *nav = [[JKNavigationController alloc] initWithRootViewController:postWordVc];
            
            //如果self是个控制器，也不能用来弹出其它控制器，因为self执行了dismiss操作
            //[self presentViewController:nav animated:YES completion:nil];
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:YES completion:nil];
        }//...
    }];
}

//block不能是nil，所以调用的时候要先判断
- (IBAction)cancel {
    [self cancelWithBlock:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self cancel];
}
@end


@implementation JKNoHighlightedVerticleButton

- (void)setHighlighted:(BOOL)highlighted{};

@end
