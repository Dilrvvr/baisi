//
//  JKAddTagToolbarView.m
//  Baisi
//
//  Created by albert on 16/3/30.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKAddTagToolbarView.h"
#import "JKAddTagViewController.h"
#import "JKPostWordViewController.h"
#import "JKNavigationController.h"

@interface JKAddTagToolbarView ()
@property (weak, nonatomic) IBOutlet UIView *topView;

/** 添加标签按钮 */
@property (nonatomic, weak) UIButton *addButton;

/** 存放所有标签lable的数组 */
@property (nonatomic, strong) NSMutableArray *tagLabels;

@end

@implementation JKAddTagToolbarView

- (NSMutableArray *)tagLabels{
    if (!_tagLabels) {
        _tagLabels = [NSMutableArray array];
    }
    return _tagLabels;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
    UIButton *addButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //添加事件
    [addButton addTarget:self action:@selector(addButtonClick) forControlEvents:(UIControlEventTouchUpInside)];
    //设置图片
    [addButton setImage:[UIImage imageNamed:@"tag_add_icon"] forState:(UIControlStateNormal)];
    
    //设置按钮尺寸为图片尺寸的三种方法
    // imageNamed如果是同一个，不会重复加载的
    //addButton.size = [UIImage imageNamed:@"tag_add_icon"].size;
    //addButton.size = [addButton imageForState:(UIControlStateNormal)].size;
    addButton.size = addButton.currentImage.size;
    [self.topView addSubview:addButton];
    self.addButton = addButton;
    
    //一开始有两个标签
    [self createTags:@[@"糗事", @"吐槽"]];
}

//按钮点击，添加标签
- (void)addButtonClick{
    //添加标签控制器
    JKAddTagViewController *addTagVc = [[JKAddTagViewController alloc] init];
    
    //根控制器
    UITabBarController *rootVc = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    
    //模态窗口的指向关系
    //rootVc.presentedViewController.presentingViewController == rootVc;
    //[a presentViewController:b animated:YES completion:nil];
    //a.presentedViewController = b;
    //b.presentingViewController = a;
    
    //获取导航控制器
    UINavigationController *postWordNav = (UINavigationController *)rootVc.presentedViewController;
//    [nav pushViewController:addTagVc animated:YES];
    
    JKNavigationController *nav = [[JKNavigationController alloc] initWithRootViewController:addTagVc];
    
    //调用block
    __weak typeof(self) weakSelf = self;
    [addTagVc setTagsBlock:^(NSArray *tags) {
        //创建标签，这里使用label
        [weakSelf createTags:tags];
    }];
    
    //将标签传递给下一个控制器
    addTagVc.tags = [self.tagLabels valueForKeyPath:@"text"];
    
    [postWordNav presentViewController:nav animated:YES completion:nil];
}

//创建标签
- (void)createTags:(NSArray *)tags{
    //先让数组中的空间从父控件移除，再清空数组
    [self.tagLabels makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.tagLabels removeAllObjects];
    
    for (NSInteger i = 0; i < tags.count; i++) {
        UILabel *tagLabel = [[UILabel alloc] init];
        tagLabel.backgroundColor = JKTagBg;
        tagLabel.font = JKTagFont;
        tagLabel.textColor = [UIColor whiteColor];
        tagLabel.textAlignment = NSTextAlignmentCenter;
        tagLabel.text = tags[i];
        [tagLabel sizeToFit];
        tagLabel.width += 2 * JKTagMargin;
        tagLabel.height = JKTagH;
        //添加点击事件
        tagLabel.userInteractionEnabled = YES;
        [tagLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addButtonClick)]];
        [self.topView addSubview:tagLabel];
        
        //为避免出现一些问题，这里也要再判断一下，限制只能有5个标签
        if (self.tagLabels.count >= 5) return;
        
        [self.tagLabels addObject:tagLabel];
    }
    
    //更新标签和“添加标签按钮”的frame
    [self setNeedsLayout]; //layoutIfNeeded
}

/**
 *  要在layoutSubviews方法中更新标签和“添加标签按钮”的frame
 *  因为在这里拿到的尺寸才是最真实的尺寸
 *  否则在随便的一个方法中，拿到的尺寸就是xib的尺寸
 */
- (void)layoutSubviews{
    [super layoutSubviews];
    
    //更新标签按钮的frame
    for (NSInteger i = 0; i < self.tagLabels.count; i++) {
        UILabel *taglabel = self.tagLabels[i];
        if (i == 0) {//第一个标签按钮
            taglabel.x = 0;
            taglabel.y = 0;
            
        }else{//其它标签按钮
            //上一个标签按钮
            UILabel *lastTaglabel = self.tagLabels[i - 1];
            
            //当前行剩余宽度
            CGFloat leftWidth = self.topView.width - CGRectGetMaxX(lastTaglabel.frame) - JKTagMargin;
            if (leftWidth >= taglabel.width) {//按钮显示在当前行
                taglabel.x = self.topView.width - leftWidth;
                taglabel.y = lastTaglabel.y;
                
            }else{//按钮显示在下一行
                //上一个标签按钮的最大Y值
                CGFloat lastTagButtonMaxY = CGRectGetMaxY(lastTaglabel.frame);
                taglabel.x = 0;
                taglabel.y = lastTagButtonMaxY + JKTagMargin;
            }
        }
    }
    
    //更新添加标签按钮的frame
    UILabel *lastTagLabel = self.tagLabels.lastObject;
    //剩余宽度
    CGFloat leftWidth = self.topView.width - CGRectGetMaxX(lastTagLabel.frame) - JKTagMargin;
    
    if (leftWidth >= self.addButton.width) {
        self.addButton.x = self.topView.width - leftWidth;
        self.addButton.y = lastTagLabel.y;
    }else{
        self.addButton.x = 0;
        self.addButton.y = CGRectGetMaxY(lastTagLabel.frame) + JKTagMargin;
    }
    
    //计算整个高度
    CGFloat oldH = self.height;
    self.height = CGRectGetMaxY(self.addButton.frame) + JKNavBarHeight;
    self.y -= self.height - oldH;
}
@end
