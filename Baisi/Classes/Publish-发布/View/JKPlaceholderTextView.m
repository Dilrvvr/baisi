//
//  JKPlaceholderTextView.m
//  Baisi
//
//  Created by albert on 16/3/30.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKPlaceholderTextView.h"

@interface JKPlaceholderTextView ()
/** 占位文字label */
@property (nonatomic, weak) UILabel *placeholderLabel;
@end

@implementation JKPlaceholderTextView

//懒加载，就不用考虑label添加的顺序了
- (UILabel *)placeholderLabel{
    if (!_placeholderLabel) {
        //添加一个用来显示占位文字的UILabel
        UILabel *placeholderLabel = [[UILabel alloc] init];
        placeholderLabel.numberOfLines = 0;
        placeholderLabel.x = 5;
        placeholderLabel.y = 7;
        placeholderLabel.text = self.placeholder;
        placeholderLabel.textColor = self.placeholderColor;
        [self addSubview:placeholderLabel];
        _placeholderLabel = placeholderLabel;
    }
    return _placeholderLabel;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //垂直方向永远有弹簧效果
        self.alwaysBounceVertical = YES;
        
        //默认占位文字文字,第二种计算label高度的方法不能设置这个，会崩
        self.placeholder = @"请输入文字...";
        
        //默认占位文字字体
        self.font = [UIFont systemFontOfSize:16];

        //设置默认占位文字颜色
        self.placeholderColor = [UIColor lightGrayColor];
        
        //监听输入的文字改变
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange) name:UITextViewTextDidChangeNotification object:nil];
    }
    return self;
}

//一定要移除通知
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/**
 *  监听文字的变化
 */
- (void)textDidChange{
    //只要有文字，就隐藏label
    self.placeholderLabel.hidden = self.hasText;
}

/**
 *  更新placeholderLabel的高度
 *  layoutSubviews中的尺寸肯定是最准确的
 *  调用layoutSubviews的时候，肯定是有尺寸的
 */
- (void)layoutSubviews{
    [super layoutSubviews];
    
    //第一种方法：sizeToFit
    //sizeToFit必须有一个准确的宽度，使用屏幕宽度确认label宽度
    self.placeholderLabel.width = self.width- 2 * self.placeholderLabel.x;
    
    //sizeToFit
    [self.placeholderLabel sizeToFit];
    
    //第二种方法：计算文字所占尺寸，此方法不能设置默认占位文字内容
    //CGSize maxSize = CGSizeMake(self.width - 2 * self.placeholderLabel.x, MAXFLOAT);
    //self.placeholderLabel.size = [self.placeholder boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.font} context:nil].size;
}

#pragma mark - 重写各种setter方法，随时响应各个属性的修改
- (void)setPlaceholderColor:(UIColor *)placeholderColor{
    _placeholderColor = placeholderColor;
    
    self.placeholderLabel.textColor = self.placeholderColor;
}

- (void)setPlaceholder:(NSString *)placeholder{
    _placeholder = [placeholder copy];
    
    self.placeholderLabel.text = placeholder;
    
    //立即布局子控件，更新label尺寸
    [self setNeedsLayout];
}

//非自定义的属性（父类的属性）也可能被改，所以也要重写setter方法
- (void)setFont:(UIFont *)font{
    [super setFont:font];
    
    self.placeholderLabel.font = font;
    
    //立即布局子控件，更新label尺寸
    [self setNeedsLayout];
}

- (void)setText:(NSString *)text{
    [super setText:text];
    
    //改变文字的话，要判断label是否要隐藏
    [self textDidChange];
}

- (void)setAttributedText:(NSAttributedString *)attributedText{
    [super setAttributedText:attributedText];
    
    //改变文字的话，要判断label是否要隐藏
    [self textDidChange];
}
/**
 *  setNeedsDisplay方法 : 会在恰当的时刻调用drawRect:
 *  setNeedsLayout方法 :  会在恰当的时刻调用layoutSubviews
 */
@end
