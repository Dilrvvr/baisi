//
//  JKTagTextField.h
//  Baisi
//
//  Created by albert on 16/3/31.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKTagTextField : UITextField
/** 点击删除后的block回调 */
@property (nonatomic, copy) void(^deleteBlock)();
@end
