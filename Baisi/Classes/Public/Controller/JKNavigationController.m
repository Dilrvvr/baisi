//
//  JKNavigationController.m
//  Baisi
//
//  Created by albert on 16/3/13.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKNavigationController.h"
#import "JKRecommendTagTableViewController.h"

@interface JKNavigationController () <UIGestureRecognizerDelegate, CAAnimationDelegate>

@end

@implementation JKNavigationController

//类第一被使用的时候调用一次
+ (void)initialize{
    
    //当导航控制器是JKNavigationController的时候，appearance设置才会生效
    //UINavigationBar *bar = [UINavigationBar appearanceWhenContainedIn:[self class], nil];
    
    //appearance设置的属性，某些控制器不想要的话，可以自己覆盖
    
    //设置所有导航条背景图片
    UINavigationBar *bar = [UINavigationBar appearance];
//    [bar setBackgroundImage:[UIImage imageNamed:@"navigationbarBackgroundWhite"] forBarMetrics:(UIBarMetricsDefault)];
  
    //设置所有导航栏文字粗体20号，此时左右item也都会是粗体20号，需要在下面自己再改回来
    [bar setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18]}];
    
    //设置所有的item字体和颜色
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    //普通状态黑色
    NSMutableDictionary *itemAttr = [NSMutableDictionary dictionary];
    itemAttr[NSForegroundColorAttributeName] = [UIColor blackColor];
    itemAttr[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    [item setTitleTextAttributes:itemAttr forState:(UIControlStateNormal)];
    //disabled状态红灰色
    NSMutableDictionary *disabledItemAttr = [NSMutableDictionary dictionary];
    disabledItemAttr[NSForegroundColorAttributeName] = JKColor(237, 155, 155);
    [item setTitleTextAttributes:disabledItemAttr forState:(UIControlStateDisabled)];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    //[self.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbarBackgroundWhite"] forBarMetrics:(UIBarMetricsDefault)];
    
    //self代表navigationController
    //self.interactivePopGestureRecognizer.delegate = (id)self;
    //清空代理也可以，系统会自动找回这个功能
    //self.interactivePopGestureRecognizer.delegate = nil;
    
    id target = self.interactivePopGestureRecognizer.delegate;
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
    pan.delegate = self;
    
    [self.view addGestureRecognizer:pan];
    
    self.interactivePopGestureRecognizer.enabled = NO;
}

- (void)handleNavigationTransition:(id)sender{}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}

/**
 *  可以在这个方法中拦截所有push进来的控制器
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    //如果push进来的不是第一个控制器，才创建返回按钮
    if (self.childViewControllers.count > 0) {
        //设置返回
        UIButton *button = [UIButton buttonWithType:(UIButtonTypeCustom)];
//        [button setTitle:@"返回" forState:(UIControlStateNormal)];
        [button setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
        [button setTitleColor:[UIColor redColor] forState:(UIControlStateHighlighted)];
        [button setImage:[UIImage imageNamed:@"navigationButtonReturn"] forState:(UIControlStateNormal)];
        [button setImage:[UIImage imageNamed:@"navigationButtonReturnClick"] forState:(UIControlStateHighlighted)];
        [button addTarget:self action:@selector(backClick) forControlEvents:(UIControlEventTouchUpInside)];
        
        //[button setBackgroundColor:[UIColor grayColor]];
        
        button.size = CGSizeMake(70, 30);
        
        //让button里面的内容移到最左边
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        //设置button的内容偏移，可以与上面的Alignment配合，也可以直接设置偏移量更大些，比如-10改为-80
//        button.contentEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);//想让导航条的按钮距离边上更近些，可以用这个
        
        //contentMode一般用来设置imageView，设置button没有用
        //button.contentMode = UIViewContentModeLeft;
        
        //让button的大小适配里面的内容
        //[button sizeToFit];
        
        //设置返回按钮为自定义是没有反应的
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        //隐藏tabBar,要写在if里面，否则第一页的tabBar也被隐藏了，就没法点了！
        viewController.hidesBottomBarWhenPushed = YES;
    }
    
    //这句super的push要放在后面，让viewController可以覆盖上面设置的leftBarButtonItem
    //这个方法执行之后，才会添加为子控制器
    [super pushViewController:viewController animated:animated];
    
    if (!JKIsIphoneX) {
        return;
    }
    
    // 修改tabBra的frame
    CGRect frame = self.tabBarController.tabBar.frame;
    frame.size.height = 49 + 34;
    frame.origin.y = [UIScreen mainScreen].bounds.size.height - frame.size.height;
    self.tabBarController.tabBar.frame = frame;
}

- (void)backClick{
    
    if ([self.topViewController isKindOfClass:[JKRecommendTagTableViewController class]]) {
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.delegate = self;
        transition.type = @"push";
        transition.subtype = kCATransitionFromRight;
        [self.view.layer addAnimation:transition forKey:nil];
    }
    
    
    [self popViewControllerAnimated:YES];
}
@end
