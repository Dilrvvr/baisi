//
//  JKDeleteJsonNull.h
//  Test
//
//  Created by albert on 2017/1/6.
//  Copyright © 2017年 Albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKDeleteJsonNull : NSObject

/** 删除后台返回的null字段 */
+ (id)jk_deleteJsonNullWithJsonObject:(id)jsonObject;

@end
