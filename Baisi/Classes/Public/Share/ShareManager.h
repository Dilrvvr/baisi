//
//  ShareManager.h
//  ZhiHuDaily
//
//  Created by albert on 16/8/27.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kShareTypeWechatSession = 0,   // 微信朋友
    kShareTypeWechatTimeline = 1,  // 微信朋友圈
    kShareTypeWechatFavorite = 2,  // 微信收藏
    kShareTypeMobileQQ = 3,        // QQ
    kShareTypeQzone = 4,           // QQ空间
    kShareTypeTypeSina = 5,        // 新浪微博
} kShareType;

@interface ShareManager : NSObject
/**
 *  通过第三方分享内容
 *
 *  @param url         分享的链接
 *  @param text        分享的标题
 *  @param description 分享的详情（微信中没有这个，可以为空）
 *  @param imageUrl    分享中图片的url （微博分享中，这个url的图片大小不能超过32k）
 *  @param type        分享的途径或方式
 */
+ (void)shareUrl:(NSString *)url text:(NSString *)text description:(NSString *)description image:(NSString *)imageUrl shareTyep:(kShareType)type;
@end
