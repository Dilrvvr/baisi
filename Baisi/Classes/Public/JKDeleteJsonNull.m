//
//  JKDeleteJsonNull.m
//  Test
//
//  Created by albert on 2017/1/6.
//  Copyright © 2017年 Albert. All rights reserved.
//

#import "JKDeleteJsonNull.h"

@implementation JKDeleteJsonNull

+ (NSMutableArray *)solveNullArray:(NSArray *)arrayObject{
    
    NSMutableArray *tmpArr = [NSMutableArray array];
    
    for (id obj in arrayObject) {
        
        if ([obj isKindOfClass:[NSNull class]]) {
            
            // 可以删掉，也可以打开注释替换为空字符串
//            [tmpArr addObject:@""];
            
        }else if ([obj isKindOfClass:[NSArray class]]) {
            
            [tmpArr addObject:[self solveNullArray:obj]];
            
        }else if ([obj isKindOfClass:[NSDictionary class]]) {
            
            [tmpArr addObject:[self solveNullDictionry:obj]];
            
        }else{
            
            [tmpArr addObject:obj];
            
        }
        
    }
    
    return tmpArr;
}

+ (NSMutableDictionary *)solveNullDictionry:(NSDictionary *)dictionryObject{
    
    NSMutableDictionary *tmpDict = [NSMutableDictionary dictionaryWithDictionary:dictionryObject];
    
    for (id <NSCopying> key in [dictionryObject allKeys]) {
        
        id value = (NSDictionary *)dictionryObject[key];
        
        if (!value || [value isEqual:[NSNull null]]) {
            
            // 删除对应key
            [tmpDict removeObjectForKey:key];
            
            // 或者替换为空字符串
//            tmpDict[key] = @"";
            
        } else if ([value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSDictionary class]]) {
            
            tmpDict[key] = [self jk_deleteJsonNullWithJsonObject:value];
            
        }
    }
    
    return tmpDict;
}

+ (id)jk_deleteJsonNullWithJsonObject:(id)jsonObject{
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        
        return [self solveNullArray:jsonObject];
        
    }else if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        
        return [self solveNullDictionry:jsonObject];
        
    }else if ([jsonObject isKindOfClass:[NSNull class]]) {
        
        return nil;
        
    }
    
    return jsonObject;
}
@end
