//
//  JKFileExplorerManager.h
//  TestCocoaHTTPserver
//
//  Created by albert on 2018/11/8.
//  Copyright © 2018 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JKFileExplorerConst.h"

@interface JKFileExplorerManager : NSObject

+ (void)initializeWithAirDropFolderName:(NSString *)folderName;

//+ (NSMutableArray *)currentPresentedNavArr;

+ (void)showWithViewController:(UIViewController *)viewController;

+ (void)showWithDirectory:(NSString *)directory
               sortConfig:(JKFileExplorerSortConfig)sortConfig
           viewController:(UIViewController *)viewController;

+ (void)handleOpenURL:(NSURL *)URL;
@end
