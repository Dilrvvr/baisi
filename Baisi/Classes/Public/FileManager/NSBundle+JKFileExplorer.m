//
//  NSBundle+JKFileExplorer.m
//  Baisi
//
//  Created by albert on 2018/11/10.
//  Copyright © 2018 安永博. All rights reserved.
//

#import "NSBundle+JKFileExplorer.h"
#import "JKFileExplorerManager.h"

@implementation NSBundle (JKFileExplorer)

+ (instancetype)jk_fileExplorerBundle {
    
    static NSBundle *fileExplorerBundle = nil;
    
    if (fileExplorerBundle == nil) {
        
        // 这里不使用mainBundle是为了适配pod 1.x和0.x
        fileExplorerBundle = [NSBundle bundleWithPath:[[NSBundle bundleForClass:[JKFileExplorerManager class]] pathForResource:@"JKFileExplorer" ofType:@"bundle"]];
    }
    
    return fileExplorerBundle;
}

+ (NSString *)jk_fileTypeCommonImageName{
    return @"file_type_common";
}

+ (NSDictionary *)jk_fileTypeIconDict{
    
    static NSDictionary *dict = nil;
    
    if (dict == nil) {
        
        dict = [NSDictionary dictionaryWithContentsOfFile:[[self jk_fileExplorerBundle] pathForResource:@"JKExplorerFileTypeIcon" ofType:@"plist"]];
    }
    
    return dict;
}

+ (UIImage *)jk_fileTypeFolderImage {
    
    static UIImage *fileTypeDirectoryImage = nil;
    
    if (fileTypeDirectoryImage == nil) {
        
        fileTypeDirectoryImage = [self jk_imageWithName:@"file_type_folder" type:@"png"];
    }
    
    return fileTypeDirectoryImage;
}

+ (UIImage *)jk_fileTypeUnknownImage{
    
    static UIImage *fileTypeUnknownImage = nil;
    
    if (fileTypeUnknownImage == nil) {
        
        fileTypeUnknownImage = [self jk_imageWithName:@"file_type_unknown" type:@"png"];
    }
    
    return fileTypeUnknownImage;
}

/** 默认type为png */
+ (UIImage *)jk_imageWithName:(NSString *)name{
    
    return [self jk_imageWithName:name type:@"png"];
}

/** 名称和type分开 */
+ (UIImage *)jk_imageWithName:(NSString *)name type:(NSString *)type{
    
    return [[[UIImage imageWithContentsOfFile:[[self jk_fileExplorerBundle] pathForResource:name ofType:type]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}
@end
