//
//  JKMediaLibraryEngine.m
//  Baisi
//
//  Created by albert on 2019/1/29.
//  Copyright © 2019 安永博. All rights reserved.
//

#import "JKMediaLibraryEngine.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation JKMediaLibraryEngine
/*
// MARK:- 判断是否有权限
- (void)requestAuthorizationForMediaLibrary {
    
    __weak typeof(self) weakSelf = self;
    
    // 请求媒体资料库权限
    MPMediaLibraryAuthorizationStatus authStatus = [MPMediaLibrary authorizationStatus];
    
    if (authStatus != MPMediaLibraryAuthorizationStatusAuthorized) {
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString *appName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
        if (appName == nil) {
            appName = @"APP";
        }
        NSString *message = [NSString stringWithFormat:@"允许%@访问你的媒体资料库？", appName];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"警告" message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }];
        
        UIAlertAction *setAction = [UIAlertAction actionWithTitle:@"设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if ([[UIApplication sharedApplication] canOpenURL:url])
            {
                [[UIApplication sharedApplication] openURL:url];
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }
        }];
        
        [alertController addAction:okAction];
        [alertController addAction:setAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
} //*/
@end
