//
//  JKFileExplorerDownloadTaskCell.m
//  Baisi
//
//  Created by albert on 2019/1/14.
//  Copyright © 2019 安永博. All rights reserved.
//

#import "JKFileExplorerDownloadTaskCell.h"
#import "JKCircularProgressView.h"
#import "JKFileExplorerDownloadItem.h"

@interface JKFileExplorerDownloadTaskCell ()

/** urlLabel */
@property (nonatomic, weak) UILabel *urlLabel;

/** fileNameLabel */
@property (nonatomic, weak) UILabel *fileNameLabel;

/** circularProgressView */
@property (nonatomic, weak) JKCircularProgressView *circularProgressView;

/** bottomLineView */
@property (nonatomic, weak) UIView *bottomLineView;
@end

@implementation JKFileExplorerDownloadTaskCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

#pragma mark
#pragma mark - 初始化

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initialization];
    }
    return self;
}

/** 初始化自身属性 交给子类重写 super自动调用该方法 */
- (void)initializeProperty{
    
}

/** 构造函数初始化时调用 注意调用super */
- (void)initialization{
    
    [self initializeProperty];
    [self createUI];
    [self layoutUI];
    [self initializeUIData];
}

/** 创建UI 交给子类重写 super自动调用该方法 */
- (void)createUI{
    
    UILabel *urlLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    urlLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:urlLabel];
    _urlLabel = urlLabel;
    
    UILabel *fileNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    fileNameLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:fileNameLabel];
    _fileNameLabel = fileNameLabel;
    
    JKCircularProgressView *circularProgressView = [[JKCircularProgressView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    circularProgressView.progressLabel.font = [UIFont systemFontOfSize:11];
    [self.contentView addSubview:circularProgressView];
    _circularProgressView = circularProgressView;
    
    // 底部分隔线
    UIView *bottomLineView = [[UIView alloc] init];
    bottomLineView.backgroundColor = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1];
    [self.contentView addSubview:bottomLineView];
    _bottomLineView = bottomLineView;
}

/** 布局UI 交给子类重写 super自动调用该方法 */
- (void)layoutUI{
    
}

/** 初始化UI数据 交给子类重写 super自动调用该方法 */
- (void)initializeUIData{
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.urlLabel.frame = CGRectMake(15, 8, self.contentView.frame.size.width - 15 - 40 - 15 - 15, 22);
    self.fileNameLabel.frame = CGRectMake(15, 30, self.contentView.frame.size.width - 15 - 40 - 15 - 15, 22);
    self.circularProgressView.frame = CGRectMake(self.contentView.frame.size.width - 15 - 40, 10, 40, 40);
    self.bottomLineView.frame = CGRectMake(0, self.contentView.frame.size.height - 0.5, self.contentView.frame.size.width, 0.5);
}

#pragma mark
#pragma mark - 点击事件


#pragma mark
#pragma mark - 发送请求


#pragma mark
#pragma mark - 处理数据


#pragma mark
#pragma mark - 赋值

- (void)setItem:(JKFileExplorerDownloadItem *)item{
    _item = item;
    
    __weak typeof(self) weakSelf = self;
    [item setProgressBlock:^(JKFileExplorerDownloadItem *item) {
        
        if (item == weakSelf.item) {
            
            weakSelf.circularProgressView.progress = item.downloadProgress;
        }
    }];
    
    self.urlLabel.text = item.downloadUrl;
    self.fileNameLabel.text = item.fileName;
    self.circularProgressView.progress = item.downloadProgress;
}

#pragma mark
#pragma mark - Property





- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
