//
//  JKFileExplorerDownloadTaskViewController.h
//  Baisi
//
//  Created by albert on 2019/1/14.
//  Copyright © 2019 安永博. All rights reserved.
//

#import "JKFileExplorerBaseViewController.h"

@interface JKFileExplorerDownloadTaskViewController : JKFileExplorerBaseViewController

+ (instancetype)addTaskWithUrl:(NSString *)url
                     directory:(NSString *)directory
                      fileName:(NSString *)fileName;
@end
