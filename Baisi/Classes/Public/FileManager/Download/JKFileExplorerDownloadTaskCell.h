//
//  JKFileExplorerDownloadTaskCell.h
//  Baisi
//
//  Created by albert on 2019/1/14.
//  Copyright © 2019 安永博. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKFileExplorerDownloadItem;

@interface JKFileExplorerDownloadTaskCell : UITableViewCell

/** item */
@property (nonatomic, strong) JKFileExplorerDownloadItem *item;
@end
