//
//  JKFileExplorerAirDropShareManager.m
//  ButterflyiOS
//
//  Created by albert on 2019/1/21.
//  Copyright © 2019 qingka. All rights reserved.
//

#import "JKFileExplorerAirDropShareManager.h"

@implementation JKFileExplorerAirDropShareManager

+ (void)shareWithViewController:(UIViewController *)viewController URL:(NSURL *)URL{
    
    if (![URL isKindOfClass:[NSURL class]]) { return; }
    
    [self shareWithViewController:viewController URLArray:@[URL]];
}

+ (void)shareWithViewController:(UIViewController *)viewController
                       URLArray:(NSArray <NSURL *> *)URLArray{
    
    NSMutableArray *tmpArr = [NSMutableArray array];
    
    [URLArray enumerateObjectsUsingBlock:^(NSURL * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       
        if ([obj isKindOfClass:[NSURL class]]) {
            
            [tmpArr addObject:obj];
        }
    }];
    
    if (tmpArr.count <= 0) { return; }
    
    if (!viewController) {
        
        viewController = [UIApplication sharedApplication].delegate.window.rootViewController;
        
        while (viewController.presentedViewController) {
            
            viewController = viewController.presentedViewController;
        }
    }
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:tmpArr applicationActivities:nil];
    
    [viewController presentViewController:controller animated:YES completion:nil];
}
@end
