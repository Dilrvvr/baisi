//
//  JKFileExplorerTextEditViewController.h
//  Baisi
//
//  Created by albert on 2019/2/15.
//  Copyright © 2019 安永博. All rights reserved.
//

#import "JKFileExplorerBaseViewController.h"

@class JKFileExplorerItem;

@interface JKFileExplorerTextEditViewController : JKFileExplorerBaseViewController

/** folderItem */
@property (nonatomic, strong) JKFileExplorerItem *folderItem;

/** filePath */
@property (nonatomic, copy) NSString *filePath;

/** isCreateNewFile */
@property (nonatomic, assign) BOOL isCreateNewFile;
@end
