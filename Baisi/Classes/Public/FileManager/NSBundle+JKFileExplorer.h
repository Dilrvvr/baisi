//
//  NSBundle+JKFileExplorer.h
//  Baisi
//
//  Created by albert on 2018/11/10.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (JKFileExplorer)

+ (instancetype)jk_fileExplorerBundle;

+ (NSString *)jk_fileTypeCommonImageName;

+ (NSDictionary *)jk_fileTypeIconDict;

//+ (UIImage *)jk_fileTypeFolderImage;

//+ (UIImage *)jk_fileTypeUnknownImage;

/** 默认type为png */
+ (UIImage *)jk_imageWithName:(NSString *)name;

/** 名称和type分开 */
+ (UIImage *)jk_imageWithName:(NSString *)name type:(NSString *)type;
@end
