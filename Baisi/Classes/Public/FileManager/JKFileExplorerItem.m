//
//  JKFileExplorerItem.m
//  TestCocoaHTTPserver
//
//  Created by albert on 2018/11/8.
//  Copyright © 2018 albert. All rights reserved.
//

#import "JKFileExplorerItem.h"
#import "NSBundle+JKFileExplorer.h"

@implementation JKFileExplorerItem

- (instancetype)init{
    if (self = [super init]) {
        
        _subTotalCountString = @"loading...";
    }
    return self;
}

#pragma mark
#pragma mark - 文件路径

// 文件路径
- (void)setFilePath:(NSString *)filePath{
    _filePath = [filePath copy];
    
    // 格式化文件路径
    [self formatFilePath:_filePath];
}

// 格式化文件路径
- (void)formatFilePath:(NSString *)filePath{
    
    if (!filePath) { return; }
    
    _previewItemURL = [NSURL fileURLWithPath:filePath];
    
    _relativePath = [filePath stringByReplacingOccurrencesOfString:[NSHomeDirectory() lastPathComponent] withString:@"Home"];
    
    _fileDirectory = [filePath stringByDeletingLastPathComponent];
    
    self.fileName = [filePath lastPathComponent];
}

#pragma mark
#pragma mark - 获取文件属性错误

- (void)setFileAttributeError:(NSError *)fileAttributeError{
    _fileAttributeError = fileAttributeError;
    
    if (_fileAttributeError) {
        
        _fileSizeString = _fileAttributeError.localizedDescription;
    }
}

#pragma mark
#pragma mark - 获取子文件名列表错误

- (void)setFetchSubFileNameListError:(NSError *)fetchSubFileNameListError{
    _fetchSubFileNameListError = fetchSubFileNameListError;
    
    if (_fetchSubFileNameListError) {
        
        _subTotalCountString = _fetchSubFileNameListError.localizedDescription;
    }
}

#pragma mark
#pragma mark - 子文件名列表

- (void)setSubFileNameList:(NSArray *)subFileNameList{
    _subFileNameList = subFileNameList;
    
    _subTotalCount = _subFileNameList.count;
    
    _subTotalCountString = [NSString stringWithFormat:@"%lu 项", (unsigned long)_subTotalCount];
}

#pragma mark
#pragma mark - 文件名称

// 文件名称
- (void)setFileName:(NSString *)fileName{
    _fileName = [fileName copy];
    
    [self formatFileName:_fileName];
}

// 格式化文件名称
- (void)formatFileName:(NSString *)fileName{
    
    if (!fileName || !_fileType) { return; }
    
    if (_isFolder) { // 文件夹
        
        _fileSuffix = @"folder";
        _fileTypeImageName = @"file_type_folder";
        
        _previewItemTitle = fileName;
        
    } else {
        
        _fileSuffix = [fileName.pathExtension lowercaseString];
        
        _previewItemTitle = [fileName stringByDeletingPathExtension];
        
        if ([_fileSuffix isEqualToString:@""]) {
            
            _fileSuffix = @"?";
        }
        
        _fileTypeImageName = [[NSBundle jk_fileTypeIconDict] objectForKey:_fileSuffix];
        
    
        _fileTypeImageName = _fileTypeImageName ? _fileTypeImageName : [NSBundle jk_fileTypeCommonImageName];
    }
}

#pragma mark
#pragma mark - 文件类型

- (void)setFileType:(NSString *)fileType{
    _fileType = [fileType copy];
    
    // 格式化文件类型
    [self formatFileType:_fileType];
}

// 格式化文件类型
- (void)formatFileType:(NSString *)fileType{
    
    _isFolder = [fileType isEqualToString:NSFileTypeDirectory];
    
    [self formatFileName:_fileName];
}

#pragma mark
#pragma mark - 文件大小

- (void)setFileSize:(NSNumber *)fileSize{
    _fileSize = fileSize;
    
    // 格式化文件大小
    [self formatFileSize:_fileSize];
}

// 格式化文件大小
- (void)formatFileSize:(NSNumber *)fileSize{
    
    if (!fileSize) {
        
        _fileSizeString = @"unknown fileSize";
        
        return;
    }
    
    long long fileLength = [fileSize longLongValue];
    
    _fileSizeString = [NSByteCountFormatter stringFromByteCount:fileLength countStyle:(NSByteCountFormatterCountStyleFile)];
    
    /*
    if (fileLength < 1024) { // Byte
        
        _fileSizeString = [fileSize.stringValue stringByAppendingString:@" B"];
        
    } else if (fileLength < 1024 * 1024) { // KB
        
        CGFloat KB = fileLength * 1.0 / 1024.0;
        
        _fileSizeString = [NSString stringWithFormat:@"%.2f KB", KB];
        
    } else if (fileLength < 1024 * 1024 * 1024) { // MB
        
        CGFloat MB = fileLength * 1.0 / 1024.0 / 1024.0;
        
        _fileSizeString = [NSString stringWithFormat:@"%.2f MB", MB];
        
    } else { // GB
        
        CGFloat GB = fileLength * 1.0 / 1024.0 / 1024.0 / 1024.0;
        
        _fileSizeString = [NSString stringWithFormat:@"%.2f GB", GB];
    } //*/
}

#pragma mark
#pragma mark - 创建时间

// 创建时间
- (void)setCreationDate:(NSDate *)creationDate{
    _creationDate = creationDate;
    
    // 格式化创建时间
    [self formatCreationDate:_creationDate];
}

// 格式化创建时间
- (void)formatCreationDate:(NSDate *)creationDate{
    
    _creationDateTimeStamp = @([creationDate timeIntervalSince1970]);
    
    _creationDateString = [JKFileExplorerItem formatDate:creationDate format:nil];
}

#pragma mark
#pragma mark - 修改时间

// 修改时间
- (void)setModificationDate:(NSDate *)modificationDate{
    _modificationDate = modificationDate;
    
    // 格式化修改时间
    [self formatModificationDate:_modificationDate];
}

// 格式化修改时间
- (void)formatModificationDate:(NSDate *)modificationDate{
    
    _modificationDateTimeStamp = @([modificationDate timeIntervalSince1970]);
    
    _creationDateString = [JKFileExplorerItem formatDate:modificationDate format:nil];
}

/** 将NSDate转换为时间字符串 格式可自定义，若传nil则默认返回值格式：2016-08-19 12:59:00 */
+ (NSString *)formatDate:(NSDate *)date format:(NSString *)format{
    
    if (!date) { return @"unknown"; }
    
    NSDateFormatter *dateFmt = [[NSDateFormatter alloc] init];
    
    dateFmt.dateFormat = format ? format : @"yyyy-MM-dd HH:mm:ss";
    
    return [dateFmt stringFromDate:date];
}
@end
