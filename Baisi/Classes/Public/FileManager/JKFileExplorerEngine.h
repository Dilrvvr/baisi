//
//  JKFileExplorerEngine.h
//  TestCocoaHTTPserver
//
//  Created by albert on 2018/11/8.
//  Copyright © 2018 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JKFileExplorerConst.h"

@class JKFileExplorerItem;

@interface JKFileExplorerEngine : NSObject

+ (dispatch_queue_t)explorerEngineQueue;

+ (void)destroyEngineQueue;

/** 根据文件夹路径获取文件名列表 */
+ (void)fetchFileNameListWithDirectoryPath:(NSString *)directoryPath
                                   success:(void(^)(NSString *directoryPath, NSArray <NSString *> *fileNameList))success
                                   failure:(void(^)(NSError *error))failure;
/** 根据fileIetm获取文件名列表 */
+ (void)fetchFileNameListWithFileItem:(JKFileExplorerItem *)fileItem
                                   success:(void(^)(JKFileExplorerItem *fileItem, NSArray <NSString *> *fileNameList))success
                                   failure:(void(^)(NSError *error))failure;

/** 根据文件夹路径获取fileItem列表 排序参数 */
+ (void)fetchFileItemListWithDirectoryPath:(NSString *)directoryPath
                                sortConfig:(JKFileExplorerSortConfig)sortConfig
                                   success:(void(^)(NSString *directoryPath, NSArray <JKFileExplorerItem *> *fileItemList, NSArray <JKFileExplorerItem *> *folderList, NSArray <JKFileExplorerItem *> *fileList))success
                                   failure:(void(^)(NSError *error))failure;

/** 根据文件夹路径和文件名列表获取fileItem列表 排序参数 */
+ (void)fetchFileItemListWithDirectoryPath:(NSString *)directoryPath
                              fileNameList:(NSArray <NSString *> *)fileNameList
                                sortConfig:(JKFileExplorerSortConfig)sortConfig
                                   success:(void(^)(NSString *directoryPath, NSArray <JKFileExplorerItem *> *fileItemList, NSArray <JKFileExplorerItem *> *folderList, NSArray <JKFileExplorerItem *> *fileList,NSMutableDictionary *filePathItemDict))success;

/** 根据文件夹路径获取fileItem列表 未排序 */
+ (void)fetchFileItemListWithDirectoryPath:(NSString *)directoryPath
                                   success:(void(^)(NSString *directoryPath, NSArray <JKFileExplorerItem *> *fileItemList, NSMutableDictionary *filePathItemDict))success
                                   failure:(void(^)(NSError *error))failure;

/** 根据文件夹路径和fileNameList获取fileItem列表 未排序 */
+ (void)fetchFileItemListWithDirectoryPath:(NSString *)directoryPath
                              fileNameList:(NSArray <NSString *> *)fileNameList
                                    finish:(void(^)(NSString *directoryPath, NSArray <JKFileExplorerItem *> *fileItemList, NSMutableDictionary *filePathItemDict))finish;

/** 对fileItem列表排序 */
+ (void)sortFileItemList:(NSArray <JKFileExplorerItem *> *)fileItemList
              sortConfig:(JKFileExplorerSortConfig)sortConfig
                  finish:(void(^)(NSArray <JKFileExplorerItem *> *fileItemList, NSArray <JKFileExplorerItem *> *folderList, NSArray <JKFileExplorerItem *> *fileList))finish;

/** 根据文件夹路径异步创建一个fileItem */
+ (void)fetchFileItemWithDirectoryPath:(NSString *)directoryPath
                                finish:(void(^)(NSString *directoryPath, JKFileExplorerItem *fileItem))finish;


/** 根据文件路径创建JKFileExplorerItem */
+ (JKFileExplorerItem *)createFileItemWithFilePath:(NSString *)filePath;

/**
 * 检查重名，并返回合适的文件路径
 * filePath : 要检查重名的路径
 * sourceFilePath : 源文件，用于判断是否文件夹，来决定重名后的命名方式
 * 如果是文件夹，则直接加(1)，文件则在后缀前加(1)
 */
+ (NSString *)checkDuplicationNameWithFilePath:(NSString *)filePath sourceFilePath:(NSString *)sourceFilePath;

/**
 * 检查重名，并返回合适的文件路径
 * filePath : 要检查重名的路径
 * isFolder : 是否文件夹，决定重名后的命名方式
 * 如果是文件夹，则直接加(1)，文件则在后缀前加(1)
 */
+ (NSString *)checkDuplicationNameWithFilePath:(NSString *)filePath isFolder:(BOOL)isFolder;

/** 去除文件后缀，若最后一位是. 也移除掉 */
+ (NSString *)stringByDeletingPathExtensionAndDotWithString:(NSString *)string;

/** 解压文件 progress在子线程回调 */
+ (void)unZipWithFileItem:(JKFileExplorerItem *)fileItem
                 progress:(void(^)(CGFloat progress))progress
                 complete:(void (^_Nullable)(NSString *originalPath, NSString *unZippedPath, BOOL succeeded, NSError * _Nullable error))complete;

/** 压缩单个文件 progress在子线程回调 */
+ (void)zipWithFileItem:(JKFileExplorerItem *)fileItem
               password:(NSString *)password
               progress:(void(^)(CGFloat progress))progress
               complete:(void (^_Nullable)(NSString *originalPath, NSString *zippedPath, BOOL succeeded, NSError * _Nullable error))complete;

/** 批量压缩文件 progress在子线程回调 */
+ (void)multiZipWithFileItems:(NSArray <JKFileExplorerItem *> *)fileItems
                  zipFilePath:(NSString *)zipFilePath
                     password:(NSString *)password
                     progress:(void(^)(CGFloat progress))progress
                     complete:(void (^_Nullable)(NSString *zippedPath, BOOL succeeded, NSError * _Nullable error))complete;

/** 批量移动文件 */
+ (void)multiMoveItemAtPaths:(NSArray <NSString *> *)paths
                     toPaths:(NSArray <NSString *> *)toPaths
                    progress:(void(^)(CGFloat progress))progress
                    complete:(void(^)(NSArray <NSString *> *successPaths, NSArray <NSString *> *failurePaths))complete;

/** 复制单个文件 */
+ (void)copyItemAtPath:(NSString *)path
                toPath:(NSString *)toPath
               success:(void(^)(NSString *destinationPath))success
               failure:(void(^)(NSString *orginalPath, NSString *destinationPath, NSError *error))failure;

/** 批量复制文件 */
+ (void)multiCopyItemAtPaths:(NSArray <NSString *> *)paths
                     toPaths:(NSArray <NSString *> *)toPaths
                    progress:(void(^)(CGFloat progress))progress
                    complete:(void(^)(NSArray <NSString *> *successPaths, NSArray <NSString *> *failurePaths))complete;

/** 保存文件 */
+ (void)saveData:(NSData *)data
          toPath:(NSString *)toPath
         success:(void(^)(NSString *destinationPath))success
         failure:(void(^)(NSString *destinationPath, NSError *error))failure;

+ (NSURLSessionDownloadTask *)downLoadFileWithUrl:(NSString *)url
                                        directory:(NSString *)directory
                                         fileName:(NSString *)fileName
                                         progress:(void (^)(NSProgress *downloadProgress))progress
                                completionHandler:(void(^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler;

#pragma mark
#pragma mark - 获取当前磁盘信息

/// 获取磁盘信息，注意回调是在子线程
+ (void)checkDiskSizeComplete:(void(^)(NSString *totalSize, NSString *usedSize, NSString *freeSize))complete;
@end
