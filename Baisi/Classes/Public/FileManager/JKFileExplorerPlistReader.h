//
//  JKFileExplorerPlistReader.h
//  Baisi
//
//  Created by albert on 2018/11/30.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKFileExplorerPlistReader : NSObject

+ (void)readPlistWithFilePath:(NSString *)filePath complete:(void(^)(NSString *result))complete;
@end
