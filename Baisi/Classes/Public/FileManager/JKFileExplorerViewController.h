//
//  JKFileExplorerViewController.h
//  TestCocoaHTTPserver
//
//  Created by albert on 2018/11/8.
//  Copyright © 2018 albert. All rights reserved.
//

#import "JKFileExplorerBaseViewController.h"
#import "JKFileExplorerConst.h"

@class JKFileExplorerItem;

@interface JKFileExplorerViewController : JKFileExplorerBaseViewController

/** airDrop或从其它APP传过来的新文件 */
@property (nonatomic, strong) NSString *fileNewItemPath;

/** nextVC */
@property (nonatomic, weak) JKFileExplorerViewController *nextVC;

/** 将要操作的文件 */
@property (nonatomic, copy) NSArray <JKFileExplorerItem *> *operatingFileItems;

/** 排列样式 */
@property (nonatomic, assign) JKFileExplorerArrangeType arrangeType;

/** 排序方式 */
@property (nonatomic, assign) JKFileExplorerSortConfig sortConfig;

/** 文件夹路径 */
@property (nonatomic, copy) NSString *directoryPath;

/** currentDirectory */
@property (nonatomic, copy, readonly) NSString *currentDirectory;

/** viewItem */
@property (nonatomic, strong) JKFileExplorerItem *fileItem;

/** 是否根控制器 */
@property (nonatomic, assign) BOOL isRootViewController;

+ (void)showWithViewController:(UIViewController *)viewController;
@end
