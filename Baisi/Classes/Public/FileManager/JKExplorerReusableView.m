//
//  JKExplorerReusableView.m
//  Baisi
//
//  Created by albert on 2018/11/18.
//  Copyright © 2018 安永博. All rights reserved.
//

#import "JKExplorerReusableView.h"

@implementation JKExplorerReusableView

+ (Class)layerClass{
    
    return [JKExplorerFixReusableViewLayer class];
}
@end


#ifdef __IPHONE_11_0
@implementation JKExplorerFixReusableViewLayer

- (CGFloat)zPosition {
    return 0;
}

@end
#endif
