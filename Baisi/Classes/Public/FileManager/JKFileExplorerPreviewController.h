//
//  JKFileExplorerPreviewController.h
//  Baisi
//
//  Created by albert on 2018/11/18.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <QuickLook/QuickLook.h>

@interface JKFileExplorerPreviewController : QLPreviewController

/** isIphoneX */
@property (nonatomic, assign) BOOL isIphoneX;

+ (void)showWithPreviewItemList:(NSArray <id<QLPreviewItem>>*)previewItemList
                          index:(NSInteger)index
                 viewController:(UIViewController *)viewController;
@end
