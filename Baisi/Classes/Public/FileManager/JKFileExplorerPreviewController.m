//
//  JKFileExplorerPreviewController.m
//  Baisi
//
//  Created by albert on 2018/11/18.
//  Copyright © 2018 安永博. All rights reserved.
//

#import "JKFileExplorerPreviewController.h"
#import "NSBundle+JKFileExplorer.h"
#import "JKFileExplorerPlistReader.h"
#import "JKFileExplorerTextView.h"
#import "JKFileExplorerPreviewController.h"

@interface JKFileExplorerPreviewController () <QLPreviewControllerDataSource, QLPreviewControllerDelegate>

/** leftItem */
@property (nonatomic, strong) UIBarButtonItem *leftItem;

/** previewItemList */
@property (nonatomic, copy) NSArray *previewItemList;

/** textView */
@property (nonatomic, weak) JKFileExplorerTextView *textView;

/** indicatorView */
@property (nonatomic, weak) UIActivityIndicatorView *indicatorView;
@end

@implementation JKFileExplorerPreviewController

+ (void)showWithPreviewItemList:(NSArray <id<QLPreviewItem>>*)previewItemList
                          index:(NSInteger)index
                 viewController:(UIViewController *)viewController{
    
    JKFileExplorerPreviewController *vc = [[JKFileExplorerPreviewController alloc] init];
    
    vc.previewItemList = previewItemList;
    
    vc.dataSource = vc;
    vc.delegate = vc;
    
    if (index < previewItemList.count && index > 0) {
        
        vc.currentPreviewItemIndex = index;
    }
    
    [viewController.navigationController pushViewController:vc animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isIphoneX = NO;
    
    if (@available(iOS 11.0, *)) {
        
        _isIphoneX = [UIApplication sharedApplication].delegate.window.safeAreaInsets.bottom > 0;
    }
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (self.previewItemList.count == 1) {
        
        NSString *path = [[[self.previewItemList firstObject] previewItemURL] path];
        
        if ([[path pathExtension] isEqualToString:@"plist"]) {
            
            [self textView];
            
            [self parserPlistWithFilePath:path];
        }
        
    } else {
        
    }
    
//    UIImage *letItemImage = [NSBundle jk_imageWithName:@"nav_back@3x" type:@"png"];
//
//    self.leftItem = [[UIBarButtonItem alloc] initWithImage:letItemImage style:(UIBarButtonItemStylePlain) target:self action:@selector(leftItemClick)];
    
//    self.navigationItem.leftBarButtonItem = nil;
    
//    QLPreviewController *qlVc = [[QLPreviewController alloc] init];
//
//    qlVc.dataSource = self;
//    qlVc.delegate = self;
//
//    [self addChildViewController:qlVc];
//
//    qlVc.view.frame = self.view.bounds;
//    [self.view addSubview:qlVc.view];
}

- (void)parserPlistWithFilePath:(NSString *)filePath{
    
    [self.indicatorView startAnimating];
    
    [JKFileExplorerPlistReader readPlistWithFilePath:filePath complete:^(NSString *result) {
        
        self.textView.text = result ? result : @"未能读取到数据";
        
        if (!result) {
            
            self.textView.backgroundColor = nil;
        }
        
        [self.indicatorView stopAnimating];
    }];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
 
    if (!_textView) { return; }
    
    _textView.frame = CGRectMake(0, _isIphoneX ? 88 : 64, self.view.frame.size.width, self.view.frame.size.height - (_isIphoneX ? 88 : 64));
    
    _indicatorView.center = CGPointMake(_indicatorView.frame.size.width * 0.5, self.view.frame.size.height * 0.382);
}

#pragma mark
#pragma mark - <QLPreviewControllerDataSource, QLPreviewControllerDelegate>

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller{
    
    return self.previewItemList.count;
}

- (id<QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index{
    
    return [self.previewItemList objectAtIndex:index];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)leftItemClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (JKFileExplorerTextView *)textView{
    if (!_textView) {
        JKFileExplorerTextView *textView = [[JKFileExplorerTextView alloc] init];
        [self.view addSubview:textView];
        textView.backgroundColor = [UIColor whiteColor];
        textView.editable = NO;
        textView.contentInset = UIEdgeInsetsMake(0, 0, _isIphoneX ? 34 : 0, 0);
//        textView.textContainer.lineFragmentPadding = 0;
        textView.textContainerInset = UIEdgeInsetsMake(10, 10, 0, 5);
        
        UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleGray)];
        
        [self.view addSubview:indicatorView];
        
        _indicatorView = indicatorView;
        
        
        SEL selector = NSSelectorFromString(@"setContentInsetAdjustmentBehavior:");
        
        if ([textView respondsToSelector:selector]) {
            
            IMP imp = [textView methodForSelector:selector];
            void (*func)(id, SEL, NSInteger) = (void *)imp;
            func(textView, selector, 2);
            
            // [tbView performSelector:@selector(setContentInsetAdjustmentBehavior:) withObject:@(2)];
        }
        
        _textView = textView;
    }
    return _textView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
