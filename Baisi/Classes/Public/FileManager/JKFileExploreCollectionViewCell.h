//
//  JKFileExploreCollectionViewCell.h
//  TestCocoaHTTPserver
//
//  Created by albert on 2018/11/8.
//  Copyright © 2018 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JKFileExplorerConst.h"

@class JKFileExplorerItem;

@interface JKFileExploreCollectionViewCell : UICollectionViewCell

/** 点击详情按钮 */
@property (nonatomic, copy) void (^infoButtonClickBlock)(JKFileExplorerItem *fileItem);

/** 点击更多按钮 */
@property (nonatomic, copy) void (^moreButtonClickBlock)(JKFileExplorerItem *fileItem);

/** 点击删除按钮 */
@property (nonatomic, copy) void (^deleteButtonClickBlock)(JKFileExplorerItem *fileItem);

/** 监听更多操作显示 */
@property (nonatomic, copy) void (^moreActionWillShowClickBlock)(JKFileExplorerItem *fileItem);

/** fileItem */
@property (nonatomic, strong) JKFileExplorerItem *fileItem;

/** 排列样式 */
@property (nonatomic, assign) JKFileExplorerArrangeType arrangeType;

- (void)hideMoreActionContainerView;

- (void)refreshFileNewStatus;

- (void)enterEditingModeAnimated:(BOOL)animated;

- (void)exitEditingModeAnimated:(BOOL)animated;
@end
