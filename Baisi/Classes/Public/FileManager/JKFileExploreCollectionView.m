//
//  JKFileExploreCollectionView.m
//  Baisi
//
//  Created by albert on 2018/11/19.
//  Copyright © 2018 安永博. All rights reserved.
//

#import "JKFileExploreCollectionView.h"

@interface JKFileExploreCollectionView () <UIGestureRecognizerDelegate>

/** 第一次识别手势的类名 */
@property (nonatomic, copy) NSString *firstRecognizedClass;
@end

@implementation JKFileExploreCollectionView

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesEnded:touches withEvent:event];
    
    self.firstRecognizedClass = nil;
}

#pragma mark
#pragma mark - UIGestureRecognizerDelegate

//- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
//
//    NSString *className = NSStringFromClass([gestureRecognizer.view class]);
//
//    NSLog(@"gestureRecognizerShouldBegin手势->%@", className);
//
//    if (!self.firstRecognizedClass) {
//
//        self.firstRecognizedClass = className;
//
//        return YES;
//    }
//
//    if ([className isEqualToString:self.firstRecognizedClass]) {
//
//        return YES;
//    }
//
//    return NO;
//}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{

//    NSLog(@"collectionView手势->%@", NSStringFromClass([gestureRecognizer.view class]));
    
    // nav的侧滑不允许和scrollView.panGestureRecognizer同时响应
    if (gestureRecognizer == self.panGestureRecognizer &&
        [otherGestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        
        return NO;
    }
    
    return NO;
}

// 这个方法返回YES，第一个手势和第二个互斥时，第一个会失效
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
//
//    return YES;
//}

// 这个方法返回YES，第一个和第二个互斥时，第二个会失效
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
//
//    return YES;
//}
@end
