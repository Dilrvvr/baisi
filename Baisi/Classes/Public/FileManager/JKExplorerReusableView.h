//
//  JKExplorerReusableView.h
//  Baisi
//
//  Created by albert on 2018/11/18.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKExplorerReusableView : UICollectionReusableView

@end



#ifdef __IPHONE_11_0
@interface JKExplorerFixReusableViewLayer : CALayer

@end
#endif
