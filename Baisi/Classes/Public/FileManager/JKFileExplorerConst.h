//
//  JKFileExplorerConst.h
//  Baisi
//
//  Created by albert on 2018/11/16.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark
#pragma mark - 通知

/**
 * 有新文件添加 发一个此通知来让JKFileExplorer更新
 * 文件路径放在userInfo中，key为JKFileExplorerFileOperateDestinationFilePathKey
 */
UIKIT_EXTERN NSString * const JKFileExplorerDidAddFileNotification;

/**
 * 有文件被删除 发一个此通知来让JKFileExplorer更新
 * 文件路径放在userInfo中，key为JKFileExplorerFileOperateDestinationFilePathKey
 */
UIKIT_EXTERN NSString * const JKFileExplorerDidDeleteFileNotification;



/** 将要操作文件(移动/复制)的通知 */
UIKIT_EXTERN NSString * const JKFileExplorerWillOperateFileNotification;

/** 操作文件(移动/复制)完成的通知 */
UIKIT_EXTERN NSString * const JKFileExplorerDidOperateFileNotification;

/** 文件操作结果类型key */
UIKIT_EXTERN NSString * const JKFileExplorerFileOperateResultKey;

/** 文件操作完成的新文件路径key */
UIKIT_EXTERN NSString * const JKFileExplorerFileOperateDestinationFilePathsKey;

#pragma mark
#pragma mark - 枚举

/** 文件操作类型 */
typedef NS_ENUM(NSUInteger, JKFileExplorerFileOperateType) {
    
    /** 无类型 */
    JKFileExplorerFileOperateTypeNone = 0,
    
    /** 移动 */
    JKFileExplorerFileOperateTypeMove = 1,
    
    /** 复制 */
    JKFileExplorerFileOperateTypeCopy = 2,
};

/** 文件操作结果 */
typedef NS_ENUM(NSUInteger, JKFileExplorerFileOperateResult) {
    
    /** 取消 */
    JKFileExplorerFileOperateResultCancel = 0,
    
    /** 移动成功 */
    JKFileExplorerFileOperateResultMoveSuccess = 1,
    
    /** 移动失败 */
    JKFileExplorerFileOperateResultMoveFailed = 2,
    
    /** 复制成功 */
    JKFileExplorerFileOperateResultCopySuccess = 3,
    
    /** 复制失败 */
    JKFileExplorerFileOperateResultCopyFailed = 4,
};

/** 排列样式 */
typedef NS_ENUM(NSUInteger, JKFileExplorerArrangeType) {
    
    /** 无类型 */
    JKFileExplorerArrangeTypeNone = -1,
    
    /** 列表样式 */
    JKFileExplorerArrangeTypeRow = 0,
    
    /** 平铺样式 */
    JKFileExplorerArrangeTypeSquare = 1,
};

/** 排序标识 */
typedef NS_ENUM(NSUInteger, JKFileExplorerSortIdentifier) {
    
    /** 按文件名排序 */
    JKFileExplorerSortIdentifierFileName = 0,
    
    /** 按修改时间排序 */
    JKFileExplorerSortIdentifierFileSize,
    
    /** 按创建时间排序 */
    JKFileExplorerSortIdentifierCreateDate,
    
    /** 按修改时间排序 */
    JKFileExplorerSortIdentifierModificationDate,
};

/** 升序降序 */
typedef NS_ENUM(NSUInteger, JKFileExplorerSortType) {
    
    /** 升序 */
    JKFileExplorerSortTypeAsc = 0,
    
    /** 降序 */
    JKFileExplorerSortTypeDesc,
};

/** 文件夹位置 */
typedef NS_ENUM(NSUInteger, JKFileExplorerSortFolderLocation) {
    
    /** 文件夹在上 */
    JKFileExplorerSortFolderLocationTop = 0,
    
    /** 与文件混在一起 */
    JKFileExplorerSortFolderLocationMix,
    
    /** 文件夹在下 */
    JKFileExplorerSortFolderLocationBottom,
};

#pragma mark
#pragma mark - 结构体

typedef struct _JKFileExplorerSortConfig {
    
    /** 排序标识 */
    JKFileExplorerSortIdentifier sortIdentifier;
    
    /** 升序降序 */
    JKFileExplorerSortType sortType;
    
    /** 文件夹位置 */
    JKFileExplorerSortFolderLocation folderLocation;
    
} JKFileExplorerSortConfig;

