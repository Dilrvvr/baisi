//
//  JKFileExploreCollectionViewCell.m
//  TestCocoaHTTPserver
//
//  Created by albert on 2018/11/8.
//  Copyright © 2018 albert. All rights reserved.
//

#import "JKFileExploreCollectionViewCell.h"
#import "NSBundle+JKFileExplorer.h"
#import "JKFileExplorerItem.h"

@interface JKFileExploreCollectionViewCell () <UIGestureRecognizerDelegate>
{
    /** 滑动手势的最大距离 */
    CGFloat maxDistance;
    
    /** minCenterX */
    CGFloat minCenterX;
    
    /** maxCenterX */
    CGFloat maxCenterX;
    
    /** contentCenter */
    CGPoint contentCenter;
}

/** containerView */
@property (nonatomic, weak) UIView *containerView;

/** fileNameLabel */
@property (nonatomic, weak) UILabel *fileNameLabel;

/** fileSizeOrCountLabel */
@property (nonatomic, weak) UILabel *fileSizeOrCountLabel;

/** fileIconImageView */
@property (nonatomic, weak) UIImageView *fileIconImageView;

/** 文件后缀 */
@property (nonatomic, weak) UILabel *fileSuffixLabel;

/** infoButton */
@property (nonatomic, weak) UIButton *infoButton;

/** rightArrowImageView */
@property (nonatomic, weak) UIImageView *rightArrowImageView;

/** bottomLineView */
@property (nonatomic, weak) UIView *bottomLineView;

/** coverMoreActionView */
@property (nonatomic, weak) UIButton *coverMoreActionView;

/** moreActionContainerView */
@property (nonatomic, weak) UIButton *moreActionContainerView;

/** 展示新文件状态的view */
@property (nonatomic, weak) UIView *fileNewStatusView;

/** closeButton */
@property (nonatomic, weak) UIButton *closeButton;

/** deleteButton */
@property (nonatomic, weak) UIButton *deleteButton;

/** moreButton */
@property (nonatomic, weak) UIButton *moreButton;

/** selectStatusView */
@property (nonatomic, weak) UIView *selectStatusView;

/** selectButton */
@property (nonatomic, weak) UIButton *selectButton;

/** panGesture */
@property (nonatomic, strong) UIPanGestureRecognizer *panGesture;

/** longPressGesture */
@property (nonatomic, strong) UILongPressGestureRecognizer *longPressGesture;
@end

@implementation JKFileExploreCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initialization];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self initialization];
    }
    return self;
}

- (void)initialization{
    
    maxDistance = 100;
    
    //self.contentView.layer.anchorPoint = CGPointMake(0, 0.5);
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.selectedBackgroundView = [[UIView alloc] init];
    
    self.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:224.0/255.0 green:225.0/255.0 blue:226.0/255.0 alpha:1];
    
    [self buildUI];
    
    self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
    
    self.panGesture.delegate = self;
    
    [self addGestureRecognizer:self.panGesture];
    
    self.longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [self.contentView addGestureRecognizer:self.longPressGesture];
}

- (void)longPress:(UILongPressGestureRecognizer *)gesture{
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        [self refreshFileNewStatus];
        
        [self showMoreActionContainerView];
    }
}

- (void)panGestureAction:(UIPanGestureRecognizer *)panGesture{
    
    if ([(UICollectionView *)self.superview isDragging]) {
        
        return;
    }
    
    switch (panGesture.state) {
        case UIGestureRecognizerStatePossible:
            break;
        case UIGestureRecognizerStateBegan:
        {
            contentCenter = self.contentView.center;
            
            minCenterX = self.frame.size.width * 0.5 - maxDistance;
            
            maxCenterX = self.frame.size.width * 0.5;
            
            [self refreshFileNewStatus];
            
            if (!self.fileItem.showMoreAction) {
                
                !self.moreActionWillShowClickBlock ? : self.moreActionWillShowClickBlock(self.fileItem);
            }
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            [(UICollectionView *)self.superview setScrollEnabled:NO];
            
            // 获取偏移
            CGPoint point = [panGesture translationInView:self.contentView];
            
            contentCenter.x += point.x;
            
            contentCenter.x = contentCenter.x > maxCenterX ? maxCenterX : contentCenter.x;
            
            contentCenter.x = contentCenter.x < minCenterX ? minCenterX : contentCenter.x;
            
            self.contentView.center = contentCenter;
            
            self.coverMoreActionView.center = self.contentView.center;
            
            // 归零
            [panGesture setTranslation:CGPointZero inView:self.contentView];
        }
            break;
            
        default:
        {
            [(UICollectionView *)self.superview setScrollEnabled:YES];
            
            if (self.contentView.center.x > self.frame.size.width * 0.5 - 50) { // 隐藏
                
                [self hideMoreActionContainerView];
                
            } else { // 显示
                
                [self showMoreActionContainerView];
            }
        }
            break;
    }
}

- (void)buildUI{
    
    // 展示选中状态的view
//    UIView *containerView = [[UIView alloc] init];
//    [self.contentView insertSubview:containerView atIndex:0];
//    _containerView = containerView;
    
    // 文件图标
    UIImageView *fileIconImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:fileIconImageView];
    _fileIconImageView = fileIconImageView;
    
    // 文件后缀，放在fileIconImageView上
    UILabel *fileSuffixLabel = [[UILabel alloc] init];
    fileSuffixLabel.numberOfLines = 0;
    fileSuffixLabel.textAlignment = NSTextAlignmentCenter;
    fileSuffixLabel.font = [UIFont systemFontOfSize:10];
    fileSuffixLabel.textColor = [UIColor colorWithRed:25.5/255.0 green:25.5/255.0 blue:25.5/255.0 alpha:1];
    [_fileIconImageView addSubview:fileSuffixLabel];
    _fileSuffixLabel = fileSuffixLabel;
    
    // 文件名
    UILabel *fileNameLabel = [[UILabel alloc] init];
    fileNameLabel.font = [UIFont systemFontOfSize:14];
    fileNameLabel.textColor = [UIColor colorWithRed:25.5/255.0 green:25.5/255.0 blue:25.5/255.0 alpha:1];
    [self.contentView addSubview:fileNameLabel];
    _fileNameLabel = fileNameLabel;
    
    // 文件大小或数量
    UILabel *fileSizeOrCountLabel = [[UILabel alloc] init];
    fileSizeOrCountLabel.font = [UIFont systemFontOfSize:11];
    fileSizeOrCountLabel.textColor = [UIColor colorWithRed:25.5/255.0 green:25.5/255.0 blue:25.5/255.0 alpha:1];
    [self.contentView addSubview:fileSizeOrCountLabel];
    _fileSizeOrCountLabel = fileSizeOrCountLabel;
    
    // 详情按钮
    UIButton *infoButton = [UIButton buttonWithType:(UIButtonTypeDetailDisclosure)];
    [self.contentView addSubview:infoButton];
    _infoButton = infoButton;
    
    [infoButton addTarget:self action:@selector(infoButtonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    
    // 右箭头
    UIImageView *rightArrowImageView = [[UIImageView alloc] init];
    rightArrowImageView.image = [NSBundle jk_imageWithName:@"cell_arrow_right" type:@"png"];
    [self.contentView addSubview:rightArrowImageView];
    _rightArrowImageView = rightArrowImageView;
    
    // 底部分隔线
    UIView *bottomLineView = [[UIView alloc] init];
    bottomLineView.backgroundColor = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1];
    [self.contentView addSubview:bottomLineView];
    _bottomLineView = bottomLineView;
    
    // 遮挡更多操作的view
    UIButton *coverMoreActionView = [UIButton buttonWithType:(UIButtonTypeCustom)];
    coverMoreActionView.backgroundColor = [UIColor whiteColor];
    coverMoreActionView.userInteractionEnabled = NO;
    [self insertSubview:coverMoreActionView belowSubview:self.selectedBackgroundView];
    _coverMoreActionView = coverMoreActionView;
    
    // 展示新文件状态的view
    UIView *fileNewStatusView = [[UIView alloc] init];
    fileNewStatusView.hidden = YES;
    fileNewStatusView.backgroundColor = [UIColor colorWithRed:0 green:48.0/255.0 blue:235.0/255.0 alpha:0.7];
    fileNewStatusView.userInteractionEnabled = NO;
    [coverMoreActionView insertSubview:fileNewStatusView atIndex:0];
    _fileNewStatusView = fileNewStatusView;
    
    // 展示选中状态的view
    UIView *selectStatusView = [[UIView alloc] init];
    selectStatusView.hidden = YES;
    selectStatusView.backgroundColor = [UIColor colorWithRed:0 green:48.0/255.0 blue:235.0/255.0 alpha:0.3];
    selectStatusView.userInteractionEnabled = NO;
    [coverMoreActionView insertSubview:selectStatusView atIndex:1];
    _selectStatusView = selectStatusView;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    [self layoutUI];
}

- (void)layoutUI{
    
    switch (_arrangeType) {
        case JKFileExplorerArrangeTypeSquare:
        {
            [self layoutForSquare];
        }
            break;
            
        default:
        {
            [self layoutForRow];
        }
            break;
    }
    
    //self.contentView.layer.frame = self.contentView.bounds;
    //self.contentView.layer.position = CGPointMake(0, self.contentView.bounds.size.height * 0.5);
    
    self.coverMoreActionView.frame = self.bounds;
    self.fileNewStatusView.frame = self.coverMoreActionView.bounds;
    
    self.selectStatusView.frame = self.coverMoreActionView.bounds;
}

- (void)layoutForRow{
    
    _selectButton.frame = CGRectMake(_fileItem.isEditing ? 0 : -self.frame.size.height, 0, self.frame.size.height, self.frame.size.height);
    
    [_selectButton setImageEdgeInsets:UIEdgeInsetsZero];
    
    if (_fileItem.isEditing) {
        
        self.contentView.frame = CGRectMake(CGRectGetMaxX(_selectButton.frame), 0, self.frame.size.width - CGRectGetMaxX(_selectButton.frame), self.frame.size.height);
    } else {
        
        self.contentView.frame = self.bounds;
    }
    
    CGFloat cellWidth = self.contentView.frame.size.width;
    CGFloat cellHeight = self.contentView.frame.size.height;
    
    self.moreActionContainerView.frame = CGRectMake(cellWidth - 100, 0, 100, cellHeight);
    
    self.moreButton.frame = CGRectMake(0, 0, 50, cellHeight);
    
    self.deleteButton.frame = CGRectMake(50, 0, 50, cellHeight);
    
    // 横向间距
    CGFloat horizontalMargin = 15;
    
    self.fileIconImageView.frame = CGRectMake(10, (cellHeight - 40) * 0.5, 40, 40);
    
    self.fileSuffixLabel.frame = self.fileIconImageView.bounds;
    
    self.infoButton.frame = CGRectMake(cellWidth - horizontalMargin - self.infoButton.frame.size.width, (cellHeight - self.infoButton.frame.size.height) * 0.5, self.infoButton.frame.size.width, self.infoButton.frame.size.height);
    
    self.rightArrowImageView.frame = CGRectMake(cellWidth - horizontalMargin - 7, (cellHeight - 12) * 0.5, 7, 12);
    
    CGFloat fileNameLabelX = CGRectGetMaxX(self.fileIconImageView.frame) + 10;
    CGFloat fileNameLabelW = self.infoButton.frame.origin.x - 10 - fileNameLabelX;
    CGFloat fileNameLabelH = ceil(self.fileNameLabel.font.lineHeight);
    CGFloat fileNameLabelY = cellHeight * 0.5 - fileNameLabelH - 2.5;
    
    self.fileNameLabel.frame = CGRectMake(fileNameLabelX, fileNameLabelY, fileNameLabelW, fileNameLabelH);
    
    self.fileSizeOrCountLabel.frame = CGRectMake(fileNameLabelX, cellHeight * 0.5 + 2.5, fileNameLabelW, ceil(self.fileSizeOrCountLabel.font.lineHeight));
    
    self.bottomLineView.frame = CGRectMake(0, cellHeight - 0.5, cellWidth, 0.3);
}

- (void)layoutForSquare{
    
    _selectButton.frame = CGRectMake(self.frame.size.width - 40, self.frame.size.height - 40, 40, 40);
    
    [_selectButton setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 0, 0)];
    
    CGFloat cellWidth = self.contentView.frame.size.width;
    CGFloat cellHeight = self.contentView.frame.size.height;
    
    self.moreActionContainerView.frame = self.bounds;
    
    self.closeButton.center = CGPointMake(cellWidth - self.closeButton.frame.size.width * 0.5, self.closeButton.frame.size.height * 0.5);
    
    self.moreButton.frame = CGRectMake((cellWidth - 100) / 3, (cellHeight - 50) * 0.5, 50, 50);
    
    self.deleteButton.frame = CGRectMake(CGRectGetMaxX(self.moreButton.frame) + self.moreButton.frame.origin.x, self.moreButton.frame.origin.y, 50, 50);
    
    self.fileIconImageView.frame = CGRectMake((cellWidth - 50) * 0.5, 10, 50, 50);
    
    self.fileSuffixLabel.frame = self.fileIconImageView.bounds;
    
    self.infoButton.hidden = YES;
//    self.infoButton.frame = CGRectMake(cellWidth - horizontalMargin - self.infoButton.frame.size.width, (cellHeight - self.infoButton.frame.size.height) * 0.5, self.infoButton.frame.size.width, self.infoButton.frame.size.height);
    self.rightArrowImageView.hidden = YES;
//    self.rightArrowImageView.frame = CGRectMake(cellWidth - horizontalMargin - 7, (cellHeight - 12) * 0.5, 7, 12);
    
    CGFloat fileNameLabelX = 5;
    CGFloat fileNameLabelW = cellWidth - 10;
    CGFloat fileNameLabelH = ceil(self.fileNameLabel.font.lineHeight);
    
    CGFloat fileSizeOrCountLabelH = ceil(self.fileSizeOrCountLabel.font.lineHeight);
    
    CGFloat sepH = (cellHeight - CGRectGetMaxY(self.fileIconImageView.frame) - fileNameLabelH - fileSizeOrCountLabelH) / 3;
    
    CGFloat fileNameLabelY = CGRectGetMaxY(self.fileIconImageView.frame) + sepH;
    
    self.fileNameLabel.frame = CGRectMake(fileNameLabelX, fileNameLabelY, fileNameLabelW, fileNameLabelH);
    
    self.fileSizeOrCountLabel.frame = CGRectMake(fileNameLabelX, (CGRectGetMaxY(self.fileNameLabel.frame) + sepH), fileNameLabelW, fileSizeOrCountLabelH);
    
    self.bottomLineView.frame = CGRectMake(0, cellHeight - 0.5, cellWidth, 0.3);
}

- (void)infoButtonClick:(UIButton *)button{
    
    !self.infoButtonClickBlock ? : self.infoButtonClickBlock(self.fileItem);
}

- (void)selectButtonClick:(UIButton *)button{
    
}

- (void)closeButtonClick:(UIButton *)button{
    
    [self hideMoreActionContainerView];
}

- (void)deleteButtonClick:(UIButton *)button{
    
    [self hideMoreActionContainerView];
    
    !self.deleteButtonClickBlock ? : self.deleteButtonClickBlock(self.fileItem);
}

- (void)moreButtonClick:(UIButton *)button{
    
    [self hideMoreActionContainerView];
    
    !self.moreButtonClickBlock ? : self.moreButtonClickBlock(self.fileItem);
}

- (void)setArrangeType:(JKFileExplorerArrangeType)arrangeType{
    _arrangeType = arrangeType;
    
    self.panGesture.enabled = _arrangeType == JKFileExplorerArrangeTypeRow;
    
    self.longPressGesture.enabled = !self.panGesture.enabled;
    
    self.closeButton.hidden = _arrangeType == JKFileExplorerArrangeTypeRow;
    
    self.fileNameLabel.textAlignment = _arrangeType == JKFileExplorerArrangeTypeSquare ? NSTextAlignmentCenter : NSTextAlignmentLeft;
    
    self.fileSizeOrCountLabel.textAlignment = self.fileNameLabel.textAlignment;
    
    self.deleteButton.layer.cornerRadius = _arrangeType == JKFileExplorerArrangeTypeSquare ? 25 : 0;
    self.moreButton.layer.cornerRadius = self.deleteButton.layer.cornerRadius;
    
    self.bottomLineView.hidden = _arrangeType == JKFileExplorerArrangeTypeSquare;
}

- (void)updateMoreActionContainerView{
    
    if (_arrangeType == JKFileExplorerArrangeTypeSquare) {
        
        if (_fileItem.showMoreAction) {
            
            [self insertSubview:self.moreActionContainerView aboveSubview:self.contentView];
            
        } else {
            
            [self insertSubview:self.moreActionContainerView atIndex:0];
        }
        
    } else {
        
        [self insertSubview:self.moreActionContainerView atIndex:0];
        
        if (_fileItem.showMoreAction) {
            
            self.contentView.center = CGPointMake(self.frame.size.width * 0.5 - 100, self.frame.size.height * 0.5);
            
        } else {
            
            self.contentView.center = CGPointMake(self.frame.size.width * 0.5, self.frame.size.height * 0.5);
        }
    }
}

#pragma mark
#pragma mark - 赋值

- (void)setFileItem:(JKFileExplorerItem *)fileItem{
    _fileItem = fileItem;
    
    self.userInteractionEnabled = !_fileItem.isOperating;
    
    self.coverMoreActionView.backgroundColor = _fileItem.isOperating ? [UIColor redColor] : [UIColor whiteColor];
    
    switch (_fileItem.fileOperateType) {
        case JKFileExplorerFileOperateTypeMove:
        {
            self.coverMoreActionView.backgroundColor = [UIColor redColor];
        }
            break;
        case JKFileExplorerFileOperateTypeCopy:
        {
            self.coverMoreActionView.backgroundColor = [UIColor colorWithRed:0 green:48.0/255.0 blue:235.0/255.0 alpha:1];
        }
            break;
            
        default:
        {
            self.coverMoreActionView.backgroundColor = [UIColor whiteColor];
        }
            break;
    }
    
    if (_fileItem.isNew) {
        
        self.fileNewStatusView.hidden = NO;
        
    } else {
        
        [self refreshFileNewStatus];
    }
    
    _fileItem.showMoreAction = NO;
    
    [self updateMoreActionContainerView];
    
    self.coverMoreActionView.userInteractionEnabled = NO;
    
    self.selectStatusView.hidden = !_fileItem.isEditing || !_fileItem.isSelected;
    
    _selectButton.selected = _fileItem.isSelected;
    
    if (_fileItem.isEditing) {
        
        [self enterEditingModeAnimated:NO];
        
    } else {
        
        [self exitEditingModeAnimated:NO];
    }
    
    self.deleteButton.enabled = _fileItem.deletable;
    
    self.fileSuffixLabel.text = _fileItem.fileSuffix;
    self.fileNameLabel.text = _fileItem.fileName;
    self.fileIconImageView.image = (_fileItem.fileTypeImageName == nil) ? nil : [NSBundle jk_imageWithName:_fileItem.fileTypeImageName];
    self.fileSuffixLabel.hidden = (_fileItem.fileTypeImageName != nil);
    
    self.rightArrowImageView.hidden = !_fileItem.isFolder;
    self.infoButton.hidden = !self.rightArrowImageView.hidden;
    
    self.fileIconImageView.backgroundColor = (_fileItem.fileTypeImageName == nil) ? [UIColor orangeColor] : nil;
    
    self.fileSizeOrCountLabel.text = _fileItem.isFolder ? _fileItem.subTotalCountString : _fileItem.fileSizeString;
    
    __weak typeof(self) weakSelf = self;
    
    if (!_fileItem.loadSubFileNameListComplete) {
        
        [_fileItem setLoadSubFileNameListComplete:^(JKFileExplorerItem *fileItem) {
            
            if (weakSelf && (weakSelf.fileItem == fileItem)) {
                
                weakSelf.fileSizeOrCountLabel.text = fileItem.subTotalCountString;
            }
        }];
    }
    
    [_fileItem setFileInfoDidChange:^(JKFileExplorerItem *fileItem) {
        
        if (weakSelf.fileItem == fileItem ||
            [weakSelf.fileItem.filePath isEqualToString:fileItem.filePath]) {
            
            weakSelf.fileItem = fileItem;
        }
    }];
    
    //NSLog(@"%@-->fileItem.isSelected-->%d-->selectStatusView.hidden-->%d", _fileItem.fileName, fileItem.isSelected, self.selectStatusView.hidden);
}

- (void)setHighlighted:(BOOL)highlighted{
    
    if (self.fileItem.showMoreAction) { return; }
    
    [super setHighlighted:highlighted];
    
}

- (void)setSelected:(BOOL)selected{
    
    if (self.fileItem.showMoreAction) { return; }
    
    [super setSelected:selected];
}

- (void)showMoreActionContainerView{
    
    self.fileItem.showMoreAction = YES;
    
    !self.moreActionWillShowClickBlock ? : self.moreActionWillShowClickBlock(self.fileItem);
    
    if (_arrangeType == JKFileExplorerArrangeTypeSquare) {
        
        [self showMoreActionContainerViewForSquare];
        
    } else {
        
        [self showMoreActionContainerViewForRow];
    }
    
    self.coverMoreActionView.userInteractionEnabled = YES;
}

- (void)showMoreActionContainerViewForSquare{
    
    self.moreActionContainerView.alpha = 0;
    [self insertSubview:self.moreActionContainerView aboveSubview:self.contentView];
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.moreActionContainerView.alpha = 1;
    }];
}

- (void)showMoreActionContainerViewForRow{
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.contentView.center = CGPointMake(self.frame.size.width * 0.5 - 100, self.frame.size.height * 0.5);
        
        self.coverMoreActionView.center = self.contentView.center;
    }];
}

- (void)hideMoreActionContainerView{
    
    self.fileItem.showMoreAction = NO;
    
    if (_arrangeType == JKFileExplorerArrangeTypeSquare) {
        
        [self hideMoreActionContainerViewForSquare];
        
    } else {
        
        [self hideMoreActionContainerViewForRow];
    }
    
    self.coverMoreActionView.userInteractionEnabled = NO;
}

- (void)hideMoreActionContainerViewForSquare{
    
    [self setHighlighted:NO];
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.moreActionContainerView.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        self.moreActionContainerView.alpha = 1;
        
        [self insertSubview:self.moreActionContainerView atIndex:0];
    }];
}

- (void)hideMoreActionContainerViewForRow{
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.contentView.center = CGPointMake(self.frame.size.width * 0.5, self.frame.size.height * 0.5);
        
        self.coverMoreActionView.center = self.contentView.center;
        
    } completion:^(BOOL finished) {
        
        [self insertSubview:self.moreActionContainerView atIndex:0];
    }];
}

- (void)refreshFileNewStatus{
    
    if (self.fileNewStatusView.hidden) { return; }
    
    self.fileItem.isNew = NO;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.fileNewStatusView.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        self.fileNewStatusView.hidden = YES;
        
        self.fileNewStatusView.alpha = 1;
    }];
}

#pragma mark
#pragma mark - 编辑模式

- (void)enterEditingModeAnimated:(BOOL)animated{
    
    self.fileItem.isEditing = YES;
    
    self.panGesture.enabled = NO;
    self.longPressGesture.enabled = NO;
    
    self.infoButton.userInteractionEnabled = NO;
    
    [self refreshFileNewStatus];
    
    if (self.fileItem.showMoreAction) {
        
        [self hideMoreActionContainerView];
    }
    
    if (_arrangeType == JKFileExplorerArrangeTypeSquare) {
        
        [self enterSquareEditingModeAnimated:animated];
        
    } else {
        
        [self enterRowEditingModeAnimated:animated];
    }
}

- (void)enterSquareEditingModeAnimated:(BOOL)animated{
    
    self.selectButton.frame = CGRectMake(self.frame.size.width - 30, self.frame.size.height - 30, 30, 30);
    
    if (!animated) {
        
        self.selectButton.alpha = 1;
        self.selectButton.hidden = NO;
        
        return;
    }
    
    self.selectButton.alpha = 0;
    self.selectButton.hidden = NO;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.selectButton.alpha = 1;
    }];
}

- (void)enterRowEditingModeAnimated:(BOOL)animated{
    
    if (!animated) {
        
        self.selectButton.frame = CGRectMake(0, 0, self.frame.size.height, self.frame.size.height);
        self.contentView.frame = CGRectMake(CGRectGetMaxX(self.selectButton.frame), 0, self.frame.size.width - CGRectGetMaxX(self.selectButton.frame), self.frame.size.height);
        self.selectButton.hidden = NO;
        
        return;
    }
    
    self.selectButton.frame = CGRectMake(-self.frame.size.height, 0, self.frame.size.height, self.frame.size.height);
    self.selectButton.hidden = NO;
    self.contentView.frame = self.bounds;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.selectButton.frame = CGRectMake(0, 0, self.frame.size.height, self.frame.size.height);
        self.contentView.frame = CGRectMake(CGRectGetMaxX(self.selectButton.frame), 0, self.frame.size.width - CGRectGetMaxX(self.selectButton.frame), self.frame.size.height);
    }];
}

- (void)exitEditingModeAnimated:(BOOL)animated{
    
    self.fileItem.isEditing = NO;
    self.fileItem.selected = NO;
    
    self.selectStatusView.hidden = YES;
    
    self.panGesture.enabled = _arrangeType == JKFileExplorerArrangeTypeRow;
    self.longPressGesture.enabled = !self.panGesture.enabled;
    
    self.infoButton.userInteractionEnabled = YES;
    
    if (_arrangeType == JKFileExplorerArrangeTypeSquare) {
        
        [self exitSquareEditingModeAnimated:animated];
        
    } else {
        
        [self exitRowEditingModeAnimated:animated];
    }
}

- (void)exitSquareEditingModeAnimated:(BOOL)animated{
    
    if (!animated) {
        
        self.selectButton.hidden = YES;
        self.selectButton.alpha = 1;
        
        return;
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.selectButton.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        self.selectButton.hidden = YES;
        self.selectButton.alpha = 1;
    }];
}

- (void)exitRowEditingModeAnimated:(BOOL)animated{
    
    if (!animated) {
        
        self.selectButton.frame = CGRectMake(-self.frame.size.height, 0, self.frame.size.height, self.frame.size.height);
        self.contentView.frame = self.bounds;
        
        self.selectButton.hidden = YES;
        
        return;
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.selectButton.frame = CGRectMake(-self.frame.size.height, 0, self.frame.size.height, self.frame.size.height);
        self.contentView.frame = self.bounds;
        
    } completion:^(BOOL finished) {
        
        self.selectButton.hidden = YES;
    }];
}

#pragma mark
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    if (gestureRecognizer == self.panGesture) {

        CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];

        if (point.x < 30) {

            return NO;
        }
    }
    
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    
//    NSLog(@"1\ngestureRecognizer-->%@\notherGestureRecognizer-->%@", gestureRecognizer.view, otherGestureRecognizer.view);
    
    return YES;
}

#pragma mark
#pragma mark - property

- (UIButton *)moreActionContainerView{
    if (!_moreActionContainerView) {
        UIButton *moreActionContainerView = [UIButton buttonWithType:(UIButtonTypeCustom)];
        moreActionContainerView.userInteractionEnabled = YES;
        moreActionContainerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
        [self insertSubview:moreActionContainerView atIndex:0];
        _moreActionContainerView = moreActionContainerView;
    }
    return _moreActionContainerView;
}

- (UIButton *)deleteButton{
    if (!_deleteButton) {
        UIButton *deleteButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
        deleteButton.backgroundColor = [UIColor redColor];
        [deleteButton setTitle:@"删除" forState:(UIControlStateNormal)];
        [deleteButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [deleteButton setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateDisabled)];
        [self.moreActionContainerView insertSubview:deleteButton atIndex:1];
        _deleteButton = deleteButton;
        
        [deleteButton addTarget:self action:@selector(deleteButtonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _deleteButton;
}

- (UIButton *)moreButton{
    if (!_moreButton) {
        UIButton *moreButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
        moreButton.backgroundColor = [UIColor lightGrayColor];
        [moreButton setTitle:@"更多" forState:(UIControlStateNormal)];
        [moreButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [self.moreActionContainerView insertSubview:moreButton atIndex:0];
        _moreButton = moreButton;
        
        [moreButton addTarget:self action:@selector(moreButtonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _moreButton;
}

- (UIButton *)closeButton{
    if (!_closeButton) {
        UIButton *closeButton = [UIButton buttonWithType:(UIButtonTypeContactAdd)];
        closeButton.transform = CGAffineTransformMakeRotation(M_PI * 0.25);
        [self.moreActionContainerView addSubview:closeButton];
        _closeButton = closeButton;
        
        [closeButton addTarget:self action:@selector(closeButtonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _closeButton;
}

- (UIButton *)selectButton{
    if (!_selectButton) {
        UIButton *selectButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        
//        selectButton.hidden = YES;
        
        [selectButton setImage:[NSBundle jk_imageWithName:@"file_select_off"] forState:(UIControlStateNormal)];
        [selectButton setImage:[NSBundle jk_imageWithName:@"file_select_on"] forState:(UIControlStateSelected)];
        [selectButton setImage:[NSBundle jk_imageWithName:@"file_select_on"] forState:(UIControlStateSelected | UIControlStateHighlighted)];
        
        selectButton.userInteractionEnabled = NO;
        [self addSubview:selectButton];
        _selectButton = selectButton;
        
        [selectButton addTarget:self action:@selector(selectButtonClick:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _selectButton;
}
@end
