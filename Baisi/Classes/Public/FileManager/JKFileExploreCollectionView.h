//
//  JKFileExploreCollectionView.h
//  Baisi
//
//  Created by albert on 2018/11/19.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JKFileExploreCollectionView : UICollectionView

@end

NS_ASSUME_NONNULL_END
