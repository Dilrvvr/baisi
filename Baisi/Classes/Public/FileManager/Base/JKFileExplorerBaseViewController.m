//
//  JKFileExplorerBaseViewController.m
//  Baisi
//
//  Created by albert on 2018/11/18.
//  Copyright © 2018 安永博. All rights reserved.
//

#import "JKFileExplorerBaseViewController.h"

@interface JKFileExplorerBaseViewController ()

/** 提示信息的数组 */
@property (nonatomic, strong) NSMutableArray *statusMessageArr;

/** 提示信息label */
@property (nonatomic, weak) UILabel *statusMessageLabel;
@end

@implementation JKFileExplorerBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isIphoneX = NO;
    
    if (@available(iOS 11.0, *)) {
        
        _isIphoneX = [UIApplication sharedApplication].delegate.window.safeAreaInsets.bottom > 0;
    }
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)buildUI{}

- (void)layoutUI{}

- (void)loadData{}

#pragma mark
#pragma mark - HUD

- (void)showStatusWithMessage:(NSString *)message{
    
    if (!message) { return; }
    
    [self.statusMessageArr addObject:message];
    
    if (self.statusMessageLabel.hidden) {
        
        [self showStatusMessage];
    }
}

- (void)showStatusMessage{
    
    if (self.statusMessageArr.count == 0) { return; }
    
    NSString *message = [self.statusMessageArr firstObject];
    
    if (![message isKindOfClass:[NSString class]]) {
        
        [self.statusMessageArr removeObject:message];
        
        return;
    }
    
    [self.view bringSubviewToFront:self.statusMessageLabel];
    
    self.statusMessageLabel.alpha = 0;
    self.statusMessageLabel.hidden = NO;
    
    self.statusMessageLabel.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    self.statusMessageLabel.text = message;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.statusMessageLabel.alpha = 1;
        
        self.statusMessageLabel.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.25 delay:1 options:0 animations:^{
            
            self.statusMessageLabel.alpha = 0;
            
            self.statusMessageLabel.transform = CGAffineTransformMakeScale(0.1, 0.1);
            
        } completion:^(BOOL finished) {
            
            if (self.statusMessageArr.count > 0) {
                
                [self.statusMessageArr removeObjectAtIndex:0];
            }
            
            if (self.statusMessageArr.count > 0) {
                
                [self performSelector:@selector(showStatusMessage) withObject:nil afterDelay:0.5];
                
            } else {
                
                self.statusMessageLabel.hidden = YES;
                self.statusMessageLabel.alpha = 1;
                self.statusMessageLabel.transform = CGAffineTransformIdentity;
            }
        }];
    }];
}

- (UILabel *)tipLabel{
    if (!_tipLabel) {
        UILabel *tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, ([UIScreen mainScreen].bounds.size.height - 200) * 0.5, [UIScreen mainScreen].bounds.size.width - 30, 200)];
        tipLabel.hidden = YES;
        tipLabel.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
        tipLabel.font = [UIFont systemFontOfSize:16];
        tipLabel.textAlignment = NSTextAlignmentCenter;
        tipLabel.numberOfLines = 0;
        [self.view addSubview:tipLabel];
        
        _tipLabel = tipLabel;
    }
    return _tipLabel;
}

- (UITabBar *)tabBar{
    if (!_tabBar) {
        UITabBar *tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - (self.isIphoneX ? 83 : 49), self.view.frame.size.width, (self.isIphoneX ? 83 : 49))];
        [self.view addSubview:tabBar];
        _tabBar = tabBar;
    }
    return _tabBar;
}

- (UILabel *)statusMessageLabel{
    if (!_statusMessageLabel) {
        
        UILabel *statusMessageLabel = [[UILabel alloc] init];
        
        statusMessageLabel.frame = CGRectMake((self.view.frame.size.width - 250) * 0.5, (self.view.frame.size.height - 98) * 0.5, 250, 98);
        statusMessageLabel.font = [UIFont systemFontOfSize:16];
        statusMessageLabel.textAlignment = NSTextAlignmentCenter;
        statusMessageLabel.textColor = [UIColor whiteColor];
        statusMessageLabel.numberOfLines = 0;
        statusMessageLabel.layer.backgroundColor = [[UIColor blackColor] CGColor];
        statusMessageLabel.layer.cornerRadius = 5;
        statusMessageLabel.userInteractionEnabled = NO;
        statusMessageLabel.hidden = YES;
        [self.view addSubview:statusMessageLabel];
        _statusMessageLabel = statusMessageLabel;
    }
    return _statusMessageLabel;
}

- (NSMutableArray *)statusMessageArr{
    if (!_statusMessageArr) {
        _statusMessageArr = [NSMutableArray array];
    }
    return _statusMessageArr;
}
@end
