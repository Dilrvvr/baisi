//
//  JKFileExploreBaseCollectionViewCell.m
//  Baisi
//
//  Created by albert on 2018/12/15.
//  Copyright © 2018 安永博. All rights reserved.
//

#import "JKFileExploreBaseCollectionViewCell.h"

@interface JKFileExploreBaseCollectionViewCell ()

@end

@implementation JKFileExploreBaseCollectionViewCell
#pragma mark
#pragma mark - 初始化

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initialization];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self initialization];
    }
    return self;
}

/** 初始化自身属性 交给子类重写 super自动调用该方法 */
- (void)initializeProperty{
    
}

/** 构造函数初始化时调用 注意调用super */
- (void)initialization{
    
    [self initializeProperty];
    [self createUI];
    [self layoutUI];
    [self initializeUIData];
}

/** 创建UI 交给子类重写 super自动调用该方法 */
- (void)createUI{
    
}

/** 布局UI 交给子类重写 super自动调用该方法 */
- (void)layoutUI{
    
}

/** 初始化UI数据 交给子类重写 super自动调用该方法 */
- (void)initializeUIData{
    
}

#pragma mark
#pragma mark - 点击事件


#pragma mark
#pragma mark - Property


@end
