//
//  JKFileExplorerBaseViewController.h
//  Baisi
//
//  Created by albert on 2018/11/18.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKFileExplorerBaseViewController : UIViewController

/** isIphoneX */
@property (nonatomic, assign) BOOL isIphoneX;

/** navTitle */
@property (nonatomic, copy) NSString *navTitle;

/** isEditing */
@property (nonatomic, assign) BOOL isEditing;

/** tipLabel */
@property (nonatomic, weak) UILabel *tipLabel;

/** tabBar */
@property (nonatomic, weak) UITabBar *tabBar;

- (void)buildUI;

- (void)layoutUI;

- (void)loadData;

- (void)showStatusWithMessage:(NSString *)message;
@end
