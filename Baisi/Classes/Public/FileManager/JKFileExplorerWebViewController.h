//
//  JKFileExplorerWebViewController.h
//  Baisi
//
//  Created by albert on 2018/11/8.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "JKFileExplorerBaseViewController.h"

@interface JKFileExplorerWebViewController : JKFileExplorerBaseViewController <WKNavigationDelegate, WKUIDelegate, UIScrollViewDelegate>

/** data */
@property (nonatomic, strong) NSData *data;

/** url */
@property (nonatomic, copy) NSURL *URL;

/** url */
@property (nonatomic, copy) NSString *urlString;

/** webview */
@property (weak, nonatomic, readonly) WKWebView *webView;

/** 加载进度条 */
@property (nonatomic, weak, readonly) UIProgressView *loadProgressView;

/** 是否隐藏滚动条 */
@property (nonatomic, assign) BOOL hideIndicator;

/** 监听scrollView松手时的contentOffset的block */
@property (nonatomic, copy) void (^tableViewDidEndDraggingBlock)(CGPoint tableViewContentOffset);

/** 加载完成的block */
@property (nonatomic, copy) void (^loadFinishedBlock)(JKFileExplorerWebViewController *webViewVc);

/** 开始加载的block */
@property (nonatomic, copy) void (^loadStartBlock)(JKFileExplorerWebViewController *webViewVc);

/** 监听canGoBack属性改变的block */
//@property (nonatomic, copy) void (^canGoBackBlock)(BOOL canGoBack);

/**
 * 加载网页类方法
 * urlStr   : 要加载的网页url
 * navTitle : 导航栏标题。如传nil则默认是网页本身的标题
 * pushedVc : 从哪个控制器push，传空则自己处理
 * callBackBlock : 回调
 */
+ (instancetype)loadUrlWithUrlStr:(NSString *)urlStr navTitle:(NSString *)navtitle pushedVc:(UIViewController *)pushedVc callBackBlock:(void(^)(NSDictionary *callBackparamDict))callBackBlock;

/**
 * 加载网页类方法
 * URL      : 要加载的网页URL
 * navTitle : 导航栏标题。如传nil则默认是网页本身的标题
 * pushedVc : 从哪个控制器push，传空则自己处理
 * callBackBlock : 回调
 */
+ (instancetype)loadUrlWithURL:(NSURL *)URL navTitle:(NSString *)navtitle pushedVc:(UIViewController *)pushedVc callBackBlock:(void(^)(NSDictionary *callBackparamDict))callBackBlock;
@end
