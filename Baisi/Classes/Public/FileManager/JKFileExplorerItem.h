//
//  JKFileExplorerItem.h
//  TestCocoaHTTPserver
//
//  Created by albert on 2018/11/8.
//  Copyright © 2018 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JKFileExplorerConst.h"

@interface JKFileExplorerItem : NSObject

/** 创建item时的错误 如文件路径不存在... */
//@property (nonatomic, copy) NSError *createItemError;

/** isEditing */
@property (nonatomic, assign) BOOL isEditing;

/** selected */
@property (nonatomic, assign, getter=isSelected) BOOL selected;

/** showMoreAction */
@property (nonatomic, assign) BOOL showMoreAction;

/** 能否删除 */
@property (nonatomic, assign) BOOL deletable;

/** 是否新增的 */
@property (nonatomic, assign) BOOL isNew;

/** 文件操作类型 */
@property (nonatomic, assign) JKFileExplorerFileOperateType fileOperateType;

/** 是否正在操作(移动/复制) */
@property (nonatomic, assign) BOOL isOperating;

/** 相对路径 将沙盒路径改为Home */
@property (nonatomic, copy) NSString *relativePath;

/** filePath */
@property (nonatomic, copy) NSString *filePath;

/** previewItemURL */
@property (nonatomic, strong) NSURL *previewItemURL;

/** 文件所在目录 */
@property (nonatomic, copy) NSString *fileDirectory;

/** fileName */
@property (nonatomic, copy) NSString *fileName;

/** fileSuffix */
@property (nonatomic, copy) NSString *fileSuffix;

/** 去除文件后缀的文件名 */
@property (nonatomic, copy) NSString *previewItemTitle;

/** 文件夹图标 */
@property (nonatomic, copy) NSString *fileTypeImageName;


/** 是否文件夹 */
@property (nonatomic, assign) BOOL isFolder;

/** 获取获取子文件列表完成 */
@property (nonatomic, copy) void (^loadSubFileNameListComplete)(JKFileExplorerItem *fileItem);

/** 文件信息改变 */
@property (nonatomic, copy) void (^fileInfoDidChange)(JKFileExplorerItem *fileItem);


/** 获取子文件列表出现的错误，nil表示未出错 */
@property (nonatomic, copy) NSError *fetchSubFileNameListError;

/** 文件夹中 子文件所有名称数组 */
@property (nonatomic, strong) NSArray *subFileNameList;

/** 子文件总数量描述 */
@property (nonatomic, copy) NSString *subTotalCountString;


/** 文件夹中 文件总数量 */
@property (nonatomic, assign) NSUInteger subTotalCount;

/** 文件夹中 子文件夹 数量 */
@property (nonatomic, assign) NSUInteger subFolderCount;

/** 文件夹中 子文件 数量 */
@property (nonatomic, assign) NSUInteger subFileCount;




/** 格式化后的文件大小 */
@property (nonatomic, copy) NSString *fileSizeString;

/** 格式化后的创建时间 */
@property (nonatomic, copy) NSString *creationDateString;

/** 创建时间的时间戳 */
@property (nonatomic, strong) NSNumber *creationDateTimeStamp;

/** 格式化后的修改时间 */
@property (nonatomic, copy) NSString *modificationDateString;

/** 修改时间的时间戳 */
@property (nonatomic, strong) NSNumber *modificationDateTimeStamp;



#pragma mark
#pragma mark - system attribute

/** 获取文件属性出现的错误 */
@property (nonatomic, strong) NSError *fileAttributeError;

/** 文件类型 */
@property (nonatomic, strong) NSString *fileType;

/** 文件大小 */
@property (nonatomic, strong) NSNumber *fileSize;

/** 创建时间 */
@property (nonatomic, strong) NSDate *creationDate;

/** 修改时间 */
@property (nonatomic, strong) NSDate *modificationDate;
@end
