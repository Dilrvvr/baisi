//
//  JKFileExplorerTextView.h
//  Baisi
//
//  Created by albert on 2018/11/30.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JKFileExplorerTextView : UITextView

@end

NS_ASSUME_NONNULL_END
