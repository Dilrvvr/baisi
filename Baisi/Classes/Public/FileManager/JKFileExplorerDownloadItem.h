//
//  JKFileExplorerDownloadItem.h
//  Baisi
//
//  Created by albert on 2019/1/14.
//  Copyright © 2019 安永博. All rights reserved.
//

#import "JKFileExplorerItem.h"

@interface JKFileExplorerDownloadItem : JKFileExplorerItem

/** downloadUrl */
@property (nonatomic, copy) NSString *downloadUrl;

/** downloadProgress */
@property (nonatomic, assign) double downloadProgress;

/** progressBlock */
@property (nonatomic, copy) void (^progressBlock)(JKFileExplorerDownloadItem *item);
@end
