//
//  JKFileExplorerTextView.m
//  Baisi
//
//  Created by albert on 2018/11/30.
//  Copyright © 2018 安永博. All rights reserved.
//

#import "JKFileExplorerTextView.h"

@implementation JKFileExplorerTextView

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    
    if (action == @selector(selectAll:)) {
        
        return YES;
    }
    
    return [super canPerformAction:action withSender:sender];
}

@end
