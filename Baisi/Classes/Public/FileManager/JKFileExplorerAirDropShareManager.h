//
//  JKFileExplorerAirDropShareManager.h
//  ButterflyiOS
//
//  Created by albert on 2019/1/21.
//  Copyright © 2019 qingka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKFileExplorerAirDropShareManager : NSObject

+ (void)shareWithViewController:(UIViewController *)viewController
                            URL:(NSURL *)URL;

+ (void)shareWithViewController:(UIViewController *)viewController
                       URLArray:(NSArray <NSURL *> *)URLArray;
@end
