//
//  JKFileExplorerConst.m
//  Baisi
//
//  Created by albert on 2018/11/16.
//  Copyright © 2018 安永博. All rights reserved.
//

#import "JKFileExplorerConst.h"



#pragma mark
#pragma mark - 通知

/**
 * 有新文件添加 发一个此通知来让JKFileExplorer更新
 * 文件路径放在userInfo中，key为JKFileExplorerFileOperateDestinationFilePathKey
 */
NSString * const JKFileExplorerDidAddFileNotification = @"JKFileExplorerDidAddFileNotification";

/**
 * 有文件被删除 发一个此通知来让JKFileExplorer更新
 * 文件路径放在userInfo中，key为JKFileExplorerFileOperateDestinationFilePathKey
 */
NSString * const JKFileExplorerDidDeleteFileNotification = @"JKFileExplorerDidDeleteFileNotification";

/** 将要操作文件(移动/复制)的通知 */
NSString * const JKFileExplorerWillOperateFileNotification = @"JKFileExplorerWillOperateFileNotification";

/** 操作文件(移动/复制)完成的通知 */
NSString * const JKFileExplorerDidOperateFileNotification = @"JKFileExplorerDidOperateFileNotification";

/** 文件操作结果类型key */
NSString * const JKFileExplorerFileOperateResultKey = @"JKFileExplorerFileOperateResultKey";

/** 文件操作完成的新文件路径key */
NSString * const JKFileExplorerFileOperateDestinationFilePathsKey = @"JKFileExplorerFileOperateDestinationFilePathKey";
