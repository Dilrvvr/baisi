
#import <UIKit/UIKit.h>

UIKIT_EXTERN BOOL JKIsIphoneX;


/** 微信AppKey */
UIKIT_EXTERN NSString * const JKWXAppkey;

/** 微信AppSecret */
UIKIT_EXTERN NSString * const JKWXAppSecret;

/** 微博appKey */
UIKIT_EXTERN NSString * const JKWeiboAppKey;

/** 微博appSecret */
UIKIT_EXTERN NSString * const JKWeiboAppSecret;

typedef enum{
    kPostTypeAll     = 1,  //全部
    kPostTypePicture = 10, //图片
    kPostTypeWord    = 29, //段子
    kPostTypeVideo   = 41, //视频
    kPostTypeVoice   = 31  //声音
}JKPostType;

/** 精华-顶部标题的高度 35 */
UIKIT_EXTERN CGFloat const titlesViewH;
/** 精华-顶部标题的Y 64 */
//UIKIT_EXTERN CGFloat const titlesViewY;

/** 精华-cell-间距 10 */
UIKIT_EXTERN CGFloat const JKPostCellMargin;
/** 精华-cell-文字内容的Y 50*/
UIKIT_EXTERN CGFloat const JKPostCellTextY;
/** 精华-cell-底部工具条高度 40 */
UIKIT_EXTERN CGFloat const JKPostCellBottomBar;

/** 精华-cell-图片帖子中图片的最大高度 640 */
UIKIT_EXTERN CGFloat const JKPostCellPictureMaxH;

/** JKUserModel-性别属性值-男 */
UIKIT_EXTERN NSString * const JKUserSexMale;
/** JKUserModel-性别属性值-女 */
UIKIT_EXTERN NSString * const JKUserSexFemale;

/** 精华-cell-最热评论标题高度 20 */
UIKIT_EXTERN CGFloat const JKPostCellTopCmtTitleH;

/** tabBar被选中的通知名字 */
UIKIT_EXTERN NSString * const JKTabBarDoubleTapRefreshNotification;
/** tabBar被选中的通知 - 被点击的控制器的index */
UIKIT_EXTERN NSString * const JKSelectedControllerIndexKey;
/** tabBar被选中的通知 - 被点击的控制器的 key */
UIKIT_EXTERN NSString * const JKSelectedControllerKey;

/** 重置播放器的通知 */
UIKIT_EXTERN NSString * const JKResetPlayerNotification;

/** 添加标签-间距 5 */
UIKIT_EXTERN CGFloat const JKTagMargin;
/** 标签-高度 25 */
UIKIT_EXTERN CGFloat const JKTagH;



/** 开启自动旋转 通知名字 */
UIKIT_EXTERN NSString * const JKTurnOnAutoRotateNotification;

/** 关闭自动旋转 通知名字 */
UIKIT_EXTERN NSString * const JKTurnOffAutoRotateNotification;



BOOL checkScreenX (void);



