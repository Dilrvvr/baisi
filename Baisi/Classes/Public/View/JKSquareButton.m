//
//  JKSquareButton.m
//  Baisi
//
//  Created by albert on 16/3/29.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKSquareButton.h"
#import "UIButton+WebCache.h"
#import "JKSquareModel.h"

@implementation JKSquareButton

- (void)setup{
    //文字居中
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    //设置背景图片，图片是有边缘的，所以就有了分割线效果
    [self setBackgroundImage:[UIImage imageNamed:@"mainCellBackground"] forState:(UIControlStateNormal)];
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self setup];
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)setSquare:(JKSquareModel *)square{
    _square = square;
    
    [self setTitle:square.name forState:(UIControlStateNormal)];
    [self sd_setImageWithURL:[NSURL URLWithString:square.icon] forState:(UIControlStateNormal)];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    //设置图片
    self.imageView.width = self.width * 0.5;
    self.imageView.height = self.imageView.width;
    self.imageView.y = self.height * 0.1;
    self.imageView.centerX = self.width * 0.5;
    
    //设置文字
    self.titleLabel.x = 0;
    self.titleLabel.y = CGRectGetMaxY(self.imageView.frame);
    self.titleLabel.width = self.width;
    self.titleLabel.height = self.height - self.titleLabel.y;  
}
@end
