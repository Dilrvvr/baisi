//
//  JKSquareButton.h
//  Baisi
//
//  Created by albert on 16/3/29.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKSquareModel;

@interface JKSquareButton : UIButton
/** 模型数据 */
@property (nonatomic, strong) JKSquareModel *square;
@end
