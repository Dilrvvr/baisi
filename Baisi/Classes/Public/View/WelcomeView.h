//
//  WelcomeView.h
//  01-微博动画
//
//  Created by xiaomage on 15/6/26.
//  Copyright (c) 2015年 xiaomage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeView : UIView

+ (void)show;

+ (instancetype)welcomeView;
@end
