//
//  JKTopWindow.m
//  Baisi
//
//  Created by albert on 16/3/28.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKTopWindow.h"

@implementation JKTopWindow

//强引用一下
static UIWindow *window_;

//创建和属性之类的，设置一次就ok
+ (void)initialize{
    window_ = [[UIWindow alloc] init];
    window_.frame = CGRectMake(0, 0, JKScreenW, 20);
    window_.windowLevel = UIWindowLevelAlert;
    [window_ addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topClick)]];
}

//在这里控制窗口的显示
+ (void)show{
    window_.hidden = NO;
}

//在这里控制窗口的隐藏
+ (void)hide{
    window_.hidden = YES;
}

//点击顶部window，注意类方法只能调用类方法
+ (void)topClick{
    //在keyWindow的子view中递归查找scrollView
    [self searchScrollViewInView:[UIApplication sharedApplication].keyWindow];
}

//递归查找scrollView
+ (void)searchScrollViewInView:(UIView *)superView{
    for (UIScrollView *subview in superView.subviews) {
        
        //如果是scrollView，滚动到最顶部
        if ([subview isKindOfClass:[UIScrollView class]] && subview.isShowingOnKeyWindow) {
            
            CGPoint offset = subview.contentOffset;
            
            //滑动到最上面，使用subview.contentInset.top
            offset.y = - subview.contentInset.top;
            [subview setContentOffset:offset animated:YES];
        }
        
        //继续查找子控件
        [self searchScrollViewInView:subview];
    }
}
@end
