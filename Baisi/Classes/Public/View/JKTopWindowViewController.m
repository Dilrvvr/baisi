//
//  JKTopWindowViewController.m
//  Baisi
//
//  Created by albert on 16/4/18.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "JKTopWindowViewController.h"
#import "JKSingleton.h"

@interface JKTopWindowViewController ()

@end

@implementation JKTopWindowViewController

JKSingletonM(TopWindow)

//强引用一下
static UIWindow *window_;

//创建和属性之类的，设置一次就ok
+ (void)initialize{
    window_ = [[UIWindow alloc] init];
    
    CGRect rect = [UIApplication sharedApplication].statusBarFrame;
    
    window_.frame = CGRectMake(80, 0, rect.size.width - 80, rect.size.height);
    
    window_.windowLevel = UIWindowLevelAlert;
    
    window_.rootViewController = [[self alloc] init];
    
    window_.backgroundColor = [UIColor clearColor];
    
    [window_.rootViewController.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topClick)]];
}

//设置状态栏是否隐藏
- (BOOL)prefersStatusBarHidden{
    return self.statusBarHidden;
}

- (void)setStatusBarHidden:(BOOL)statusBarHidden{
    _statusBarHidden = statusBarHidden;
    
    [self setNeedsStatusBarAppearanceUpdate];
}

//设置状态栏样式
- (UIStatusBarStyle)preferredStatusBarStyle{
    return self.statusBarStyle;
}

- (void)setStatusBarStyle:(UIStatusBarStyle)statusBarStyle{
    _statusBarStyle = statusBarStyle;
    
    [self setNeedsStatusBarAppearanceUpdate];
}

//在这里控制窗口的显示
+ (void)show{
    
    if ([[UIDevice currentDevice].systemVersion doubleValue] >= 11.0) {
        return;
    }
    
    window_.hidden = NO;
}

//在这里控制窗口的隐藏
+ (void)hide{
    window_.hidden = YES;
}

//点击顶部window，注意类方法只能调用类方法
+ (void)topClick{
    //在keyWindow的子view中递归查找scrollView
    [self searchScrollViewInView:[UIApplication sharedApplication].keyWindow];
}

//递归查找scrollView
+ (void)searchScrollViewInView:(UIView *)superView{
    for (UIScrollView *subview in superView.subviews) {
        
        //如果是scrollView，滚动到最顶部
        if ([subview isKindOfClass:[UIScrollView class]] && subview.isShowingOnKeyWindow) {
            
            CGPoint offset = subview.contentOffset;
            
            //滑动到最上面，使用subview.contentInset.top
            offset.y = - subview.contentInset.top;
            [subview setContentOffset:offset animated:YES];
        }
        
        //继续查找子控件
        [self searchScrollViewInView:subview];
    }
}
@end

