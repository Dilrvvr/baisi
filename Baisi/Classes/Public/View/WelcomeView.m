//
//  WelcomeView.m
//  01-微博动画
//
//  Created by xiaomage on 15/6/26.
//  Copyright (c) 2015年 xiaomage. All rights reserved.
//

#import "WelcomeView.h"

@interface WelcomeView ()
/** 标语 */
@property (weak, nonatomic) IBOutlet UIImageView *sloganView;
/** 文字 */
@property (weak, nonatomic) IBOutlet UILabel *textView;
/** 头像 */
@property (weak, nonatomic) IBOutlet UIImageView *iconView;

@end

@implementation WelcomeView

+ (instancetype)welcomeView{
    return [[NSBundle mainBundle] loadNibNamed:@"WelcomeView" owner:nil options:nil][0];
}

+ (void)show{
    WelcomeView *welcome = [self welcomeView];
    // 一定要设置frame
    welcome.frame = [UIApplication sharedApplication].keyWindow.bounds;
    [[UIApplication sharedApplication].keyWindow addSubview:welcome];
}

/*
 文字图片慢慢消失，alpha
 显示头像，头像往上移动的动画，弹簧效果
 欢迎回来的文字 慢慢显示 alpha
 */
// 当被添加到父控件的时候调用
- (void)didMoveToSuperview{
    [super didMoveToSuperview];
    
//    _iconView.layer.cornerRadius = 50;
//    _iconView.layer.masksToBounds = YES;
    CGFloat duration = 0.75;
    _iconView.transform = CGAffineTransformMakeTranslation(0, 50);
    
    [UIView animateWithDuration:duration delay:0.5 options:0  animations:^{
        _sloganView.alpha = 0;
    } completion:^(BOOL finished) {
        _iconView.hidden = NO;
        
        [UIView animateWithDuration:duration delay:0 usingSpringWithDamping:0.3 initialSpringVelocity:0 options:UIViewAnimationOptionCurveLinear animations:^{
            _iconView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            
            _textView.alpha = 0;
            _textView.hidden = NO;
           // 文字慢慢显示
            
            [UIView animateWithDuration:duration animations:^{
                _textView.alpha = 1;
                
            } completion:^(BOOL finished) {
               
                [self removeFromSuperview];
                
            }];
            
        }];
        
    }];
    
    
    JKLog(@"%s",__func__);
}

//- (void)didMoveToWindow
//{
//    [super didMoveToWindow];
//    NSLog(@"%s",__func__);
//}

- (void)dealloc{
    JKLog(@"%s", __func__);
}
@end
