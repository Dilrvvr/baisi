//
//  JKPushGuideView.h
//  Baisi
//
//  Created by albert on 16/3/18.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKPushGuideView : UIView

//封装通知提示
+ (void)show;

@end
