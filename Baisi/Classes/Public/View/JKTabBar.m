//
//  JKTabBar.m
//  Baisi
//
//  Created by albert on 16/3/13.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKTabBar.h"
#import "JKPublishView.h"

#import "JKPostWordViewController.h"
#import "JKNavigationController.h"


@interface JKTabBar ()
{
    UIEdgeInsets _oldSafeAreaInsets;
}
/** 发布按钮 */
@property (nonatomic,weak) UIButton *publishButton;

@end

@implementation JKTabBar

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _oldSafeAreaInsets = UIEdgeInsetsZero;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _oldSafeAreaInsets = UIEdgeInsetsZero;
}

- (void)safeAreaInsetsDidChange {
    [super safeAreaInsetsDidChange];
    
    if (!UIEdgeInsetsEqualToEdgeInsets(_oldSafeAreaInsets, self.safeAreaInsets)) {
        [self invalidateIntrinsicContentSize];
        
        if (self.superview) {
            [self.superview setNeedsLayout];
            [self.superview layoutSubviews];
        }
    }
}

- (CGSize)sizeThatFits:(CGSize)size {
    size = [super sizeThatFits:size];
    
    if (@available(iOS 11.0, *)) {
        float bottomInset = self.safeAreaInsets.bottom;
        if (bottomInset > 0 && size.height < 50 && (size.height + bottomInset < 90)) {
            size.height += bottomInset;
        }
    }
    
    return size;
}


- (void)setFrame:(CGRect)frame {
    if (self.superview) {
        if (frame.origin.y + frame.size.height != self.superview.frame.size.height) {
            frame.origin.y = self.superview.frame.size.height - frame.size.height;
        }
    }
    [super setFrame:frame];
}




#if 0

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //设置tabBar背景图片
        [self setBackgroundImage:[UIImage imageNamed:@"tabbar-light"]];
        
        //添加发布按钮
        UIButton *publishButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [publishButton setBackgroundImage:[UIImage imageNamed:@"tabBar_publish_icon"] forState:(UIControlStateNormal)];
        [publishButton setBackgroundImage:[UIImage imageNamed:@"tabBar_publish_click_icon"] forState:(UIControlStateHighlighted)];
        publishButton.size = publishButton.currentBackgroundImage.size;
        
        //注册事件
        [publishButton addTarget:self action:@selector(publishClick) forControlEvents:(UIControlEventTouchUpInside)];
        
        [self addSubview:publishButton];
        self.publishButton = publishButton;
    }
    return self;
}

//在这里布局子控件
- (void)layoutSubviews{
    [super layoutSubviews];
    
    //标记是否已经添加过监听器
    static BOOL added = NO;
    
    //设置发布按钮的frame,使用按钮当前背景图片的大小currentBackgroundImage
    //self.publishButton.bounds = CGRectMake(0, 0, self.publishButton.currentBackgroundImage.size.width, self.publishButton.currentBackgroundImage.size.height);
    
    //self.publishButton.width = self.publishButton.currentBackgroundImage.size.width;
    //self.publishButton.height = self.publishButton.currentBackgroundImage.size.height;
    //按钮的尺寸可以在initWithFrame里面设置，中心点要在layoutSubviews设置
    self.publishButton.center =CGPointMake(self.width * 0.5, self.height * 0.5);
    
    //设置其它按钮的frame
    CGFloat btnW = self.width / 5;
    CGFloat btnH = self.height;
    CGFloat btnY = 0;
    
    //按钮的索引
    NSInteger index = 0;
    for (UIControl *button in self.subviews) {
        //这样写会影响背景
        //if ([button isKindOfClass:[UIButton class]]) continue;
        
        //计算按钮的X值
        CGFloat btnX = btnW * ((index > 1) ? (index +1) : index);
        //这样写不会影响背景
        //UITabBarButton是私有的，不允许访问，但可以通过NSClassFromString
        //如果不是UITabBarButton类就跳过不设置
        //if (![button isKindOfClass:NSClassFromString(@"UITabBarButton")]) continue;
        
        //也可以这样写
        if (![button isKindOfClass:[UIControl class]] || button == self.publishButton) continue;
        button.frame = CGRectMake(btnX, btnY, btnW, btnH);
        
        //增加索引
        index++;
        
        if (added == NO) {
            //监听按钮点击
            [button addTarget:self action:@selector(buttonClick) forControlEvents:(UIControlEventTouchUpInside)];
        }
    }
    added = YES;
}

//按钮点击
- (void)buttonClick{
    //发出一个通知
//    [[NSNotificationCenter defaultCenter] postNotificationName:JKTabBarDoubleTapRefreshNotification object:nil userInfo:nil];
}

/**
 *  点击发布
 */
- (void)publishClick{
    [JKPublishView show];
    
//    JKPostWordViewController *postWordVc = [[JKPostWordViewController alloc] init];
//    JKNavigationController *nav = [[JKNavigationController alloc] initWithRootViewController:postWordVc];
    
//    如果self是个控制器，也不能用来弹出其它控制器，因为self执行了dismiss操作
//    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:YES completion:nil];
}
#endif
@end
