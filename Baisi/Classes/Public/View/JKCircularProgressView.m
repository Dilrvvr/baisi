//
//  JKCircularProgressView.m
//  Baisi
//
//  Created by albert on 2018/11/27.
//  Copyright © 2018 安永博. All rights reserved.
//

#import "JKCircularProgressView.h"

@interface JKCircularProgressView ()

// 执行动画的载体
@property (nonatomic, strong) CAShapeLayer *shapeLayer;
@end

@implementation JKCircularProgressView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initialization];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self initialization];
    }
    return self;
}

- (void)initialization{
    
    self.backgroundColor = nil;
    
    _circleColor = [UIColor redColor];
    _circleWidth = 2;
    
    UILabel *progressLabel = [[UILabel alloc] init];
    progressLabel.textAlignment = NSTextAlignmentCenter;
    progressLabel.textColor = self.circleColor;
    progressLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:progressLabel];
    _progressLabel = progressLabel;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.progressLabel.frame = self.bounds;
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    
    if (MIN(frame.size.width, frame.size.height) <= 0) { return; }
    
    [self resetShapeLayer];
}

- (void)resetShapeLayer{
    
    [_shapeLayer removeFromSuperlayer];
    _shapeLayer = nil;
    
    [self shapeLayer];
}

- (CAShapeLayer *)shapeLayer {
    if (!_shapeLayer) {
        _shapeLayer = [[CAShapeLayer alloc] init];
        _shapeLayer.frame = self.bounds;
        _shapeLayer.lineWidth = self.circleWidth;
        _shapeLayer.lineCap = kCALineCapRound;
        _shapeLayer.fillColor = [UIColor clearColor].CGColor;
        _shapeLayer.strokeColor = self.circleColor.CGColor;
        _shapeLayer.path = [[self circlePath] CGPath];
        _shapeLayer.strokeStart = 0.f;
        _shapeLayer.strokeEnd = self.progress;
        [self.layer insertSublayer:_shapeLayer atIndex:2];
    }
    return _shapeLayer;
}

- (void)setCircleWidth:(CGFloat)circleWidth{
    _circleWidth = circleWidth;
    
    [self resetShapeLayer];
}

- (void)setCircleColor:(UIColor *)circleColor{
    _circleColor = circleColor;
    
    [self resetShapeLayer];
    
    self.progressLabel.textColor = _circleColor;
}

- (void)setProgress:(CGFloat)progress{
    _progress = progress;
    
    [self.shapeLayer setStrokeEnd:progress];
    
    self.progressLabel.text = [NSString stringWithFormat:@"%.0f%%", _progress * 100.0];
}

- (UIBezierPath *)circlePath{
    
    CGFloat minWidth = MIN(self.bounds.size.width, self.bounds.size.height);
    
    CGFloat radius = minWidth * 0.5 - self.circleWidth * 0.5;
    
    CGPoint center = CGPointMake(minWidth * 0.5, minWidth * 0.5);
    
    CGFloat startA = -M_PI_2;
    CGFloat endA = startA + M_PI * 2;
    
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startA endAngle:endA clockwise:YES];
    
    return path;
}

@end
