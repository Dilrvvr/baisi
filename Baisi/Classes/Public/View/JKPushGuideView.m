//
//  JKPushGuideView.m
//  Baisi
//
//  Created by albert on 16/3/18.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKPushGuideView.h"

@implementation JKPushGuideView

//封装通知提示
+ (void)show{
    
    //获取UIApplication的keyWindow
//    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    //获取软件配置文件Info.plist
    NSDictionary *info = [NSBundle mainBundle].infoDictionary;
    
    //版本号的key
    NSString *versionKey = @"CFBundleShortVersionString";
    
    //获取当前软件的版本号
    NSString *currentVersion = info[versionKey];
    
    //获取存储到沙盒的版本号，首次打开app时为空
    NSString *localVersion = [[NSUserDefaults standardUserDefaults] stringForKey:versionKey];
    
    //如果当前版本号和沙盒的版本号不一致，就显示通知提示
    if (![currentVersion isEqualToString:localVersion]) {
        
        //类方法创建JKPushGuideView
//        JKPushGuideView *guideView = [JKPushGuideView viewFromXib];
//        
//        //注意一定要设置frame
//        guideView.frame = window.bounds;
//        
//        //主窗口添加guideView
//        [window addSubview:guideView];
        
        //将当前的版本号存到沙盒中
        [[NSUserDefaults standardUserDefaults] setObject:currentVersion forKey:versionKey];
        
        //强制同步
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//点击我知道了
- (IBAction)iKnow {
    [self removeFromSuperview];
}
@end
