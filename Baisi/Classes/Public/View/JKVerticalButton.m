//
//  JKVerticalButton.m
//  Baisi
//
//  Created by albert on 16/3/17.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKVerticalButton.h"

@implementation JKVerticalButton

- (void)setup{
    //文字居中
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
}

//代码创建的按钮要设置
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

//xib创建的按钮也要设置
- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self setup];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    //调整图片
    self.imageView.x = 0;
    self.imageView.y = 0;
    self.imageView.width = self.width;
    self.imageView.height = self.imageView.width;
    
    //调整文字
    self.titleLabel.x = 0;
    self.titleLabel.y = self.imageView.height + 15;
    self.titleLabel.width = self.width;
    self.titleLabel.height = self.height - self.titleLabel.y;
}
@end
