//
//  JKTextField.m
//  Baisi
//
//  Created by albert on 16/3/17.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKTextField.h"
#import <objc/runtime.h>

static NSString * const JKPlaceholderColorKeyPath = @"_placeholderLabel.textColor";

@implementation JKTextField

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self resignFirstResponder];
    
    //textField的tintColor设置光标颜色为输入的文字的颜色，白色
    self.tintColor = self.textColor;
}

//重写becomeFirstResponder，点击文本框时，占位文字显示为输入的文字的颜色，白色
- (BOOL)becomeFirstResponder{
    
    [self setValue:self.textColor forKeyPath:JKPlaceholderColorKeyPath];
    return [super becomeFirstResponder];
}

//重写resignFirstResponder，文本框未点击时，占位文字显示灰色
- (BOOL)resignFirstResponder{
    
    [self setValue:[UIColor grayColor] forKeyPath:JKPlaceholderColorKeyPath];
    return [super resignFirstResponder];
}

@end
