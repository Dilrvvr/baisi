//
//  JKCircularProgressView.h
//  Baisi
//
//  Created by albert on 2018/11/27.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKCircularProgressView : UIView

/** progressLabel */
@property (nonatomic, weak, readonly) UILabel *progressLabel;

/** 进度 */
@property (nonatomic, assign) CGFloat progress;

/** 进度圈的颜色 默认红色 */
@property (nonatomic, strong) UIColor *circleColor;

/** 进度圈的宽度 默认2 */
@property (nonatomic, assign) CGFloat circleWidth;
@end
