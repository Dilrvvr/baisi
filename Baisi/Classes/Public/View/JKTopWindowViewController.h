//
//  JKTopWindowViewController.h
//  Baisi
//
//  Created by albert on 16/4/18.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JKSingleton.h"

@interface JKTopWindowViewController : UIViewController

JKSingletonH(TopWindow)

/** 是否隐藏状态栏 */
@property (nonatomic, assign, getter=isStatusBarHidden) BOOL statusBarHidden;

/** 状态栏样式 */
@property (nonatomic, assign) UIStatusBarStyle statusBarStyle;

/** 显示窗口 */
+ (void)show;

/** 隐藏窗口 */
+ (void)hide;
@end
