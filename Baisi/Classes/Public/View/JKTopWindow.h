//
//  JKTopWindow.h
//  Baisi
//
//  Created by albert on 16/3/28.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKTopWindow : NSObject
/** 显示窗口 */
+ (void)show;

/** 隐藏窗口 */
+ (void)hide;
@end
