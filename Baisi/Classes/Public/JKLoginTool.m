//
//  JKLoginTool.m
//  Baisi
//
//  Created by albert on 16/4/1.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKLoginTool.h"
#import "JKLoginRegisterViewController.h"

static UIImage *placeholder_;

@implementation JKLoginTool

+ (void)initialize{
    
    placeholder_ = [[UIImage imageNamed:@"defaultUserIcon"] circleImage];
}

+ (UIImage *)headerIconPlaceHolder{
    
    return placeholder_;
}

/**
 *  判断是否有uid来检测登录状态
 *  有表示已经登录
 *  没有表示为登录
 */
+ (NSString *)getUid{
    return [self getUid:NO];
}

//是否需要弹出登录框
+ (NSString *)getUid:(BOOL)showLoginController{
    NSString *uid = [[NSUserDefaults standardUserDefaults] objectForKey:@"uid"];
    
    //稍微延时一下，因为有时候可能是从根控制器modal出来的界面再次modal登录界面
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        //需要弹出才弹出
        if (showLoginController) {
            //没有登录(没有uid)，就弹出登录界面
            if (!uid) {
                JKLoginRegisterViewController *loginVc = [[JKLoginRegisterViewController alloc] init];
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:loginVc animated:YES completion:nil];
            }
        }
    });
    return uid;
}

/**
 *  保存uid
 */
+ (void)setUid:(NSString *)uid{
    [[NSUserDefaults standardUserDefaults] setObject:uid forKey:@"uid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
