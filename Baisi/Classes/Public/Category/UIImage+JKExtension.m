//
//  UIImage+JKExtension.m
//  Baisi
//
//  Created by albert on 16/3/28.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "UIImage+JKExtension.h"

@implementation UIImage (JKExtension)
- (UIImage *)circleImage{
    //NO代表透明
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0);
    
    //获取上下文
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    //添加一个圆
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextAddEllipseInRect(ctx, rect);
    
    //裁剪
    CGContextClip(ctx);
    
    //将图片画上去
    [self drawInRect:rect];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    return image;
}
@end
