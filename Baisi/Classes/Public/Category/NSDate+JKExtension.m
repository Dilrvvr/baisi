//
//  NSDate+JKExtension.m
//  Baisi
//
//  Created by albert on 16/3/20.
//  Copyright (c) 2016年 albert. All rights reserved.
/**
 今年
   今天
      一分钟内-->刚刚
      一小时内-->xx分钟前
      其它----->xx小时前
   昨天
      昨天 18:32:23
   其它
      03-17 20:52:57
 
 非今年
   2015-12-21 08:13:37
 */

#import "NSDate+JKExtension.h"

@implementation NSDate (JKExtension)
/**
 *  对象方法,计算从date到调用对象相差的时间
 */
- (NSDateComponents *)deltaFromDate:(NSDate *)date{
    //日历
    NSCalendar *calendar = [NSCalendar currentCalendar];

    //比较时间
    NSCalendarUnit unit = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    return [calendar components:unit fromDate:date toDate:self options:0];
}

/**
 *  计算从startTingDate到resultDate相差的时间
 */
+ (NSDateComponents *)deltaFromeDate:(NSDate *)startTingDate toDate:(NSDate *)resultDate{
    //日历
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    //比较时间
    NSCalendarUnit unit = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    return [calendar components:unit fromDate:startTingDate toDate:resultDate options:0];
}

/**
 *  是否为今年
 */
- (BOOL)isThisYear{
    //日历
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSInteger nowYear = [calendar component:(NSCalendarUnitYear) fromDate:[NSDate date]];
    NSInteger selfYear = [calendar component:(NSCalendarUnitYear) fromDate:self];
    
    return nowYear == selfYear;
}

/**
 *  是否为今天第一种方法
 */
//- (BOOL)isToday{//年月日都一样才是今天
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    
//    NSCalendarUnit unit = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
//    
//    NSDateComponents *nowCmps = [calendar components:unit fromDate:[NSDate date]];
//    NSDateComponents *selfCmps = [calendar components:unit fromDate:self];
//    
//    return nowCmps.year  == selfCmps.year
//    &&     nowCmps.month == selfCmps.month
//    &&     nowCmps.day   == selfCmps.day;
//}

/**
 *  是否为今天第二种方法
 */
- (BOOL)isToday{//年月日都一样才是今天
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    
    NSString *nowStr = [fmt stringFromDate:[NSDate date]];
    NSString *selfStr = [fmt stringFromDate:self];
    
    return [nowStr isEqualToString:selfStr];
}

/**
 *  是否为昨天
 */
- (BOOL)isYesterday{
    // 2015-12-31 23:59:59 --> 2015-12-31
    // 2016-01-01 00:00:01 --> 2016-01-01
    
    //日期格式
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    
    //将日期格式转换为不带时分秒的
    NSDate *nowDate = [fmt dateFromString:[fmt stringFromDate:[NSDate date]]];
    NSDate *selfDate = [fmt dateFromString:[fmt stringFromDate:self]];
    
    //计算相差日期
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *cmps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:selfDate toDate:nowDate options:0];
    
    return cmps.year  == 0
        && cmps.month == 0
        && cmps.day   == 1;
}
@end
