//
//  JKAnimatedImageView.h
//  Baisi
//
//  Created by albert on 2018/7/14.
//  Copyright © 2018年 安永博. All rights reserved.
//

#import <FLAnimatedImage/FLAnimatedImage.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface JKAnimatedImageView : FLAnimatedImageView

- (void)fl_setImageWithURL:(NSURL *)URL;

- (void)fl_setImageWithURL:(NSURL *)URL placeholderImage:(UIImage *)placeholderImage;

- (void)fl_setImageWithURL:(NSURL *)URL placeholderImage:(UIImage *)placeholderImage options:(SDWebImageOptions)options progress:(void(^)(NSInteger receivedSize, NSInteger expectedSize, NSURL * targetURL))progress completed:(void(^)(UIImage * image, NSError * error, SDImageCacheType cacheType, NSURL * imageURL))completed;
@end
