//
//  UIBarButtonItem+JKExtention.m
//  Baisi
//
//  Created by albert on 16/3/13.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "UIBarButtonItem+JKExtention.h"

@implementation UIBarButtonItem (JKExtention)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"
//注意要传个target，不然会崩
- (instancetype)initWithImage:(NSString *)image highImage:(NSString *)highImage target:(id)target action:(SEL)action{
    
    //自定义按钮
    UIButton *button = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [button setBackgroundImage:[UIImage imageNamed:image] forState:(UIControlStateNormal)];
    [button setBackgroundImage:[UIImage imageNamed:highImage] forState:(UIControlStateHighlighted)];
    
    //还要设置尺寸才能显示出来
    button.size = button.currentBackgroundImage.size;
    
    //添加事件
    //这里如果不传target，就不知道是谁调用方法了
    [button addTarget:target action:action forControlEvents:(UIControlEventTouchUpInside)];
    
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}
#pragma clang diagnostic pop

+ (instancetype)itemWithImage:(NSString *)image highImage:(NSString *)highImage target:(id)target action:(SEL)action{
    return [[self alloc] initWithImage:image highImage:highImage target:target action:action];
}
@end
