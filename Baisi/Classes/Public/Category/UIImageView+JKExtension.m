//
//  UIImageView+JKExtension.m
//  Baisi
//
//  Created by albert on 16/3/28.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "UIImageView+JKExtension.h"
#import "UIImageView+WebCache.h"
#import "JKLoginTool.h"

@implementation UIImageView (JKExtension)

/** 设置头像 */
- (void)setHeaderIcon:(NSString *)urlStr{
    
    //设置图片
    [self sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[JKLoginTool headerIconPlaceHolder] options:(SDWebImageAvoidAutoSetImage) completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
        if (image == nil) {
            self.image = [JKLoginTool headerIconPlaceHolder];
            return;
        }
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            UIImage *img = [image circleImage];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.image = img;
            });
        });
    }];
}
@end
