//
//  JKLeftTopLabel.m
//  Baisi
//
//  Created by albert on 16/3/21.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "JKLeftTopLabel.h"

@implementation JKLeftTopLabel


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect {
//    // Drawing code
//}

//- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines{
//    
//    switch (self.textAlignment) {
//        case ver:
//            <#statements#>
//            break;
//            
//        default:
//            break;
//    }
//    
//    
//    return [super textRectForBounds:bounds limitedToNumberOfLines:numberOfLines];
//}

//- (void)drawTextInRect:(CGRect)rect{
//    rect = self.frame;
//    
//    [super drawTextInRect:rect];
//}

//- (void)setFrame:(CGRect)frame{
//    
//    frame.size.height = [self.text sizeWithAttributes:@{NSFontAttributeName : self.font}].height;
//    
//    [super setFrame:frame];
//}

- (void)setHeight:(CGFloat)height{
    
    height = [self.text sizeWithAttributes:@{NSFontAttributeName : self.font}].height;
    JKLog(@"setHeight %f", height);
    
    [super setHeight:height];
}


@end
