//
//  UILabel+JKTopLeft.m
//  Baisi
//
//  Created by albert on 16/3/21.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "UILabel+JKTopLeft.h"

@implementation UILabel (JKTopLeft)
- (void)setTextTopLeft{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSMutableDictionary *attr = [NSMutableDictionary dictionary];
    attr[NSFontAttributeName] = self.font;
    attr[NSParagraphStyleAttributeName] = paragraphStyle.copy;
    
    CGSize labelSize = [self.text sizeWithAttributes:@{NSFontAttributeName : self.font}];
    
    CGRect textFrame = CGRectMake(2, 140, self.width - 20, labelSize.height);
    
    self.frame = textFrame;
}
@end
