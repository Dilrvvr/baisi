//
//  UIImageView+JKExtension.h
//  Baisi
//
//  Created by albert on 16/3/28.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (JKExtension)
/** 设置头像 */
- (void)setHeaderIcon:(NSString *)urlStr;
@end
