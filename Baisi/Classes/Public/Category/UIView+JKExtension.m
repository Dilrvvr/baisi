//
//  UIView+JKExtension.m
//  Baisi
//
//  Created by albert on 16/3/13.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import "UIView+JKExtension.h"
#import <objc/message.h>

// 定义关联的key
static const char *jk_bottomSafeAreaView_key = "jk_bottomSafeAreaView_key";

@implementation UIView (JKExtension)

- (void)setCenterX:(CGFloat)centerX{
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (void)setCenterY:(CGFloat)centerY{
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

- (CGFloat)centerX{
    return self.center.x;
}

- (CGFloat)centerY{
    return self.center.y;
}

- (void)setSize:(CGSize)size{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGSize)size{
    return self.frame.size;
}

- (void)setWidth:(CGFloat)width{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (void)setHeight:(CGFloat)height{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (void)setX:(CGFloat)x{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (void)setY:(CGFloat)y{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)width{
    return self.frame.size.width;
}

- (CGFloat)height{
    return self.frame.size.height;
}

- (CGFloat)x{
    return self.frame.origin.x;
}

- (CGFloat)y{
    return self.frame.origin.y;
}

- (UIView *)jk_bottomSafeAreaView{
    
    if ([UIApplication sharedApplication].statusBarFrame.size.height < 44) {
        return objc_getAssociatedObject(self, jk_bottomSafeAreaView_key);
    }
    
    if (objc_getAssociatedObject(self, jk_bottomSafeAreaView_key)) {
        return objc_getAssociatedObject(self, jk_bottomSafeAreaView_key);
    }
    
    UIView *bottomSafeAreaView = [[UIView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height -34, [UIScreen mainScreen].bounds.size.width, 34)];
    [self addSubview:bottomSafeAreaView];
    [self setJk_bottomSafeAreaView:bottomSafeAreaView];
    
    return objc_getAssociatedObject(self, jk_bottomSafeAreaView_key);
}

- (void)setJk_bottomSafeAreaView:(UIView *)jk_bottomSafeAreaView{
    objc_setAssociatedObject(self, jk_bottomSafeAreaView_key, jk_bottomSafeAreaView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

/** 适配iOS11的scrollView */
- (void)jk_adapt_iOS_11_scrollView{
    if (![self isKindOfClass:[UIScrollView class]]) {
        return;
    }
    
    SEL selector = NSSelectorFromString(@"setContentInsetAdjustmentBehavior:");
    
    if ([self respondsToSelector:selector]) {
        
        IMP imp = [self methodForSelector:selector];
        void (*func)(id, SEL, NSInteger) = (void *)imp;
        func(self, selector, 2);
        
        // [tbView performSelector:@selector(setContentInsetAdjustmentBehavior:) withObject:@(2)];
    }
    
    if (!JKIsIphoneX) {
        return;
    }
    
    if ([self isKindOfClass:[UITableView class]] && (CGRectGetMaxY(self.frame) >= JKScreenH)) {
        
        [(UITableView *)self setContentInset:UIEdgeInsetsMake([(UITableView *)self contentInset].top, [(UITableView *)self contentInset].left, JKCurrentHomeIndicatorHeight, [(UITableView *)self contentInset].right)];
        
        [((UITableView *)self) setScrollIndicatorInsets:UIEdgeInsetsMake([(UITableView *)self contentInset].top, 0, JKCurrentHomeIndicatorHeight, 0)];
        
        ((UITableView *)self).rowHeight = 44;
        ((UITableView *)self).sectionFooterHeight = 0;
        ((UITableView *)self).sectionHeaderHeight = 0;
        
        ((UITableView *)self).tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, JKScreenW, CGFLOAT_MIN)];
        ((UITableView *)self).tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, JKScreenW, CGFLOAT_MIN)];
        
        [(UITableView *)self setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGFLOAT_MIN, CGFLOAT_MIN)]];
    }
}

/** 判断控件是否真正显示在窗口范围内 */
- (BOOL)isShowingOnKeyWindow{
    //主窗口
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    
    //得到self在主窗口中的frame
    // 转换self的frame，写成nil就是转为以屏幕左上角为原点
    //CGRect newFrame = [self.superview convertRect:subview.frame toView:nil];
    CGRect newFrame = [keyWindow convertRect:self.frame fromView:self.superview];
    
    //窗口的bounds
    CGRect windowBounds = keyWindow.bounds;
    
    //主窗口的bounds 和 subview主窗口左上角为原点的frame 是否有重叠
    // CGRectIntersectsRect(newFrame, windowBounds)比较两个rect是否有交叉
    BOOL intersects = CGRectIntersectsRect(newFrame, windowBounds);
    
    //返回。控件没有被添加到窗口上时，控件的window == nil
    return !self.hidden && self.alpha > 0.01 && intersects && self.window == keyWindow;
}

/** 从xib中加载view */
+ (instancetype)viewFromXib{
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
}

/** 切换横竖屏 */
+ (void)changeInterfaceOrientation:(UIInterfaceOrientation)orientation{
    
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector             = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val                  = orientation;
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }
}
@end
