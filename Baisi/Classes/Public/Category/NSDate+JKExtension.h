//
//  NSDate+JKExtension.h
//  Baisi
//
//  Created by albert on 16/3/20.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (JKExtension)
/**
 *  对象方法,计算从date到调用对象相差的时间
 */
- (NSDateComponents *)deltaFromDate:(NSDate *)date;

/**
 *  计算从startTingDate到resultDate相差的时间
 */
+ (NSDateComponents *)deltaFromeDate:(NSDate *)startTingDate toDate:(NSDate *)resultDate;

/**
 *  是否为今年
 */
- (BOOL)isThisYear;

/**
 *  是否为今天
 */
- (BOOL)isToday;

/**
 *  是否为昨天
 */
- (BOOL)isYesterday;
@end
