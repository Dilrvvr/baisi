//
//  JKAnimatedImageView.m
//  Baisi
//
//  Created by albert on 2018/7/14.
//  Copyright © 2018年 安永博. All rights reserved.
//

#import "JKAnimatedImageView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import <SDWebImage/NSData+ImageContentType.h>

@interface JKAnimatedImageView ()

/** 当前的url */
@property (nonatomic, copy) NSString *currentUrl;

/** gifQueue */
@property (nonatomic, strong) dispatch_queue_t gifQueue;
@end

@implementation JKAnimatedImageView

- (dispatch_queue_t)gifQueue{
    if (!_gifQueue) {
        _gifQueue = dispatch_queue_create("gifQueue", DISPATCH_QUEUE_CONCURRENT);
    }
    return _gifQueue;
}

- (void)fl_setImageWithURL:(NSURL *)URL{
    
    [self fl_setImageWithURL:URL placeholderImage:nil];
}

- (void)fl_setImageWithURL:(NSURL *)URL placeholderImage:(UIImage *)placeholderImage{
    
    [self fl_setImageWithURL:URL placeholderImage:placeholderImage options:0 progress:nil completed:nil];
}

- (void)fl_setImageWithURL:(NSURL *)URL placeholderImage:(UIImage *)placeholderImage options:(SDWebImageOptions)options progress:(void(^)(NSInteger receivedSize, NSInteger expectedSize, NSURL * targetURL))progress completed:(void(^)(UIImage * image, NSError * error, SDImageCacheType cacheType, NSURL * imageURL))completed{
    
    self.currentUrl = URL.absoluteString;
    
    // 忽略大写
    BOOL isGif = [URL.absoluteString.pathExtension.lowercaseString isEqualToString:@"gif"];
    
    NSString *path = [[[SDWebImageManager sharedManager] imageCache] defaultCachePathForKey:URL.absoluteString];
    
    if (isGif && [[NSFileManager defaultManager] fileExistsAtPath:path]) {
        
        NSData *imageData = [NSData dataWithContentsOfFile:path];
        
        SDImageFormat imageFormat = [NSData sd_imageFormatForImageData:imageData];
        
        if (imageFormat != SDImageFormatGIF) { return; }
        
        dispatch_async(self.gifQueue, ^{
            
            NSString *url = URL.absoluteString;
            
            FLAnimatedImage *animatedImage = [FLAnimatedImage animatedImageWithGIFData:imageData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (![url isEqualToString:self.currentUrl]) {
                    
                    JKLog(@"直接设置gif时URL不一致！！！");
                    
                    return;
                }
                
                self.animatedImage = animatedImage;
                self.image = nil;
                
                !completed ? : completed(nil, nil, 0, nil);
            });
        });
        
        return;
    }
    
    [self sd_setImageWithURL:URL placeholderImage:nil options:options progress:progress completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        !completed ? : completed(image, error, cacheType, imageURL);
        
        // 忽略大写
        BOOL isGif = [imageURL.absoluteString.pathExtension.lowercaseString isEqualToString:@"gif"];
        
        if (!isGif) { return; }
        
        NSString *path = [[[SDWebImageManager sharedManager] imageCache] defaultCachePathForKey:imageURL.absoluteString];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:path]) { return; }
        
        dispatch_async(self.gifQueue, ^{
            
            NSData *imageData = [NSData dataWithContentsOfFile:path];
            
            SDImageFormat imageFormat = [NSData sd_imageFormatForImageData:imageData];
            
            if (imageFormat != SDImageFormatGIF) { return; }
            
            FLAnimatedImage *animatedImage = [FLAnimatedImage animatedImageWithGIFData:imageData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (![self.currentUrl isEqualToString:imageURL.absoluteString]) {
                    
                    JKLog(@"URL不一致！！！");
                    
                    return;
                }
                
                self.animatedImage = animatedImage;
                self.image = nil;
            });
        });
    }];
}

@end
