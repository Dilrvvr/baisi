//
//  UIImage+JKExtension.h
//  Baisi
//
//  Created by albert on 16/3/28.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (JKExtension)
- (UIImage *)circleImage;
@end
