//
//  UILabel+JKTopLeft.h
//  Baisi
//
//  Created by albert on 16/3/21.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (JKTopLeft)
- (void)setTextTopLeft;
@end
