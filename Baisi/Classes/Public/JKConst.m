
#import <UIKit/UIKit.h>

BOOL JKIsIphoneX = NO;

/** 微信AppKey */
NSString * const JKWXAppkey = @"wxc4c34b3806d1cfb2";

/** 微博appKey */
NSString * const JKWeiboAppKey = @"1935760277";

/** 精华-顶部标题的高度 35 */
CGFloat const titlesViewH = 35;
/** 精华-顶部标题的Y 64 */
//CGFloat const titlesViewY = 64;

/** 帖子cell的间距 10 */
CGFloat const JKPostCellMargin = 8;
/** 精华-cell-文字内容的Y 50*/
CGFloat const JKPostCellTextY = 50;
/** 精华-cell-底部工具条高度 40 */
CGFloat const JKPostCellBottomBar = 40;

/** 精华-cell-图片帖子中图片的最大高度 640 */
CGFloat const JKPostCellPictureMaxH = 640;

/** JKUserModel-性别属性值-男 */
NSString * const JKUserSexMale = @"m";
/** JKUserModel-性别属性值-女 */
NSString * const JKUserSexFemale = @"f";

/** 精华-cell-最热评论标题高度 20 */
CGFloat const JKPostCellTopCmtTitleH = 20;

/** tabBar被选中的通知名字 */
NSString * const JKTabBarDoubleTapRefreshNotification = @"JKTabBarDoubleTapRefreshNotification";
/** tabBar被选中的通知 - 被点击的控制器的index */
NSString * const JKSelectedControllerIndexKey = @"JKSelectedControllerIndexKey";
/** tabBar被选中的通知 - 被点击的控制器的 key */
NSString * const JKSelectedControllerKey = @"JKSelectedControllerKey";

/** 重置播放器的通知 */
NSString * const JKResetPlayerNotification = @"JKResetPlayerNotification";

/** 添加标签-间距 5 */
CGFloat const JKTagMargin = 5;
/** 标签-高度 25 */
CGFloat const JKTagH = 25;



/** 开启自动旋转 通知名字 */
NSString * const JKTurnOnAutoRotateNotification = @"JKTurnOnAutoRotateNotification";

/** 关闭自动旋转 通知名字 */
NSString * const JKTurnOffAutoRotateNotification = @"JKTurnOffAutoRotateNotification";




BOOL checkScreenX (void) {
    
    if (@available(iOS 11.0, *)) {
        
        JKIsIphoneX = [UIApplication sharedApplication].delegate.window.safeAreaInsets.bottom > 0;
    }
    
    return JKIsIphoneX;
}

