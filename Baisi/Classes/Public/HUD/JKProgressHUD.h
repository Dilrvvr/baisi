//
//  JKProgressHUD.h
//  ZhiHuDaily
//
//  Created by albert on 16/8/20.
//  Copyright © 2016年 albert. All rights reserved.
//  继承于SVProgressHUD，可在这里自定义

#import <SVProgressHUD/SVProgressHUD.h>

@interface JKProgressHUD : SVProgressHUD
+ (void)showWithLoadingIsAllowUserAction:(BOOL)isAllowUserAction;

+ (void)showWithStatus:(NSString *)status isAllowUserAction:(BOOL)isAllowUserAction;

+ (void)doNotAllowUserAction;
@end
