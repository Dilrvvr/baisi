//
//  JKProgressHUD.m
//  ZhiHuDaily
//
//  Created by albert on 16/8/20.
//  Copyright © 2016年 albert. All rights reserved.
//  加载提示工具

#import "JKProgressHUD.h"

@implementation JKProgressHUD

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)setup{
    self.defaultMaskType = SVProgressHUDMaskTypeNone;
    self.defaultStyle = SVProgressHUDStyleDark;
    self.minimumDismissTimeInterval = 1;
    self.maximumDismissTimeInterval = 1;
    
    
    //sdsdsdsdsd
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [self.containerView addSubview:view];

    view.backgroundColor = [UIColor redColor];
    

        
    
    
//    [JKProgressHUD setInfoImage:[UIImage imageNamed:@"l2_8_share_icon_02"]];
    
    

    
}

+ (void)showWithLoadingIsAllowUserAction:(BOOL)isAllowUserAction{
    [self showWithStatus:@"加载中..." isAllowUserAction:isAllowUserAction];
    
    if (!isAllowUserAction) {
        [self doNotAllowUserAction];
    }
}

+ (void)showWithStatus:(NSString *)status isAllowUserAction:(BOOL)isAllowUserAction{
    [self showWithStatus:status];
    
    if (!isAllowUserAction) {
        [self doNotAllowUserAction];
    }
}

+ (void)doNotAllowUserAction{
    [self setDefaultMaskType:SVProgressHUDMaskTypeClear];
}



@end
