//JKSingleton.h
 /**JKSingleton.h
 单例对象自始至终会一直存在
 可以用在登录控制器上，使登录控制器为单例对象，点击许多地方都可以弹出登录界面
 */

// .h文件
#define JKSingletonH(name) + (instancetype)shared##name;

// .m文件
// '\'代表下一行也是宏，注意空格
// 宏定义传参  (name)    ##name
// dispatch_once本身就是线程安全的
#if __has_feature(objc_arc)
//编译器是ARC环境
#define JKSingletonM(name) \
static id _instance; \
\
+ (instancetype)allocWithZone:(struct _NSZone *)zone { \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
return _instance; \
} \
\
+ (instancetype)shared##name { \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[self alloc] init]; \
}); \
return _instance; \
} \
\
- (id)copyWithZone:(NSZone *)zone { \
return _instance; \
}

#else

//编译器是MRC环境
#define JKSingletonM(name) \
static id _instance; \
\
+ (instancetype)allocWithZone:(struct _NSZone *)zone { \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
return _instance; \
} \
\
+ (instancetype)shared##name { \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[self alloc] init]; \
}); \
return _instance; \
} \
\
- (id)copyWithZone:(NSZone *)zone { \
return _instance; \
} \
 \
- (oneway void)release{}; \
- (id)retain{return self;} \
- (NSInteger)retainCount{return 1;} \
- (id)autorelease{return self;}

#endif

