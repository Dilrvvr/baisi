//
//  FullViewController.m
//  TTNews
//
//  Created by 瑞文戴尔 on 16/4/3.
//  Copyright © 2016年 瑞文戴尔. All rights reserved.
//

#import "FullViewController.h"
#import "JKTopWindowViewController.h"
#import "AppDelegate.h"

@interface FullViewController ()

@end

@implementation FullViewController

- (instancetype)init{
    if (self = [super init]) {
        [(AppDelegate *)[UIApplication sharedApplication].delegate setIsCanAutoRotate:YES];
        
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    return self;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationLandscapeRight;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[JKTopWindowViewController sharedTopWindow] setStatusBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[JKTopWindowViewController sharedTopWindow] setStatusBarHidden:NO];
}

- (void)dealloc{
    JKLog(@"%s", __func__);
    [(AppDelegate *)[UIApplication sharedApplication].delegate setIsCanAutoRotate:NO];
}
@end
