//
//  JKLoginTool.h
//  Baisi
//
//  Created by albert on 16/4/1.
//  Copyright (c) 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKLoginTool : NSObject
//检测登录状态
//+ (BOOL)checkLoginStatus;

/**
 *  判断是否有uid来检测登录状态
 *  有表示已经登录
 *  没有表示为登录
 */
+ (NSString *)getUid;

//是否需要弹出登录框
+ (NSString *)getUid:(BOOL)showLoginController;

/**
 *  保存uid
 */
+ (void)setUid:(NSString *)uid;

+ (UIImage *)headerIconPlaceHolder;
@end
