//
//  JKVideoManager.h
//  Baisi
//
//  Created by albert on 2018/3/20.
//  Copyright © 2018年 安永博. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JKPostModel;

@interface JKVideoManager : NSObject

/** 视频缓存文件夹文件总大小 */
+ (NSString *)videoCacheSize;

/** 视频缓存文件夹的信息 */
+ (NSDictionary *)videoCacheDirectoryInfo;

/** 保存视频的文件夹名称 */
+ (NSString *)videoCacheDirectoryName;

/** videoInfo.plist 文件夹名 即@"videoInfo.plist" */
+ (NSString *)videoInfoName;

/** 保存视频的文件夹路径 */
+ (NSString *)videoDirectoryPath;

/** videoInfo.plist 路径 */
+ (NSString *)videoInfoPath;

/** 视频是否已缓存 */
+ (BOOL)videoExistWithUrl:(NSString *)url;

/** 获取视频缓存的路径 */
+ (NSString *)videoPathWithUrl:(NSString *)url;

/** 删除缓存的视频 */
+ (BOOL)deleteVideWithUrl:(NSString *)url;

+ (void)downloadVideoWithPostModel:(JKPostModel *)model
                    progress:(void (^)(NSProgress *downloadProgress))downloadProgressBlock
           completionHandler:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler;

@end
