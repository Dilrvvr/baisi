//
//  JKWebServerViewController.h
//  Baisi
//
//  Created by albert on 2018/11/23.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JKWebServerViewController : UIViewController

/** directory */
@property (nonatomic, copy) NSString *directory;
@end

NS_ASSUME_NONNULL_END
