//
//  JKWebServerHelper.h
//  Baisi
//
//  Created by albert on 2018/11/23.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKWebServerHelper : NSObject

/** 获取当前设备Wifi名称 */
+ (NSString *)deviceWifiSSID;

/** 获取ip地址 */
+ (NSString *)deviceIPAdress;

#pragma mark - 获取设备当前网络IP地址
+ (NSString *)getIPAddress:(BOOL)preferIPv4;
@end
