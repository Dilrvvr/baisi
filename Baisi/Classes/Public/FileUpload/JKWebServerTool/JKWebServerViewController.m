//
//  JKWebServerViewController.m
//  Baisi
//
//  Created by albert on 2018/11/23.
//  Copyright © 2018 安永博. All rights reserved.
//

#import "JKWebServerViewController.h"
#import "JKWebServerManager.h"

@interface JKWebServerViewController ()

/** tipLabel */
@property (nonatomic, weak) UILabel *tipLabel;
@end

@implementation JKWebServerViewController

-(void)dealloc{
    
    [JKWebServerManager stop];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createUI];
    
    [self layoutUI];
    
    [self initializaData];
    
    [self startServer];
}

- (void)createUI{
    
    UILabel *tipLabel = [[UILabel alloc] init];
    tipLabel.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    tipLabel.font = [UIFont systemFontOfSize:16];
    tipLabel.textAlignment = NSTextAlignmentCenter;
    tipLabel.numberOfLines = 0;
    [self.view addSubview:tipLabel];
    _tipLabel = tipLabel;
    
}

- (void)layoutUI{
    
    self.tipLabel.frame = CGRectMake(15, (self.view.frame.size.height - 60) * 0.5, (self.view.frame.size.width - 30), 60);
}

- (void)initializaData{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.title = @"WebServer";
    
}

- (void)startServer{
    
    [JKWebServerManager startWithUploadDirectoryPath:self.directory delegate:nil config:^(JKWebServerManager *manager) {
        
        
    } success:^(NSString *url, NSString *IP, NSUInteger port) {
        
        self.tipLabel.text = [NSString stringWithFormat:@"请在网页输入此地址:%@", url];
        
    } failure:^(NSError *error) {
        
        self.tipLabel.text = error.localizedDescription;
    }];
}

#pragma mark
#pragma mark - GCDWebUploaderDelegate

/**
 *  This method is called whenever a file has been downloaded.
 */
- (void)webUploader:(GCDWebUploader *)uploader didDownloadFileAtPath:(NSString*)path{
    
}

/**
 *  This method is called whenever a file has been uploaded.
 */
- (void)webUploader:(GCDWebUploader *)uploader didUploadFileAtPath:(NSString*)path{
    
}

/**
 *  This method is called whenever a file or directory has been moved.
 */
- (void)webUploader:(GCDWebUploader *)uploader didMoveItemFromPath:(NSString*)fromPath toPath:(NSString*)toPath{
    
}

/**
 *  This method is called whenever a file or directory has been deleted.
 */
- (void)webUploader:(GCDWebUploader *)uploader didDeleteItemAtPath:(NSString*)path{
    
}

/**
 *  This method is called whenever a directory has been created.
 */
- (void)webUploader:(GCDWebUploader *)uploader didCreateDirectoryAtPath:(NSString*)path{
    
}

#pragma mark
#pragma mark - GCDWebServerDelegate

/**
 *  This method is called after the server has successfully started.
 */
- (void)webServerDidStart:(GCDWebServer*)server{
    
}

/**
 *  This method is called after the Bonjour registration for the server has
 *  successfully completed.
 *
 *  Use the "bonjourServerURL" property to retrieve the Bonjour address of the
 *  server.
 */
- (void)webServerDidCompleteBonjourRegistration:(GCDWebServer*)server{
    
}

/**
 *  This method is called after the NAT port mapping for the server has been
 *  updated.
 *
 *  Use the "publicServerURL" property to retrieve the public address of the
 *  server.
 */
- (void)webServerDidUpdateNATPortMapping:(GCDWebServer*)server{
    
}

/**
 *  This method is called when the first GCDWebServerConnection is opened by the
 *  server to serve a series of HTTP requests.
 *
 *  A series of HTTP requests is considered ongoing as long as new HTTP requests
 *  keep coming (and new GCDWebServerConnection instances keep being opened),
 *  until before the last HTTP request has been responded to (and the
 *  corresponding last GCDWebServerConnection closed).
 */
- (void)webServerDidConnect:(GCDWebServer*)server{
    
}

/**
 *  This method is called when the last GCDWebServerConnection is closed after
 *  the server has served a series of HTTP requests.
 *
 *  The GCDWebServerOption_ConnectedStateCoalescingInterval option can be used
 *  to have the server wait some extra delay before considering that the series
 *  of HTTP requests has ended (in case there some latency between consecutive
 *  requests). This effectively coalesces the calls to -webServerDidConnect:
 *  and -webServerDidDisconnect:.
 */
- (void)webServerDidDisconnect:(GCDWebServer*)server{
    
}

/**
 *  This method is called after the server has stopped.
 */
- (void)webServerDidStop:(GCDWebServer*)server{
    
}
@end
