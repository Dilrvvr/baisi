//
//  JKWebServerManager.h
//  Baisi
//
//  Created by albert on 2018/11/23.
//  Copyright © 2018 安永博. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GCDWebUploader, GCDWebServer;

@protocol JKWebServerManagerDelegate <NSObject>

@optional

/**
 *  This method is called whenever a file has been downloaded.
 */
- (void)webUploader:(GCDWebUploader *)uploader didDownloadFileAtPath:(NSString *)path;

/**
 *  This method is called whenever a file has been uploaded.
 */
- (void)webUploader:(GCDWebUploader *)uploader didUploadFileAtPath:(NSString *)path;

/**
 *  This method is called whenever a file or directory has been moved.
 */
- (void)webUploader:(GCDWebUploader *)uploader didMoveItemFromPath:(NSString *)fromPath toPath:(NSString *)toPath;

/**
 *  This method is called whenever a file or directory has been deleted.
 */
- (void)webUploader:(GCDWebUploader *)uploader didDeleteItemAtPath:(NSString *)path;

/**
 *  This method is called whenever a directory has been created.
 */
- (void)webUploader:(GCDWebUploader *)uploader didCreateDirectoryAtPath:(NSString *)path;


/**
 *  This method is called after the server has successfully started.
 */
- (void)webServerDidStart:(GCDWebServer *)server;

/**
 *  This method is called after the Bonjour registration for the server has
 *  successfully completed.
 *
 *  Use the "bonjourServerURL" property to retrieve the Bonjour address of the
 *  server.
 */
- (void)webServerDidCompleteBonjourRegistration:(GCDWebServer *)server;

/**
 *  This method is called after the NAT port mapping for the server has been
 *  updated.
 *
 *  Use the "publicServerURL" property to retrieve the public address of the
 *  server.
 */
- (void)webServerDidUpdateNATPortMapping:(GCDWebServer *)server;

/**
 *  This method is called when the first GCDWebServerConnection is opened by the
 *  server to serve a series of HTTP requests.
 *
 *  A series of HTTP requests is considered ongoing as long as new HTTP requests
 *  keep coming (and new GCDWebServerConnection instances keep being opened),
 *  until before the last HTTP request has been responded to (and the
 *  corresponding last GCDWebServerConnection closed).
 */
- (void)webServerDidConnect:(GCDWebServer *)server;

/**
 *  This method is called when the last GCDWebServerConnection is closed after
 *  the server has served a series of HTTP requests.
 *
 *  The GCDWebServerOption_ConnectedStateCoalescingInterval option can be used
 *  to have the server wait some extra delay before considering that the series
 *  of HTTP requests has ended (in case there some latency between consecutive
 *  requests). This effectively coalesces the calls to -webServerDidConnect:
 *  and -webServerDidDisconnect:.
 */
- (void)webServerDidDisconnect:(GCDWebServer *)server;

/**
 *  This method is called after the server has stopped.
 */
- (void)webServerDidStop:(GCDWebServer *)server;
@end



@interface JKWebServerManager : NSObject

/** 上传的路径 */
@property(nonatomic, readonly) NSString *uploadDirectory;

/** 代理 */
@property(nonatomic, weak, nullable) id<JKWebServerManagerDelegate> delegate;

/** 限制文件上传类型 */
@property(nonatomic, copy) NSArray *allowedFileExtensions;

/** 是否允许隐藏文件 */
@property(nonatomic) BOOL allowHiddenItems;

/** 网页标题 浏览器标签标题 */
@property(nonatomic, copy) NSString *title;

/** 设置展示在网页上顶部的文字 默认和title一致 */
@property(nonatomic, copy) NSString *header;

/** 设置展示在网页上的文字(开场白) */
@property(nonatomic, copy) NSString *prologue;

/** 设置展示在网页上的文字(收场白) */
@property(nonatomic, copy) NSString *epilogue;

/** 设置展示在网页上底部的文字 通常显示版权信息等 */
@property(nonatomic, copy) NSString *footer;



+ (void)startWithUploadDirectoryPath:(NSString *)directoryPath
                            delegate:(id<JKWebServerManagerDelegate>)delegate
                             success:(void(^)(NSString *url, NSString *IP, NSUInteger port))success
                             failure:(void(^)(NSError *error))failure;

+ (void)startWithUploadDirectoryPath:(NSString *)directoryPath
                            delegate:(id<JKWebServerManagerDelegate>)delegate
                              config:(void(^)(JKWebServerManager *manager))config
                             success:(void(^)(NSString *url, NSString *IP, NSUInteger port))success
                             failure:(void(^)(NSError *error))failure;

+ (void)stop;
@end


