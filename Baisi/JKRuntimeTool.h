//
//  JKRuntimeTool.h
//  Baisi
//
//  Created by 安永博 on 2016/11/24.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKRuntimeTool : NSObject
/** 获取某个类的属性列表 .h中的属性 */
+ (void)getPropertiesWithClassName:(NSString *)className;

/** 获取某个类所有的成员变量列表 包括私有的 */
+ (void)getMemberVariablesWithClassName:(NSString *)className;
@end
