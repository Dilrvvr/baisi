#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "JKAlertAction.h"
#import "JKAlertCollectionViewCell.h"
#import "JKAlertConst.h"
#import "JKAlertTableViewCell.h"
#import "JKAlertTextView.h"
#import "JKAlertView.h"
#import "JKAlertX.h"
#import "JKBaseAlertView.h"
#import "UIControl+JKAlertX.h"
#import "UIGestureRecognizer+JKAlertX.h"
#import "UIView+JKAlertX.h"

FOUNDATION_EXPORT double JKAlertXVersionNumber;
FOUNDATION_EXPORT const unsigned char JKAlertXVersionString[];

